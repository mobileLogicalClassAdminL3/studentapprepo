package com.thoughtbot.expandablerecyclerview.models;

import java.util.List;

public class ExpandableGroup<T> {
    private String title;
    private List<T> items;

    public ExpandableGroup(String title, List<T> items) {
        this.title = title;
        this.items = items;
    }

    public String getTitle() {
        return title;
    }

    public List<T> getItems() {
        return items;
    }

    public int getItemCount() {
        return items == null ? 0 : items.size();
    }
}
