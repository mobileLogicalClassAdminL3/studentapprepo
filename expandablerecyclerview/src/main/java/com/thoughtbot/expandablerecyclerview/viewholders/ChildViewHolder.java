package com.thoughtbot.expandablerecyclerview.viewholders;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.thoughtbot.expandablerecyclerview.listeners.OnChildClickListener;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

/**
 * ViewHolder for {@link ExpandableGroup}
 */
public class ChildViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private OnChildClickListener listener;

    public ChildViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (listener != null) {
            listener.onChildClick(getAdapterPosition(), -1, "");
        }
    }

    public void setOnChildClickListener(OnChildClickListener listener) {
        this.listener = listener;
    }
}
