package com.thoughtbot.expandablerecyclerview.listeners;

import androidx.annotation.Nullable;

public interface OnChildClickListener {

    /**
     * @param flatPos the flat position (raw index within the list of visible items in the
     *                RecyclerView of a GroupViewHolder)
     * @return false if click expanded group, true if click collapsed group
     */
    void onChildClick(int flatPos,
                      int childPos,
                      @Nullable String subjectTitle);
}