package com.logicalclass.onlinecourseproject.ocr_q

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import com.canhub.cropper.CropImageContractOptions
import com.canhub.cropper.CropImageView
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.databinding.ActivityCameraBinding
import com.logicalclass.onlinecourseproject.repository.RxUtils
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils
import com.logicalclass.onlinecourseproject.utils.hide
import com.otaliastudios.cameraview.CameraListener
import com.otaliastudios.cameraview.PictureResult
import com.otaliastudios.cameraview.controls.Flash
import io.reactivex.Observable
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStream

class QuestionsScannerActivity : AppCompatActivity() {
    lateinit var gallery: ActivityResultLauncher<Intent>
    lateinit var cropImage: ActivityResultLauncher<CropImageContractOptions>
    lateinit var binding: ActivityCameraBinding
    var flashModes = listOf(Flash.AUTO, Flash.ON, Flash.OFF)
    var currentFlashMode = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCameraBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        binding.cameraView.setLifecycleOwner(this)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            title = getString(R.string.scanner)
        }
        binding.cameraView.addCameraListener(object : CameraListener() {
            override fun onPictureTaken(result: PictureResult) {
                super.onPictureTaken(result)
                result.toFile(getFile()) {
                    it?.let {
                        navigateToCrop(it)
                    }
                }
            }
        })

        binding.tvLogo.text = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.ORG_NAME)

        binding.ivCancel.setOnClickListener {
            binding.flCropMessage.hide()
        }

        binding.ivFlash.setOnClickListener {
            currentFlashMode = (currentFlashMode + 1) % flashModes.size
            updateFlash()
        }
        binding.primaryClickButton.setOnClickListener {
            binding.cameraView.takePicture()
        }

        binding.llTypeQuestion.setOnClickListener {
            showTypeQuestionDialog()
        }

        binding.llGallery.setOnClickListener {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.type = "image/*"
            gallery.launch(intent)
        }
        cropImage = registerForActivityResult(CropImageContract()) { result ->
            if (result.isSuccessful) {
                result.uriContent?.let { navigateToNextScreen(it) }
            }
        }
        gallery =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
                if (result.resultCode == Activity.RESULT_OK) {
                    result.data?.data?.let {
                        val inputStream: InputStream? =
                            applicationContext.contentResolver.openInputStream(it)
                        val outputFile = getFile()
                        inputStream?.let {
                            outputFile.outputStream().write(it)
                        }
                        navigateToCrop(outputFile)
                    }
                }
            }
        Handler(Looper.getMainLooper()).postDelayed({
            updateFlash()
        }, 1000)
    }

    private fun showTypeQuestionDialog() {
        val dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_type_doubt, null, false)
        val editText = dialogView.findViewById<EditText>(R.id.edtTypeDoubt)
        val builder = AlertDialog.Builder(this)
            .setView(dialogView)
            .setPositiveButton("Find Solution") { _, _ ->
                navigateToNextScreen(typedText = editText.text.toString())
            }
            .setNegativeButton("Cancel", null)
            .show()

        /*editText.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            editText.post {
                val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(editText, InputMethodManager.SHOW_FORCED)
            }
        }*/
        builder.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = false
        editText.doAfterTextChanged {
            it?.let {
                builder.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = it.isNotEmpty()
            }
        }
        editText.requestFocus()
    }

    private fun updateFlash() {
        when (flashModes[currentFlashMode]) {
            Flash.AUTO -> {
                binding.cameraView.flash = Flash.AUTO
                binding.ivFlash.setImageResource(R.drawable.ic_flash_auto)
            }
            Flash.ON -> {
                binding.cameraView.flash = Flash.ON
                binding.ivFlash.setImageResource(R.drawable.ic_flash_on)
            }
            else -> {
                binding.cameraView.flash = Flash.OFF
                binding.ivFlash.setImageResource(R.drawable.ic_flash_off)
            }
        }
    }

    fun navigateToCrop(file: File) {
        cropImage.launch(
            com.canhub.cropper.options(uri = Uri.fromFile(file)) {
                setGuidelines(CropImageView.Guidelines.OFF)
                setOutputCompressFormat(Bitmap.CompressFormat.PNG)
            }
        )
    }

    private fun navigateToNextScreen(fileUri: Uri? = null, typedText: String? = null) {
        startActivity(
            Intent(
                this@QuestionsScannerActivity,
                QuestionsScanningResultActivity::class.java
            ).apply {
                typedText?.let { putExtra("TYPED_TEXT", typedText) }
                fileUri?.let { putExtra("URI", fileUri?.toString()) }
            })
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    fun getFile(): File {
        return File(filesDir, "img${System.currentTimeMillis()}.jpg")
    }

    fun OutputStream.write(inputStream: InputStream) {
        try {
            val buffer = ByteArray(4 * 1024) // or other buffer size
            var read: Int
            while (inputStream.read(buffer).also { read = it } != -1) {
                write(buffer, 0, read)
            }
            close()
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(applicationContext, "Crash", Toast.LENGTH_SHORT).show()
        }
    }

    fun Bitmap.saveToFile(file: File, callback: (success: Boolean, e: Exception?) -> Unit) {
        Observable.fromCallable {
            FileOutputStream(file.path).use { out ->
                this@saveToFile.compress(Bitmap.CompressFormat.PNG, 100, out)
            }
        }.subscribeOn(RxUtils.ioThread())
            .observeOn(RxUtils.androidThread())
            .subscribe({
                callback(true, null)
            }, {
                callback(false, java.lang.Exception(it))
            }
            )
    }
}