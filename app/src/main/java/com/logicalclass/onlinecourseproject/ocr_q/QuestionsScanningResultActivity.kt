package com.logicalclass.onlinecourseproject.ocr_q

import android.annotation.SuppressLint
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.databinding.ActivityQuestionsScanningResultBinding
import com.logicalclass.onlinecourseproject.ocr_q.model.ScannedDataResponse
import com.logicalclass.onlinecourseproject.repository.AppService
import com.logicalclass.onlinecourseproject.repository.RxUtils
import com.logicalclass.onlinecourseproject.utils.*
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils.REFRESH_TIPS
import io.reactivex.disposables.CompositeDisposable

class QuestionsScanningResultActivity : AppCompatActivity(), OCRUtils.Listener {
    private lateinit var binding: ActivityQuestionsScanningResultBinding
    private lateinit var ocrUtils: OCRUtils
    private var disposable: CompositeDisposable? = null

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityQuestionsScanningResultBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        setToolbarData()
        ocrUtils = OCRUtils(this)
        disposable = CompositeDisposable()

        when {
            intent.hasExtra("TYPED_TEXT") -> {
                val text = intent.getStringExtra("TYPED_TEXT")
                binding.ivImage.hide()
                binding.txtScannedContent.text = text
                requestForScannedContent(text ?: "")
            }
            intent.hasExtra("URI") -> {
                fetchTextFromImage()
            }
            else -> Unit
        }
    }

    private fun fetchTextFromImage() {
        val uri = intent.getStringExtra("URI")
        Uri.parse(uri)?.let {
            binding.ivImage.setImageURI(it)
            binding.txtScannedContent.hide()
            try {
                val bitmap =
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        ImageDecoder.decodeBitmap(ImageDecoder.createSource(contentResolver, it))
                    } else {
                        MediaStore.Images.Media.getBitmap(contentResolver, it)
                    }
                ocrUtils.detect(bitmap)
            } catch (e: Exception) {
                Toast.makeText(applicationContext, "Bitmap failed", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onSuccess(text: String) {
        requestForScannedContent(text)
    }

    override fun onError(exception: Exception?) {
        runOnUiThread {
            showAlertMessageWithAction(
                message = getString(R.string.something_went_wrong),
                callBack = ::onBackPressed
            )
        }
    }

    private fun requestForScannedContent(scannedContent: String) {
        if (CommonUtils.isNetworkAvailable(application)) {
            disposable?.add(
                AppService.create().getScannedContent(
                    SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.TOKEN_ID_2),
                    scannedContent
                )
                    .subscribeOn(RxUtils.ioThread())
                    .doOnSubscribe {
                        showProgress()
                    }.observeOn(RxUtils.androidThread())
                    .subscribe({ response: ScannedDataResponse ->
                        handleScannedResponse(response)
                    }, {
                        showAlertMessageWithAction(
                            message = getString(R.string.something_went_wrong),
                            callBack = ::onBackPressed
                        )
                        hideProgress()
                    })
            )
        } else {
            showAlertMessageWithAction(
                message = getString(R.string.no_network_error),
                callBack = ::onBackPressed
            )
        }
    }

    private fun handleScannedResponse(response: ScannedDataResponse) {
        with(response) {
            if (status == 200 && data != null && data.questions != null && data.questions.isNotEmpty()) {
                val layoutManager = LinearLayoutManager(this@QuestionsScanningResultActivity)
                val mockTestListAdapter =
                    QScanningResultsAdapter(this@QuestionsScanningResultActivity, data.questions)
                binding.rvList.layoutManager = layoutManager
                binding.rvList.adapter = mockTestListAdapter
                Handler(Looper.getMainLooper()).postDelayed({
                    hideProgress()
                }, 1500)
                SharedPrefsUtils.setBooleanPreference(
                    this@QuestionsScanningResultActivity,
                    REFRESH_TIPS,
                    true
                )
            } else {
                showAlertMessageWithAction(message = msg, callBack = ::onBackPressed)
                hideProgress()
            }
        }
    }

    private fun hideProgress() {
        binding.rlProgress.hide()
    }

    private fun showProgress() {
        binding.rlProgress.show()
    }

    private fun setToolbarData() {
        supportActionBar?.title =
            SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.ORG_NAME)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.clear()
    }
}