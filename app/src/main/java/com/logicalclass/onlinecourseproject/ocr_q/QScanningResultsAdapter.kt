package com.logicalclass.onlinecourseproject.ocr_q

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.logicalclass.onlinecourseproject.databinding.ItemQscanresultViewBinding
import com.logicalclass.onlinecourseproject.onlinetest.model.QuestionModel

class QScanningResultsAdapter(
    val context: Context,
    private val questionsList: List<QuestionModel>
) :
    RecyclerView.Adapter<QScanningResultsAdapter.QScanningResultsViewHolder>() {

    override fun getItemCount(): Int {
        return questionsList.size
    }

    private fun getItem(position: Int): QuestionModel {
        return questionsList[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QScanningResultsViewHolder {
        return QScanningResultsViewHolder(
            ItemQscanresultViewBinding.inflate(
                LayoutInflater.from(context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: QScanningResultsViewHolder, position: Int) {
        with(getItem(position)) {
            val resultText = "<b>Id:#${id}</b></br><b>Question:</b>${getFormattedQuestion()}" +
                    "<b>Solution:</b>${getFormattedSolution()}"
            holder.binding.txtQuestionAnswer.setDisplayText(resultText)
        }
    }

    inner class QScanningResultsViewHolder(var binding: ItemQscanresultViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
    }
}