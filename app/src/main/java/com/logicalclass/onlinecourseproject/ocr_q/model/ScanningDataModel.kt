package com.logicalclass.onlinecourseproject.ocr_q.model

import com.google.gson.annotations.SerializedName
import com.logicalclass.onlinecourseproject.onlinetest.model.QuestionModel
import com.logicalclass.onlinecourseproject.repository.Auth

data class ScannedDataResponse(
    val authData: Auth,
    @SerializedName("data")
    val data: QuestionsContent?,
    val msg: String,
    val status: Int,
    val token: String
)

data class QuestionsContent(
    val questions: List<QuestionModel>?,
)