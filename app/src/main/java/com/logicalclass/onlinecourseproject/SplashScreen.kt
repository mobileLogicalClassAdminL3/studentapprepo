package com.logicalclass.onlinecourseproject

import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.vectordrawable.graphics.drawable.Animatable2Compat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.gif.GifDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.logicalclass.onlinecourseproject.announcements.view.PublicAnnouncementsActivity
import com.logicalclass.onlinecourseproject.attendance.view.DayWiseAttendance
import com.logicalclass.onlinecourseproject.authentication.view.LoginActivity
import com.logicalclass.onlinecourseproject.home.MainActivity
import com.logicalclass.onlinecourseproject.onlineclasses.OnlineClassesActivity
import com.logicalclass.onlinecourseproject.onlinecourses.OnlineCoursesActivity
import com.logicalclass.onlinecourseproject.onlinediary.view.DigitalDiaryActivity
import com.logicalclass.onlinecourseproject.onlinetest.MockTestsActivity
import com.logicalclass.onlinecourseproject.service.MyFirebaseMessagingService.Companion.EXAM_ID
import com.logicalclass.onlinecourseproject.service.MyFirebaseMessagingService.Companion.NOTIFICATION_TYPE
import com.logicalclass.onlinecourseproject.service.MyFirebaseMessagingService.Companion.TYPE_ATTENDANCE
import com.logicalclass.onlinecourseproject.service.MyFirebaseMessagingService.Companion.TYPE_CLASS
import com.logicalclass.onlinecourseproject.service.MyFirebaseMessagingService.Companion.TYPE_DIARY
import com.logicalclass.onlinecourseproject.service.MyFirebaseMessagingService.Companion.TYPE_EXAM
import com.logicalclass.onlinecourseproject.service.MyFirebaseMessagingService.Companion.TYPE_ONLINE_COURSE
import com.logicalclass.onlinecourseproject.service.MyFirebaseMessagingService.Companion.TYPE_ONLINE_TEST_NOT_ATTEMPTED
import com.logicalclass.onlinecourseproject.service.MyFirebaseMessagingService.Companion.TYPE_ONLINE_TEST_RESCHEDULED
import com.logicalclass.onlinecourseproject.service.MyFirebaseMessagingService.Companion.TYPE_ONLINE_TEST_SCHEDULED
import com.logicalclass.onlinecourseproject.service.MyFirebaseMessagingService.Companion.TYPE_PRE_ONLINE_TEST_ALERT
import com.logicalclass.onlinecourseproject.service.MyFirebaseMessagingService.Companion.TYPE_PUBLIC_ANNOUNCEMENTS
import com.logicalclass.onlinecourseproject.socrecard.view.ScoreCardActivity
import com.logicalclass.onlinecourseproject.utils.CommonUtils
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils
import kotlinx.android.synthetic.main.splashscreen.*

class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splashscreen)
        showLogoAnimation()
    }

    private fun showLogoAnimation() {
        Glide.with(this)
            .asGif()
            .load(R.raw.splash_animation)
            .listener(object : RequestListener<GifDrawable?> {
                override fun onLoadFailed(
                    e: GlideException?, model: Any?,
                    target: Target<GifDrawable?>?,
                    isFirstResource: Boolean
                ): Boolean {
                    goWithHandlerIfAnimationFails()
                    return false
                }

                override fun onResourceReady(
                    resource: GifDrawable?, model: Any?,
                    target: Target<GifDrawable?>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    resource?.setLoopCount(1)
                    resource?.registerAnimationCallback(object :
                        Animatable2Compat.AnimationCallback() {
                        override fun onAnimationEnd(drawable: Drawable?) {
                            handleNotificationClickFlow(intent.extras)
                        }
                    })
                    return false
                }
            }).into(imgLogo)
    }

    // Using handler with postDelayed called runnable run method
    private fun goWithHandlerIfAnimationFails() {
        imgLogo.setImageResource(R.drawable.logo2)
        Handler().postDelayed(
            {
                handleNotificationClickFlow(intent.extras)
            },
            3 * 1000.toLong()
        )
    }

    private fun handleNotificationClickFlow(bundle: Bundle?) {
        if (BuildConfig.FLAVOR == "gios") {
            showServicesSuspendedDialog()
            return
        }
        bundle?.let {
            if (it.containsKey(NOTIFICATION_TYPE)) {
                val intent = when (it.getString(NOTIFICATION_TYPE)) {
                    TYPE_EXAM -> {
                        val intent = Intent(this, ScoreCardActivity::class.java)
                        intent.putExtra(EXAM_ID, it.getString(EXAM_ID))
                        intent
                    }
                    TYPE_CLASS -> {
                        Intent(this, OnlineClassesActivity::class.java)
                    }
                    TYPE_PUBLIC_ANNOUNCEMENTS -> {
                        Intent(this, PublicAnnouncementsActivity::class.java)
                    }
                    TYPE_ATTENDANCE -> {
                        Intent(this, DayWiseAttendance::class.java)
                    }
                    TYPE_DIARY -> {
                        Intent(this, DigitalDiaryActivity::class.java)
                    }
                    TYPE_ONLINE_COURSE -> {
                        Intent(this, OnlineCoursesActivity::class.java)
                    }
                    TYPE_ONLINE_TEST_SCHEDULED,
                    TYPE_ONLINE_TEST_RESCHEDULED,
                    TYPE_ONLINE_TEST_NOT_ATTEMPTED,
                    TYPE_PRE_ONLINE_TEST_ALERT -> {
                        Intent(this, MockTestsActivity::class.java)
                    }
                    else ->
                        Intent(this, SplashScreen::class.java)
                }
                startNewActivity(intent)
            } else {
                handleRegularFlow()
            }
        } ?: kotlin.run {
            handleRegularFlow()
        }
    }

    private fun handleRegularFlow() {
        val intent: Intent = when {
            SharedPrefsUtils.getBooleanPreference(
                this, SharedPrefsUtils.REMEMBER_ME, false
            ) -> {
                val intent = Intent(this@SplashScreen, MainActivity::class.java)
                intent.putExtra("coming_from", "SplashScreen")
                intent
            }
            BuildConfig.FLAVOR == "logicalclass" -> {
                Intent(this, SearchOrganisationActivity::class.java)
            }
            else -> {
                Intent(this, LoginActivity::class.java)
            }
        }
        startNewActivity(intent)
    }

    /*showing temporary message for Geetanjali school*/
    private fun showServicesSuspendedDialog() {
        val suspendedMsg =
            CommonUtils.getHtmlData("Dear Organisation,\n<font color='#FF0000'>Services were suspended</font> temporarily. Recharge for uninterrupted services.")
        val builder = AlertDialog.Builder(this)
            .setMessage(suspendedMsg)
            .setPositiveButton(android.R.string.ok, null)
            .setCancelable(false)
            .show()

        val mPositiveButton = builder.getButton(AlertDialog.BUTTON_POSITIVE)
        mPositiveButton.setOnClickListener {
            // Do nothing
        }
    }

    private fun startNewActivity(intent: Intent) {
        intent.addFlags(
            Intent.FLAG_ACTIVITY_CLEAR_TOP
                    or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    or Intent.FLAG_ACTIVITY_NEW_TASK
        )
        startActivity(intent)
        finish()
    }
}