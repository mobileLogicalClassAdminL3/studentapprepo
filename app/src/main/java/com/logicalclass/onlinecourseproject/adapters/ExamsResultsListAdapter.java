package com.logicalclass.onlinecourseproject.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.logicalclass.onlinecourseproject.databinding.ItemExamResultsListBinding;
import com.logicalclass.onlinecourseproject.models.ExamResultData;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ExamsResultsListAdapter extends RecyclerView.Adapter<ExamsResultsListAdapter.ExamResultViewHolder> {

    private Context context;
    private List<ExamResultData> examResultDataList;
    private ICallBacks callBacks;

    public interface ICallBacks {
        void onScoreCardClicked(int position, ExamResultData resultData);

        void onViewResultClicked(int position, ExamResultData resultData);
    }

    public ExamsResultsListAdapter(Context context, List<ExamResultData> examResultDataList, ICallBacks callBacks) {
        this.context = context;
        this.examResultDataList = examResultDataList;
        this.callBacks = callBacks;
    }

    @Override
    public int getItemCount() {
        return examResultDataList.size();
    }

    public ExamResultData getItem(int position) {
        return examResultDataList.get(position);
    }

    @NonNull
    @Override
    public ExamResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ExamResultViewHolder(
                ItemExamResultsListBinding.inflate(
                        LayoutInflater.from(context),
                        parent,
                        false
                ));
    }

    @Override
    public void onBindViewHolder(@NonNull ExamResultViewHolder holder, int position) {
        ExamResultData examResultData = examResultDataList.get(position);
        holder.binding.setExamData(examResultData);
    }


    class ExamResultViewHolder extends RecyclerView.ViewHolder {

        ItemExamResultsListBinding binding;

        ExamResultViewHolder(ItemExamResultsListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.btnScoreCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callBacks != null) {
                        callBacks.onScoreCardClicked(getAdapterPosition(), getItem(getAdapterPosition()));
                    }
                }
            });

            binding.btnViewResult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callBacks != null) {
                        callBacks.onViewResultClicked(getAdapterPosition(), getItem(getAdapterPosition()));
                    }
                }
            });
        }
    }
}
