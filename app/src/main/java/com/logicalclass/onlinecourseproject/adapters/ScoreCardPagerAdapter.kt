package com.logicalclass.onlinecourseproject.adapters

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.logicalclass.onlinecourseproject.socrecard.view.FragmentQuestionAnalytics
import com.logicalclass.onlinecourseproject.socrecard.view.FragmentScoreCardDetails

class ScoreCardPagerAdapter(fm: FragmentManager, val examId: String?) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val tabTitles = arrayOf("Subject Wise", "Analytical Abilities", "Time Spent", "Question Analytics")

    override fun getItem(position: Int): Fragment {
        return if (position == 3) {
            val fragment = FragmentQuestionAnalytics()
            val bundle = Bundle()
            bundle.putInt("position", position)
            bundle.putString("examId", examId)
            fragment.arguments = bundle
            fragment
        } else {
            val fragment = FragmentScoreCardDetails()
            val bundle = Bundle()
            bundle.putInt("position", position)
            fragment.arguments = bundle
            fragment
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return tabTitles.get(position)
    }

    override fun getCount(): Int {
        return tabTitles.size
    }
}