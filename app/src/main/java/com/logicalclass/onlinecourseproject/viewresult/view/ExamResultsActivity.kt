package com.logicalclass.onlinecourseproject.viewresult.view

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.adapters.ExamsResultsListAdapter
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.models.ExamResultData
import com.logicalclass.onlinecourseproject.models.ExamResultResponse
import com.logicalclass.onlinecourseproject.repository.AppService
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.androidThread
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.ioThread
import com.logicalclass.onlinecourseproject.service.MyFirebaseMessagingService.Companion.EXAM_ID
import com.logicalclass.onlinecourseproject.utils.show
import com.logicalclass.onlinecourseproject.utils.showApiParsingErrorSnack
import com.logicalclass.onlinecourseproject.socrecard.view.ScoreCardActivity
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_exam_results.*

class ExamResultsActivity : AppCompatActivity(), ExamsResultsListAdapter.ICallBacks {
    private var tokenid2: String? = null
    private var examsResultsListAdapter: ExamsResultsListAdapter? = null
    private var disposable: CompositeDisposable? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exam_results)
        disposable = CompositeDisposable()
        setToolbarData()
        tokenid2 = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.TOKEN_ID_2)
        requestForOnlineTestResult()
    }

    private fun setToolbarData() {
        if (supportActionBar != null) {
            supportActionBar?.title = getString(R.string.exam_results)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowHomeEnabled(true)
        }
    }

    private fun requestForOnlineTestResult() {
        disposable?.add(AppService.create().getOnlineTestResult(tokenid2)
                .subscribeOn(ioThread())
                .doOnSubscribe {
                    showProgress()
                }.observeOn(androidThread())
                .subscribe({ response: ExamResultResponse ->
                    handleOnlineTestResultResponse(response)
                }, {
                    rootView.showApiParsingErrorSnack()
                    llEmptyData.show()
                })
        )
    }

    private fun handleOnlineTestResultResponse(response: ExamResultResponse) {
        if (response.status == 200 && response.examResultsList.isNotEmpty()) {
            val list = response.examResultsList
            val layoutManager = LinearLayoutManager(this@ExamResultsActivity)
            examsResultsListAdapter = ExamsResultsListAdapter(
                    this@ExamResultsActivity, list, this@ExamResultsActivity)
            rvExamsList?.layoutManager = layoutManager
            rvExamsList?.adapter = examsResultsListAdapter
        } else {
            llEmptyData.show()
        }
        hideProgress()
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun showProgress() {
        rlProgress.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.clear()
    }

    override fun onScoreCardClicked(position: Int, resultData: ExamResultData) {
        val intent = Intent(this@ExamResultsActivity, ScoreCardActivity::class.java)
        intent.putExtra(EXAM_ID, resultData.examResultDetails.id)
        startActivity(intent)
    }

    override fun onViewResultClicked(position: Int, resultData: ExamResultData) {
        val intent = Intent(this@ExamResultsActivity, ViewResultsActivity::class.java)
        intent.putExtra(EXAM_ID, resultData.examResultDetails.id)
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}