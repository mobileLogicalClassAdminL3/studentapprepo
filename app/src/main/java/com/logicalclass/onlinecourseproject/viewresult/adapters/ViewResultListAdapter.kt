package com.logicalclass.onlinecourseproject.viewresult.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.logicalclass.onlinecourseproject.viewresult.view.ViewResultsActivity.Companion.TYPE_ANALYTICAL_ABILITIES
import com.logicalclass.onlinecourseproject.viewresult.view.ViewResultsActivity.Companion.TYPE_CHAPTER_WISE
import com.logicalclass.onlinecourseproject.viewresult.view.ViewResultsActivity.Companion.TYPE_TOPIC_ANALYSIS
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.databinding.ItemViewResultAnalyticsBinding
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.utils.show
import com.logicalclass.onlinecourseproject.viewresult.models.AnalyticalAbilitiesData
import com.logicalclass.onlinecourseproject.viewresult.models.ChapterWiseData
import com.logicalclass.onlinecourseproject.viewresult.models.TopicWiseAnalyticsData

class ViewResultListAdapter(private val viewResultList: List<Any>, val viewResultType: String?) :
        RecyclerView.Adapter<ViewResultListAdapter.ViewResultViewHolder>() {

    override fun getItemCount(): Int {
        return viewResultList.size
    }

    fun getItem(position: Int): Any? {
        return viewResultList[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewResultViewHolder {
        return ViewResultViewHolder(
                ItemViewResultAnalyticsBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                ))
    }

    override fun onBindViewHolder(holder: ViewResultViewHolder, position: Int) {
        val viewResultModel = getItem(position)
        setViewData(holder, viewResultModel)
    }

    private fun setViewData(holder: ViewResultViewHolder, viewResultModel: Any?) {
        when (viewResultType) {
            TYPE_CHAPTER_WISE -> {
                (viewResultModel as? ChapterWiseData)?.apply {
                    holder.binding.txtTitleLabel.text = holder.binding.txtTitleLabel.context.getString(R.string.label_chapter)
                    holder.binding.txtTitle.text = this.chapter
                    holder.binding.txtDescription.hide()
                    holder.binding.txtTimeReport.hide()
                    holder.binding.txtScore.show()
                    holder.binding.txtScore.text = holder.binding.txtScore.context.getString(R.string.score_, this.score)
                    holder.binding.txtPerformance.text = this.performanceCh

                    holder.binding.lineView.show()
                    holder.binding.lineView.setBackgroundColor(holder.binding.lineView.context.getColor(this.getBorderLineColor()))

                    holder.binding.txtRightPercentage.text = this.getRightPercentageText()
                    holder.binding.txtWrongPercentage.text = this.getWrongPercentageText()
                    holder.binding.txtAttemptedPercentage.text = this.getAttemptedPercentageText()
                    holder.binding.txtNotAttemptedPercentage.text = this.getNotAttemptedPercentageText()

                    holder.binding.rightProgress.progress = this.getRightProgress()
                    holder.binding.wrongProgress.progress = this.getWrongProgress()
                    holder.binding.attemptedProgress.progress = this.getAttemptedProgress()
                    holder.binding.notAttemptedProgress.progress = this.getNotAttemptedProgress()
                }
            }
            TYPE_TOPIC_ANALYSIS -> {
                (viewResultModel as? TopicWiseAnalyticsData)?.apply {
                    holder.binding.txtTitleLabel.text = holder.binding.txtTitleLabel.context.getString(R.string.label_Topic)
                    holder.binding.txtTitle.text = this.topic
                    holder.binding.txtDescription.show()
                    holder.binding.txtDescriptionLabel.text = holder.binding.txtDescriptionLabel.context.getString(R.string.label_chapter)
                    holder.binding.txtDescription.text = this.chapter
                    holder.binding.txtScore.show()
                    holder.binding.txtScore.text = holder.binding.txtScore.context.getString(R.string.score_,
                            this.scoreTopic?.toString())
                    holder.binding.txtPerformance.text = this.performanceTopic
                    holder.binding.txtTimeReport.show()
                    holder.binding.txtTimeReport.text = this.timeSaverSucker

                    holder.binding.lineView.show()
                    holder.binding.lineView.setBackgroundColor(holder.binding.lineView.context.getColor(this.getBorderLineColor()))

                    holder.binding.txtRightPercentage.text = this.getRightPercentageText()
                    holder.binding.txtWrongPercentage.text = this.getWrongPercentageText()
                    holder.binding.txtAttemptedPercentage.text = this.getAttemptedPercentageText()
                    holder.binding.txtNotAttemptedPercentage.text = this.getNotAttemptedPercentageText()

                    holder.binding.rightProgress.progress = this.getRightProgress()
                    holder.binding.wrongProgress.progress = this.getWrongProgress()
                    holder.binding.attemptedProgress.progress = this.getAttemptedProgress()
                    holder.binding.notAttemptedProgress.progress = this.getNotAttemptedProgress()
                }
            }
            TYPE_ANALYTICAL_ABILITIES -> {
                (viewResultModel as? AnalyticalAbilitiesData)?.apply {
                    holder.binding.txtTitle.text = this.key
                    holder.binding.txtDescription.hide()
                    holder.binding.txtScore.hide()
                    holder.binding.txtPerformance.hide()
                    holder.binding.txtTimeReport.hide()
                    holder.binding.lineView.hide()

                    holder.binding.txtRightPercentage.text = this.getRightPercentageText()
                    holder.binding.txtWrongPercentage.text = this.getWrongPercentageText()
                    holder.binding.txtAttemptedPercentage.text = this.getAttemptedPercentageText()
                    holder.binding.txtNotAttemptedPercentage.text = this.getNotAttemptedPercentageText()

                    holder.binding.rightProgress.progress = this.getRightProgress()
                    holder.binding.wrongProgress.progress = this.getWrongProgress()
                    holder.binding.attemptedProgress.progress = this.getAttemptedProgress()
                    holder.binding.notAttemptedProgress.progress = this.getNotAttemptedProgress()
                }
            }
        }
    }

    inner class ViewResultViewHolder(var binding: ItemViewResultAnalyticsBinding) : RecyclerView.ViewHolder(binding.root)
}