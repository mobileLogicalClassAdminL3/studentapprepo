package com.logicalclass.onlinecourseproject.viewresult.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.logicalclass.onlinecourseproject.viewresult.view.ViewResultsActivity.Companion.TYPE_ANALYTICAL_ABILITIES
import com.logicalclass.onlinecourseproject.viewresult.view.ViewResultsActivity.Companion.TYPE_CHAPTER_WISE
import com.logicalclass.onlinecourseproject.viewresult.view.ViewResultsActivity.Companion.TYPE_TOPIC_ANALYSIS
import com.logicalclass.onlinecourseproject.databinding.FragmentViewResultBinding
import com.logicalclass.onlinecourseproject.utils.show
import com.logicalclass.onlinecourseproject.viewresult.adapters.ViewResultListAdapter
import com.logicalclass.onlinecourseproject.viewresult.models.ViewResultData
import kotlinx.android.synthetic.main.fragment_view_result.*

class ViewResultsTabFragment : Fragment() {
    private var position: Int = 0
    private var viewResultType: String? = null
    private lateinit var binding: FragmentViewResultBinding
    private var viewResultListAdapter: ViewResultListAdapter? = null
    private var viewResultData: ViewResultData? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentViewResultBinding.inflate(LayoutInflater.from(context), container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getResultData()
        setViewData()
    }

    private fun getResultData() {
        position = arguments?.getInt(CURRENT_QUESTION_POS) ?: 0
        viewResultType = arguments?.getString(VIEW_RESULT_TYPE, "")
        viewResultData = arguments?.getParcelable(VIEW_RESULT_DATA)
    }

    private fun setViewData() {
        val layoutManager = LinearLayoutManager(activity)
        rvList.layoutManager = layoutManager
        when (viewResultType) {
            TYPE_CHAPTER_WISE -> {
                viewResultListAdapter = ViewResultListAdapter(viewResultData?.chapterWise
                        ?: emptyList(), viewResultType)
                rvList.adapter = viewResultListAdapter
            }
            TYPE_TOPIC_ANALYSIS -> {
                viewResultListAdapter = ViewResultListAdapter(viewResultData?.topicWise
                        ?: emptyList(), viewResultType)
                rvList.adapter = viewResultListAdapter
            }
            TYPE_ANALYTICAL_ABILITIES -> {
                viewResultListAdapter = ViewResultListAdapter(viewResultData?.analytics
                        ?: emptyList(), viewResultType)
                rvList.adapter = viewResultListAdapter
            }
            else -> {
                llEmptyData.show()
            }
        }
    }

    override fun onDestroyView() {
        if (view != null) {
            val parentViewGroup = view?.parent as ViewGroup?
            parentViewGroup?.removeAllViews();
        }
        super.onDestroyView()
    }

    companion object {
        private const val CURRENT_QUESTION_POS = "current_question_pos"
        private const val VIEW_RESULT_TYPE = "view_result_type"
        private const val VIEW_RESULT_DATA = "view_result_data"

        fun newInstance(num: Int, viewResultType: String, viewResultData: ViewResultData?): ViewResultsTabFragment {
            val fragment = ViewResultsTabFragment()
            val args = Bundle()
            args.putInt(CURRENT_QUESTION_POS, num)
            args.putString(VIEW_RESULT_TYPE, viewResultType)
            args.putParcelable(VIEW_RESULT_DATA, viewResultData)
            fragment.arguments = args
            return fragment
        }
    }
}