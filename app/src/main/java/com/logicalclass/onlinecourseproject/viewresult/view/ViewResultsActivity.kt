package com.logicalclass.onlinecourseproject.viewresult.view

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.repository.AppService
import com.logicalclass.onlinecourseproject.repository.RxUtils
import com.logicalclass.onlinecourseproject.service.MyFirebaseMessagingService.Companion.EXAM_ID
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.utils.show
import com.logicalclass.onlinecourseproject.utils.showApiParsingErrorSnack
import com.logicalclass.onlinecourseproject.viewresult.models.ViewResultResponse
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_exam_result_analytics.*

class ViewResultsActivity : AppCompatActivity(), TabLayout.OnTabSelectedListener {
    private var tokenid2: String? = null
    private var examId: String? = null
    private var viewResultPagerAdapter: ViewResultPagerAdapter? = null
    private var disposable: CompositeDisposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exam_result_analytics)
        disposable = CompositeDisposable()
        tokenid2 = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.TOKEN_ID_2)
        if (intent.extras != null) {
            examId = intent.getStringExtra(EXAM_ID)
        }
        setToolbarData()
        requestForAnalyticsDetailsNew()
    }

    private fun setToolbarData() {
        supportActionBar?.title = getString(R.string.title_view_results)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun requestForAnalyticsDetailsNew() {
        disposable?.add(AppService.create().getMockTestAnalyticsDetails(
                tokenid2,
                examId
        ).subscribeOn(RxUtils.ioThread())
                .doOnSubscribe {
                    showProgress()
                }.observeOn(RxUtils.androidThread())
                .subscribe({ response: ViewResultResponse ->
                    handleViewResultResponse(response)
                }, {
                    rootView.showApiParsingErrorSnack()
                })
        )
    }

    private fun handleViewResultResponse(response: ViewResultResponse) {
        if (response.status == 200) {
            setViewpagerData(response)
        }
        hideProgress()
    }

    private fun setViewpagerData(response: ViewResultResponse?) {
        viewResultPagerAdapter = ViewResultPagerAdapter(this, response?.data)
        pager.adapter = viewResultPagerAdapter
        TabLayoutMediator(tabLayout, pager) { tab, position ->
            tab.text = TAB_TITLES[position]
        }.attach()
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL
        tabLayout.addOnTabSelectedListener(this)
        tabLayout.show()
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun showProgress() {
        rlProgress.show()
    }

    override fun onTabSelected(tab: TabLayout.Tab?) {
        pager.currentItem = tab?.position ?: 0
    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {}
    override fun onTabReselected(tab: TabLayout.Tab?) {}
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val TYPE_CHAPTER_WISE = "Chapter Wise"
        const val TYPE_TOPIC_ANALYSIS = "Topic Analysis"
        const val TYPE_ANALYTICAL_ABILITIES = "Analytical Abilities"

        val TAB_TITLES = arrayOf(TYPE_CHAPTER_WISE, TYPE_TOPIC_ANALYSIS, TYPE_ANALYTICAL_ABILITIES)
    }

    override fun onBackPressed() {
        if (intent?.getBooleanExtra("isFromMockTest", false) == true) {
            val examResultsIntent = Intent(this, ExamResultsActivity::class.java)
            startActivity(examResultsIntent)
        }
        super.onBackPressed()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.clear()
    }
}