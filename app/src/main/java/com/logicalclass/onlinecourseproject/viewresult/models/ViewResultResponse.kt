package com.logicalclass.onlinecourseproject.viewresult.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.repository.Auth
import kotlinx.android.parcel.Parcelize

data class ViewResultResponse(
        val authData: Auth,
        @SerializedName("data")
        val data: ViewResultData,
        val msg: String,
        val status: Int,
        val token: String
)

@Parcelize
data class ViewResultData(
        val about: String,
        val allSubjects: List<String>,
        val analytics: List<AnalyticalAbilitiesData>?,
        val chapterWise: List<ChapterWiseData>?,
        @SerializedName("class")
        val className: String,
        val end: String,
        val examName: String,
        val start: String,
        val studentImg: String,
        val studentName: String,
        val timePerQuestion: Double,
        val topicWise: List<TopicWiseAnalyticsData>?,
        val totalTime: String
) : Parcelable

@Parcelize
data class ChapterWiseData(
        val attemptedCh: String?,
        val attemptedChPer: String?,
        val chapter: String,
        val negativeMarkCh: String,
        val notAttemptedCh: String?,
        val notAttemptedChPer: String?,
        val performanceCh: String,
        val rightCh: String?,
        val rightChPer: String?,
        val score: String,
        val wrongCh: String?,
        val wrongChPer: String?
) : Parcelable {

    fun getRightPercentageText(): String {
        return "$rightCh( $rightChPer % )"
    }

    fun getWrongPercentageText(): String {
        return "$wrongCh( $wrongCh % )"
    }

    fun getAttemptedPercentageText(): String {
        return "$attemptedCh( $attemptedChPer % )"
    }

    fun getNotAttemptedPercentageText(): String {
        return "$notAttemptedCh( $notAttemptedChPer % )"
    }

    fun getRightProgress(): Int {
        return rightChPer?.toInt() ?: 0
    }

    fun getWrongProgress(): Int {
        return wrongChPer?.toInt() ?: 0
    }

    fun getAttemptedProgress(): Int {
        return attemptedChPer?.toInt() ?: 0
    }

    fun getNotAttemptedProgress(): Int {
        return notAttemptedChPer?.toInt() ?: 0
    }

    fun getBorderLineColor(): Int {
        when (performanceCh) {
            "Very Poor" -> {
                return R.color.very_poor_color
            }
            "Poor" -> {
                return R.color.poor_color
            }
            "Average",
            "Weak" -> {
                return R.color.weak_color
            }
            "Good" -> {
                return R.color.good_color
            }
            "Excellent",
            "Vary Good",
            "Very Good" -> {
                return R.color.very_good_color
            }
            else -> {
                return R.color.transparent
            }
        }
    }
}

@Parcelize
data class TopicWiseAnalyticsData(
        val chapter: String?,
        val negativeMarkTopic: String?,
        val notTopicAttempted: Int?,
        val notTopicAttemptedPer: Int?,
        val performanceTopic: String,
        val scoreTopic: Int?,
        val timeSaverSucker: String?,
        val topic: String?,
        val topicAttempted: Int?,
        val topicAttemptedPer: Int?,
        val topicRight: Int?,
        val topicRightPer: Int?,
        val topicWrong: Int?,
        val topicWrongPer: Int?
) : Parcelable {

    fun getRightPercentageText(): String {
        return "$topicRight( $topicRightPer % )"
    }

    fun getWrongPercentageText(): String {
        return "$topicWrong( $topicWrongPer % )"
    }

    fun getAttemptedPercentageText(): String {
        return "$topicAttempted( $topicAttemptedPer % )"
    }

    fun getNotAttemptedPercentageText(): String {
        return "$notTopicAttempted( $notTopicAttemptedPer % )"
    }

    fun getRightProgress(): Int {
        return topicRightPer ?: 0
    }

    fun getWrongProgress(): Int {
        return topicWrongPer ?: 0
    }

    fun getAttemptedProgress(): Int {
        return topicAttemptedPer ?: 0
    }

    fun getNotAttemptedProgress(): Int {
        return notTopicAttemptedPer ?: 0
    }

    fun getBorderLineColor(): Int {
        when (performanceTopic) {
            "Very Poor" -> {
                return R.color.very_poor_color
            }
            "Poor" -> {
                return R.color.poor_color
            }
            "Average",
            "Weak" -> {
                return R.color.weak_color
            }
            "Good" -> {
                return R.color.good_color
            }
            "Excellent",
            "Vary Good",
            "Very Good" -> {
                return R.color.very_good_color
            }
            else -> {
                return R.color.transparent
            }
        }
    }
}

@Parcelize
data class AnalyticalAbilitiesData(
        val attempted: String?,
        val attemptedPer: String?,
        val key: String?,
        val notAttempted: String?,
        val notAttemptedPer: String?,
        val right: String?,
        val rightPer: String?,
        val wrong: String?,
        val wrongPer: String?
) : Parcelable {

    fun getRightPercentageText(): String {
        return "$right( $rightPer % )"
    }

    fun getWrongPercentageText(): String {
        return "$wrong( $wrongPer % )"
    }

    fun getAttemptedPercentageText(): String {
        return "$attempted( $attemptedPer % )"
    }

    fun getNotAttemptedPercentageText(): String {
        return "$notAttempted( $notAttemptedPer % )"
    }

    fun getRightProgress(): Int {
        return rightPer?.toInt() ?: 0
    }

    fun getWrongProgress(): Int {
        return wrongPer?.toInt() ?: 0
    }

    fun getAttemptedProgress(): Int {
        return attemptedPer?.toInt() ?: 0
    }

    fun getNotAttemptedProgress(): Int {
        return notAttemptedPer?.toInt() ?: 0
    }
}