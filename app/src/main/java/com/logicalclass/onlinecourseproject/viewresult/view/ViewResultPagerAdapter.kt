package com.logicalclass.onlinecourseproject.viewresult.view

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.logicalclass.onlinecourseproject.viewresult.view.ViewResultsActivity.Companion.TAB_TITLES
import com.logicalclass.onlinecourseproject.viewresult.models.ViewResultData

class ViewResultPagerAdapter(fm: FragmentActivity, private val viewResultData: ViewResultData?) : FragmentStateAdapter(fm) {

    override fun getItemCount(): Int = TAB_TITLES.size

    override fun createFragment(position: Int): Fragment {
        return ViewResultsTabFragment.newInstance(position, TAB_TITLES[position], viewResultData)
    }
}