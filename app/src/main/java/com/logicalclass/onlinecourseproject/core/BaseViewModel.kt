package com.logicalclass.onlinecourseproject.core

import android.app.Application
import androidx.annotation.StringRes
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel(application: Application) : AndroidViewModel(application) {

    private val app: Application = application

    val showProgress = MutableLiveData<Boolean>()
    val showAlert = MutableLiveData<String>()
    val showSnackBar = MutableLiveData<String>()
    val showToast = MutableLiveData<String>()
    val disposable: CompositeDisposable = CompositeDisposable()

    fun getString(@StringRes resource: Int): String = app.resources.getString(resource)

    override fun onCleared() {
        showProgress.postValue(null)
        showAlert.postValue(null)
        showSnackBar.postValue(null)
        showToast.postValue(null)
        disposable.clear()
        super.onCleared()
    }
}