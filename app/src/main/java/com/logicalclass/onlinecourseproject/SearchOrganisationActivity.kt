package com.logicalclass.onlinecourseproject

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.logicalclass.onlinecourseproject.authentication.view.LoginActivity
import com.logicalclass.onlinecourseproject.models.SearchOrgResponse
import com.logicalclass.onlinecourseproject.repository.AppService
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.androidThread
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.ioThread
import com.logicalclass.onlinecourseproject.utils.*
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_search_org.*

class SearchOrganisationActivity : AppCompatActivity(), ForceUpdateChecker.OnUpdateNeededListener {
    private var disposable: CompositeDisposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_org)
        setToolbarData()
        disposable = CompositeDisposable()
        btnSubmit.setOnClickListener {
            hideKeyboard()
            val orgName = edtSearch.text.toString().trim()
            if (orgName.isNotEmpty()) {
                requestForSearchOrganization(orgName)
            } else {
                rootView.showSnackMessage(getString(R.string.please_enter_organization_name))
            }
        }

        if (BuildConfig.DEBUG) {
            edtSearch.setText("demoorganisation")
        }
        checkPlayStoreUpdate()
    }

    private fun checkPlayStoreUpdate() {
        if (!BuildConfig.DEBUG)
            ForceUpdateChecker.with(this).onUpdateNeeded(this).check()
    }

    private fun setToolbarData() {
        supportActionBar?.title = getString(R.string.title_search_organization)
    }

    private fun requestForSearchOrganization(orgName: String) {
        disposable?.clear()
        disposable?.add(
            AppService.create().getCheckOrganisation(orgName)
                .subscribeOn(ioThread())
                .doOnSubscribe {
                    showProgress()
                }.observeOn(androidThread())
                .subscribe({ response: SearchOrgResponse ->
                    handleOnlineClassesResponse(response)
                }, {
                    hideProgress()
                    rootView.showApiParsingErrorSnack()
                })
        )
    }

    private fun handleOnlineClassesResponse(response: SearchOrgResponse) {
        if (response.status == 200) {
            SharedPrefsUtils.setStringPreference(this, SharedPrefsUtils.ORG_NAME, response.orgName)

            val intent = Intent(this, LoginActivity::class.java)
            val bundle = Bundle()
            bundle.putParcelable("organisation_obj", response)
            intent.putExtras(bundle)
            startActivity(intent)
            finish()
        } else {
            rootView.showSnackMessage(response.message ?: getString(R.string.something_went_wrong))
        }
        hideProgress()
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun showProgress() {
        rlProgress.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.clear()
    }

    override fun onUpdateNeeded(updateUrl: String?, mandatoryUpdate: Boolean?) {
        val builder = AlertDialog.Builder(this)
        if (mandatoryUpdate == true) {
            builder.setCancelable(false)
        } else {
            builder.setCancelable(true)
            builder.setNegativeButton("No, Thanks") { dialog, _ ->
                dialog.dismiss()
            }
        }
        builder.setTitle("New version available")
        builder.setPositiveButton("Update") { dialog, _ ->
            dialog.dismiss()
            redirectStore(updateUrl)
        }
        builder.setMessage(getString(R.string.update_available_description_dialog))
            .show()
    }

    private fun redirectStore(updateUrl: String?) {
        val intent = Intent(Intent.ACTION_VIEW).setData(Uri.parse(updateUrl))
        startActivity(intent)
    }
}