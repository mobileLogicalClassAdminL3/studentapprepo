/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.logicalclass.onlinecourseproject.videotutoriales.view

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.logicalclass.onlinecourseproject.*
import com.logicalclass.onlinecourseproject.utils.FileUtils
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.utils.show
import com.logicalclass.onlinecourseproject.utils.showSnackMessage
import kotlinx.android.synthetic.main.activity_player.*
import kotlinx.android.synthetic.main.exo_playback_control_view.*
import java.io.File

/**
 * A fullscreen activity to play audio or video streams.
 */
class PlayerActivity : AppCompatActivity() {
    private var player: SimpleExoPlayer? = null
    private var playWhenReady = true
    private var currentWindow = 0
    private var playbackPosition: Long = 0
    private var fullScreenVideo = false
    private var fullScreenPDF = false

    private val playerStateListener = object : Player.EventListener {

        override fun onPlayerStateChanged(
                playWhenReady: Boolean,
                playbackState: Int
        ) {
            val stateString: String = when (playbackState) {
                ExoPlayer.STATE_IDLE -> {
                    progressBarVideo.hide()
                    "ExoPlayer.STATE_IDLE -"
                }
                ExoPlayer.STATE_BUFFERING -> {
                    progressBarVideo.show()
                    "ExoPlayer.STATE_BUFFERING -"
                }
                ExoPlayer.STATE_READY -> {
                    progressBarVideo.hide()
                    "ExoPlayer.STATE_READY -"
                }
                ExoPlayer.STATE_ENDED -> {
                    progressBarVideo.hide()
                    "ExoPlayer.STATE_ENDED -"
                }
                else -> {
                    progressBarVideo.hide()
                    "UNKNOWN_STATE -"
                }
            }
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "changed state to $stateString playWhenReady: $playWhenReady")
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player)
        setToolbarData()
        bntExoFullscreen.setOnClickListener {
            if (fullScreenVideo) {
                exitVideoFulScreenMode()
            } else {
                enterVideoFullScreenMode()
            }
        }

        /***PDF Code***/
        PRDownloader.initialize(applicationContext)
        initiatePDFLoading()
        bntPDFFullscreen.setOnClickListener {
            if (fullScreenPDF) {
                exitPDFFulScreenMode()
            } else {
                enterPDFFullScreenMode()
            }
        }
    }

    private fun setToolbarData() {
        /*supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)*/
        supportActionBar?.hide()
    }

    private fun enterPDFFullScreenMode() {
        bntPDFFullscreen.setImageDrawable(ContextCompat.getDrawable(this@PlayerActivity, R.drawable.ic_fullscreen_close))
        playerView.hide()
        player?.playWhenReady = false
        fullScreenPDF = true
    }

    private fun exitPDFFulScreenMode() {
        bntPDFFullscreen.setImageDrawable(ContextCompat.getDrawable(this@PlayerActivity, R.drawable.ic_fullscreen_open))
        playerView.show()
        player?.playWhenReady = true
        fullScreenPDF = false
    }

    private fun enterVideoFullScreenMode() {
        bntExoFullscreen.setImageDrawable(ContextCompat.getDrawable(this@PlayerActivity, R.drawable.ic_fullscreen_close))
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
        /*supportActionBar?.hide()*/
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        val params = playerView.layoutParams as ConstraintLayout.LayoutParams
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.MATCH_PARENT
        playerView.layoutParams = params
        pdfView.hide()
        fullScreenVideo = true
    }

    private fun exitVideoFulScreenMode() {
        bntExoFullscreen.setImageDrawable(ContextCompat.getDrawable(this@PlayerActivity, R.drawable.ic_fullscreen_open))
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        /*supportActionBar?.show()*/
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        val params = playerView.layoutParams as ConstraintLayout.LayoutParams
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = (250 * applicationContext.resources.displayMetrics.density).toInt()
        playerView.layoutParams = params
        pdfView.show()
        fullScreenVideo = false
    }

    private fun initializePlayer() {
        player = SimpleExoPlayer.Builder(this).build()
        playerView?.player = player
        playerView?.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIT
        val videoURL = intent?.getStringExtra(VIDEO_URL)
        val uri = Uri.parse(videoURL)
        val mediaSource = buildMediaSource(uri)
        player?.playWhenReady = playWhenReady
        player?.seekTo(currentWindow, playbackPosition)
        player?.addListener(playerStateListener)
        player?.prepare(mediaSource, false, false)
    }

    private fun releasePlayer() {
        if (player != null) {
            playbackPosition = player?.currentPosition ?: 0
            currentWindow = player?.currentWindowIndex ?: 0
            playWhenReady = player?.playWhenReady ?: false
            player?.removeListener(playerStateListener)
            player?.release()
            player = null
        }
    }

    private fun buildMediaSource(uri: Uri): MediaSource {
        val dataSourceFactory: DataSource.Factory = DefaultDataSourceFactory(
                this,
                Util.getUserAgent(applicationContext, applicationContext.getString(R.string.app_name))
        )
        return ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri)
    }

    @SuppressLint("InlinedApi")
    private fun hideSystemUi() {
        playerView?.systemUiVisibility = (View.SYSTEM_UI_FLAG_LOW_PROFILE
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
    }

    public override fun onStart() {
        super.onStart()
        if (Util.SDK_INT > 23) {
            initializePlayer()
        }
    }

    public override fun onResume() {
        super.onResume()
        hideSystemUi()
        if (Util.SDK_INT <= 23 || player == null) {
            initializePlayer()
        }
    }

    public override fun onPause() {
        super.onPause()
        if (Util.SDK_INT <= 23) {
            releasePlayer()
        }
    }

    public override fun onStop() {
        super.onStop()
        if (Util.SDK_INT > 23) {
            releasePlayer()
        }
    }

    /*************************************  PDF Code *********************************************/
    private fun initiatePDFLoading() {
        progressBarPDF.show()
        downloadPdfFromInternet(
                intent?.getStringExtra(PDF_URL),
                FileUtils.getRootDirPath(this),
                STATIC_PDF_FILE_NAME
        )
    }

    @Suppress("SameParameterValue")
    private fun downloadPdfFromInternet(url: String?, dirPath: String, fileName: String) {
        PRDownloader.download(
                url,
                dirPath,
                fileName
        ).build()
                .start(object : OnDownloadListener {
                    override fun onDownloadComplete() {
                        val downloadedFile = File(dirPath, fileName)
                        progressBarPDF.hide()
                        showPdfFromFile(downloadedFile)
                    }

                    override fun onError(error: Error?) {
                        pdfView.showSnackMessage("Error in downloading file : $error")
                    }
                })
    }

    private fun showPdfFromFile(file: File) {
        pdfView.fromFile(file)
                .password(null)
                .defaultPage(0)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .enableDoubletap(true)
                .onPageError { page, _ ->
                    pdfView.showSnackMessage("Error at page: $page")
                }
                .load()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        private val TAG = PlayerActivity::class.java.name
        private const val STATIC_PDF_FILE_NAME = "TutorialPDFFile"
        const val PDF_URL = "pdf_url"
        const val VIDEO_URL = "video_url"
        const val TOPIC_NAME = "topic_name"
    }
}
