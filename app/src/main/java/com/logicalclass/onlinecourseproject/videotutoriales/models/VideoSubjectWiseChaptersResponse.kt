package com.logicalclass.onlinecourseproject.videotutoriales.models

import com.google.gson.annotations.SerializedName
import com.logicalclass.onlinecourseproject.models.SubjectChapterModel
import com.logicalclass.onlinecourseproject.models.SubjectSection
import com.logicalclass.onlinecourseproject.models.getChaptersList
import com.logicalclass.onlinecourseproject.repository.Auth

data class VideoSubjectWiseChaptersResponse(
    val authData: Auth,
    @SerializedName("data")
    val data: List<SubjectChapterModel<String>>,
    val msg: String,
    val status: Int,
    val token: String
) {
    fun getSectionHeadersList(): List<SubjectSection?>? {
        val sectionHeaders: ArrayList<SubjectSection> = ArrayList()
        data.forEach { model ->
            sectionHeaders.add(SubjectSection(model.subject, getChaptersList(model.chapters)))
        }
        return sectionHeaders
    }

    fun getSubject(subjectPos: Int): SubjectChapterModel<String>? {
        if (!data.isNullOrEmpty()) {
            return data[subjectPos]
        }
        return null
    }

    fun getChapter(subjectPos: Int, chapterPos: Int): String? {
        if (!data.isNullOrEmpty()) {
            return data[subjectPos].chapters[chapterPos]
        }
        return null
    }
}





