package com.logicalclass.onlinecourseproject.videotutoriales.models

import com.google.gson.annotations.SerializedName
import com.logicalclass.onlinecourseproject.models.TopicModel
import com.logicalclass.onlinecourseproject.repository.Auth

data class VideoTopicsResponse(
    val authData: Auth,
    @SerializedName("data")
    val data: VideoTopicsData,
    val msg: String,
    val status: Int,
    val token: String
)

data class VideoTopicsData(
    val basicAssetsUrl: String,
    val chapter: String,
    @SerializedName("class")
    val className: String,
    val studyMaterials: List<TopicModel>,
    val subject: String
)