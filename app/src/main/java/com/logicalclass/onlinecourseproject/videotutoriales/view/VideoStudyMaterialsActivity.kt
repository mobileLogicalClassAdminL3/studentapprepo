package com.logicalclass.onlinecourseproject.videotutoriales.view

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.ItemAnimator
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.repository.AppService
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.androidThread
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.ioThread
import com.logicalclass.onlinecourseproject.studymaterials.adapters.SubjectChapterAdapter
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.utils.show
import com.logicalclass.onlinecourseproject.utils.showApiParsingErrorSnack
import com.logicalclass.onlinecourseproject.videotutoriales.models.VideoSubjectWiseChaptersResponse
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_announcements.*

class VideoStudyMaterialsActivity : AppCompatActivity() {

    private lateinit var adapter: SubjectChapterAdapter
    private var StudentAuth: String? = null
    private var orgAuth: String? = null
    private var tokenid2: String? = null
    private var disposable: CompositeDisposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subjects_chapters)
        setToolbarData()
        disposable = CompositeDisposable()
        tokenid2 = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.TOKEN_ID_2)
        StudentAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.STUDENT_AUTH)
        orgAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.ORG_AUTH)
        requestForStudyMaterials()
    }

    private fun setToolbarData() {
        supportActionBar?.title = getString(R.string.title_video_tutorials)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun requestForStudyMaterials() {
        disposable?.add(
            AppService.create().getVideoSubjectWiseChapters(
                tokenid2,
                StudentAuth,
                orgAuth,
                SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.CLASS_NAME)
            ).subscribeOn(ioThread())
                .doOnSubscribe {
                    showProgress()
                }.observeOn(androidThread())
                .subscribe({ response: VideoSubjectWiseChaptersResponse ->
                    handleSubjectChaptersResponse(response)
                }, {
                    rootView.showApiParsingErrorSnack()
                    llEmptyData.show()
                })
        )
    }

    private fun handleSubjectChaptersResponse(response: VideoSubjectWiseChaptersResponse) {
        if (response.status == 200 && response.data.isNotEmpty()) {
            setListAdapter(response)
        } else {
            llEmptyData.show()
        }
        hideProgress()
    }

    private fun setListAdapter(response: VideoSubjectWiseChaptersResponse) {
        val layoutManager = LinearLayoutManager(this)
        rvList.layoutManager = layoutManager
        rvList.setHasFixedSize(true)
        val animator: ItemAnimator? = rvList.itemAnimator
        if (animator is DefaultItemAnimator) {
            animator.supportsChangeAnimations = false
        }
        adapter = SubjectChapterAdapter(response.getSectionHeadersList())
        adapter.setChildClickListener { groupPos, childPos, _ ->
            val subject = response.getSubject(groupPos)?.subject
            val chapter = response.getChapter(groupPos, childPos)
            val intent = Intent(this, VideoTopicsActivity::class.java)
            intent.putExtra("subject", subject)
            intent.putExtra("chapter", chapter)
            startActivity(intent)
        }
        rvList.adapter = adapter
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun showProgress() {
        rlProgress.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.clear()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
