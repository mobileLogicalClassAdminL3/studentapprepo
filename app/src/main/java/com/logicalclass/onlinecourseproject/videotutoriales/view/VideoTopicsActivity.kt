package com.logicalclass.onlinecourseproject.videotutoriales.view

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.logicalclass.onlinecourseproject.BuildConfig
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.models.TopicModel
import com.logicalclass.onlinecourseproject.repository.AppService
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.androidThread
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.ioThread
import com.logicalclass.onlinecourseproject.subjectchapters.TopicsListAdapter
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.utils.show
import com.logicalclass.onlinecourseproject.utils.showApiParsingErrorSnack
import com.logicalclass.onlinecourseproject.videotutoriales.models.VideoTopicsResponse
import com.logicalclass.onlinecourseproject.videotutoriales.view.PlayerActivity.Companion.PDF_URL
import com.logicalclass.onlinecourseproject.videotutoriales.view.PlayerActivity.Companion.TOPIC_NAME
import com.logicalclass.onlinecourseproject.videotutoriales.view.PlayerActivity.Companion.VIDEO_URL
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_topics.*

class VideoTopicsActivity : AppCompatActivity(), TopicsListAdapter.ICallBack {
    private var subject: String? = null
    private var chapter: String? = null
    private lateinit var topicsListAdapter: TopicsListAdapter
    private var studentAuth: String? = null
    private var orgAuth: String? = null
    private var tokenid2: String? = null
    private var disposable: CompositeDisposable? = null
    private var baseURLForFile: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_topics)
        getIntentData()
        setToolbarData()
        disposable = CompositeDisposable()
        tokenid2 = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.TOKEN_ID_2)
        studentAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.STUDENT_AUTH)
        orgAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.ORG_AUTH)
        requestForAnnouncementsList()
    }

    private fun getIntentData() {
        subject = intent?.getStringExtra("subject")
        chapter = intent?.getStringExtra("chapter")

        /*txtSubjectName.text = subject
        txtChapterName.text = chapter*/
    }

    private fun setToolbarData() {
        supportActionBar?.title = subject
        supportActionBar?.subtitle = chapter
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun requestForAnnouncementsList() {
        disposable?.add(
            AppService.create().getVideoStudyMaterialTopics(
                tokenid2,
                studentAuth,
                orgAuth,
                SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.CLASS_NAME),
                subject,
                chapter
            ).subscribeOn(ioThread())
                .doOnSubscribe {
                    showProgress()
                }.observeOn(androidThread())
                .subscribe({ response: VideoTopicsResponse ->
                    handleTopicsListResponse(response)
                }, {
                    rootView.showApiParsingErrorSnack()
                    llEmptyData.show()
                })
        )
    }

    private fun handleTopicsListResponse(response: VideoTopicsResponse) {
        if (response.status == 200 && response.data.studyMaterials.isNotEmpty()) {
            val list = response.data.studyMaterials
            baseURLForFile = response.data.basicAssetsUrl
            val layoutManager = LinearLayoutManager(this)
            topicsListAdapter = TopicsListAdapter(this, list, this)
            rvList.layoutManager = layoutManager
            rvList.adapter = topicsListAdapter
        } else {
            llEmptyData.show()
        }
        hideProgress()
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun showProgress() {
        rlProgress.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.clear()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onTopicSelected(position: Int, resultData: TopicModel?) {
        val videoURL = BuildConfig.HTTP_KEY + baseURLForFile + "/" + resultData?.videoFile
        val pdfURL = BuildConfig.HTTP_KEY + baseURLForFile + "/" + resultData?.pdfFile
        val intent = Intent(this, PlayerActivity::class.java)
        intent.putExtra(PDF_URL, pdfURL)
        intent.putExtra(VIDEO_URL, videoURL)
        intent.putExtra(TOPIC_NAME, resultData?.getNameOfTheTopic())
        startActivity(intent)
    }

}
