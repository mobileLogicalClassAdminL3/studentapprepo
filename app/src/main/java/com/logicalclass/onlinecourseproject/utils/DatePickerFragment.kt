/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.logicalclass.onlinecourseproject.utils

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import com.logicalclass.onlinecourseproject.attendance.view.DayWiseAttendance
import com.logicalclass.onlinecourseproject.onlinediary.view.DigitalDiaryActivity
import java.util.*


/**
 * A simple [Fragment] subclass for the date picker.
 */
class DatePickerFragment : DialogFragment(), DatePickerDialog.OnDateSetListener {
    private var minDate: Long? = null
    private var maxDate: Long? = null

    /**
     * Creates the date picker dialog with the current date from Calendar.
     *
     * @param savedInstanceState Saved instance state bundle
     * @return DatePickerDialog     The date picker dialog
     */
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog { // Use the current date as the default date in the picker.
        val c = Calendar.getInstance()
        val year = c[Calendar.YEAR]
        val month = c[Calendar.MONTH]
        val day = c[Calendar.DAY_OF_MONTH]
        // Create a new instance of DatePickerDialog and return it.
        val datePickerDialog = DatePickerDialog(activity!!, this, year, month, day)

        minDate?.let {
            datePickerDialog.datePicker.minDate = it
        }
        maxDate?.let {
            datePickerDialog.datePicker.maxDate = it
        }
        return datePickerDialog
    }

    fun setMaxDate(maxDate: Long?) {
        this.maxDate = maxDate
    }

    fun setMinDate(minDate: Long?) {
        this.minDate = minDate
    }

    /**
     * Grabs the date and passes it to processDatePickerResult().
     *
     * @param datePicker The date picker view
     * @param year       The year chosen
     * @param month      The month chosen
     * @param day        The day chosen
     */
    override fun onDateSet(datePicker: DatePicker, year: Int, month: Int, day: Int) {
        if (activity is DayWiseAttendance) {
            val activity = activity as DayWiseAttendance?
            activity?.processDatePickerResult(year, month, day)
        } else if (activity is DigitalDiaryActivity) {
            val activity = activity as DigitalDiaryActivity?
            activity?.processDatePickerResult(year, month, day)
        }
    }
}