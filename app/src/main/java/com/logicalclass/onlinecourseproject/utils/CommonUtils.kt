package com.logicalclass.onlinecourseproject.utils

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.text.Html
import android.text.Spanned
import android.text.TextUtils
import android.util.Base64
import android.util.DisplayMetrics
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat.startActivity
import androidx.core.content.res.ResourcesCompat
import com.logicalclass.onlinecourseproject.BuildConfig
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import kotlin.math.ceil

object CommonUtils {

    fun loadBitmapFromView(v: View): Bitmap {
        v.measure(
            RelativeLayout.LayoutParams.WRAP_CONTENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        val b = Bitmap.createBitmap(v.measuredWidth, v.measuredHeight, Bitmap.Config.ARGB_8888)
        val c = Canvas(b)
        v.layout(v.left, v.top, v.right, v.bottom)
        v.draw(c)
        return b
    }

    fun isNetworkAvailable(context: Context): Boolean {
        val connManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val capabilities = connManager.getNetworkCapabilities(connManager.activeNetwork)
            if (capabilities != null) {
                when {
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                    else -> false
                }
            } else false
        } else connManager.activeNetworkInfo?.isConnected ?: false
    }

    fun browse(mActivity: Activity, url: String?) {
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        mActivity.startActivity(i)
    }

    fun navigateToPlayStore(context: Context) {
        val packageName = context.packageName
        try {
            startActivity(
                context,
                Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")), null
            )
        } catch (e: ActivityNotFoundException) {
            startActivity(
                context,
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                ), null
            )
        }
    }

    fun getHtmlData(content: String?): Spanned {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(content, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(content)
        }
    }

    fun dial(mActivity: Context, number: String) {
        val call = Uri.parse("tel:$number")
        val surf = Intent(Intent.ACTION_DIAL, call)
        mActivity.startActivity(surf)
    }

    fun mailto(mActivity: Activity, email: String?, subject: String?, content: String?) {
        val mIntent = Intent(Intent.ACTION_SENDTO)
        mIntent.data = Uri.parse("mailto:")
        mIntent.putExtra(Intent.EXTRA_EMAIL, email)
        if (!TextUtils.isEmpty(subject)) mIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        if (!TextUtils.isEmpty(content)) mIntent.putExtra(Intent.EXTRA_TEXT, content)
        if (mIntent.resolveActivity(mActivity.packageManager) != null) {
            mActivity.startActivity(mIntent)
        }
    }

    fun shareText(mActivity: Activity, shareBody: String?, subject: String?) {
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        if (!TextUtils.isEmpty(subject)) sharingIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
        mActivity.startActivity(Intent.createChooser(sharingIntent, "Share Using..."))
    }

    /**
     * Hides the soft keyboard
     */
    fun hideSoftKeyboard(mActivity: Activity) {
        try {
            val focusedView = mActivity.currentFocus
            if (focusedView != null) {
                val inputMethodManager =
                    mActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(focusedView.windowToken, 0)
            } else {
                val editText = EditText(mActivity)
                val inputMethodManager =
                    mActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(editText.windowToken, 0)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Shows the soft keyboard
     */
    fun showSoftKeyboard(mActivity: Activity, mView: View) {
        try {
            val inputMethodManager =
                mActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mView.requestFocus()
            inputMethodManager.showSoftInput(mView, 0)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    // Find todays date
    val currentTimeStamp: String?
        get() = try {
            val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            dateFormat.format(Date())
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }

    // Find todays date
    val currentDateStamp: String?
        get() = try {
            val dateFormat = SimpleDateFormat("dd-MM-yyyy")
            dateFormat.format(Date())
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }

    /**
     * Load json data from asset
     */
    fun loadJSONFromAsset(mContext: Context, fileName: String?): String? {
        var json = ""
        json = try {
            val `is` = mContext.assets.open(fileName!!)
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            String(buffer)
        } catch (ex: Exception) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    fun getFbHashKey(mContext: Context): String? {
        var hashKey: String? = null
        try {
            val info = mContext.packageManager.getPackageInfo(
                BuildConfig.APPLICATION_ID,
                PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                hashKey = Base64.encodeToString(md.digest(), Base64.DEFAULT)
            }
        } catch (e: PackageManager.NameNotFoundException) {
        } catch (e: NoSuchAlgorithmException) {
        }
        return hashKey
    }

    fun watchYoutubeVideo(mContext: Context, id: String?) {
        val videoId = getVideoIdFromYoutubeUrl(id)
        val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$videoId"))
        try {
            mContext.startActivity(appIntent)
        } catch (ex: ActivityNotFoundException) {
            val webIntent =
                Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=$videoId"))
            mContext.startActivity(webIntent)
        }
    }

    fun getVideoIdFromYoutubeUrl(url: String?): String? {
        var videoId: String? = null
        val regex =
            "http(?:s)?:\\/\\/(?:m.)?(?:www\\.)?youtu(?:\\.be\\/|be\\.com\\/(?:watch\\?(?:feature=youtu.be\\&)?v=|v\\/|embed\\/|user\\/(?:[\\w#]+\\/)+))([^&#?\\n]+)"
        val pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(url)
        videoId = if (matcher.find()) {
            matcher.group(1)
        } else {
            url
        }
        return videoId
    }

    fun getScreenResolution(context: Context): String {
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val metrics = DisplayMetrics()
        display.getMetrics(metrics)
        val width = metrics.widthPixels
        val height = metrics.heightPixels
        return "{$width,$height}"
    }

    fun getScreenHeight(context: Context): Int {
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val metrics = DisplayMetrics()
        display.getMetrics(metrics)
        return metrics.heightPixels
    }

    fun getStatusBarHeight(context: Context): Int {
        val resources = context.resources
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        return if (resourceId > 0) resources.getDimensionPixelSize(resourceId) else ceil(((if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) 24 else 25) * resources.displayMetrics.density).toDouble())
            .toInt()
    }

    fun getDrawableFromName(context: Context, drawableName: String): Drawable? {
        val resources: Resources = context.resources
        val resourceId: Int = resources.getIdentifier(
            drawableName, "drawable",
            context.packageName
        )
        return ResourcesCompat.getDrawable(resources, resourceId, null)
    }
}