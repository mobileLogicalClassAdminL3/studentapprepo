package com.logicalclass.onlinecourseproject.utils

import android.app.Activity
import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.text.Html
import android.text.Spanned
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.google.android.material.snackbar.Snackbar
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.core.ViewModelFactory
import org.threeten.bp.DayOfWeek
import org.threeten.bp.temporal.WeekFields
import java.util.*

fun View.show() {
    visibility = View.VISIBLE
}

fun View.inVisible() {
    visibility = View.INVISIBLE
}

fun View.showOrHide(show: Boolean?) {
    show?.let {
        visibility = if (it) View.VISIBLE else View.GONE
    } ?: kotlin.run {
        visibility = View.GONE
    }
}

fun View.hide() {
    visibility = View.GONE
}

fun <T : ViewModel> getProviderFactory(viewModel: T): ViewModelFactory<T> =
    ViewModelFactory(viewModel)

inline fun <reified T : ViewModel> getViewModel(owner: ViewModelStoreOwner, viewModel: T): T {
    return ViewModelProvider(owner, getProviderFactory(viewModel)).get(T::class.java)
}

fun dpToPx(dp: Int, context: Context): Int =
    TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(),
        context.resources.displayMetrics
    ).toInt()

internal fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

internal inline fun Boolean?.orFalse(): Boolean = this ?: false

internal fun Context.getDrawableCompat(@DrawableRes drawable: Int) =
    ContextCompat.getDrawable(this, drawable)

internal fun Context.getColorCompat(@ColorRes color: Int) = ContextCompat.getColor(this, color)

internal fun TextView.setTextColorRes(@ColorRes color: Int) =
    setTextColor(context.getColorCompat(color))

fun daysOfWeekFromLocale(): Array<DayOfWeek> {
    val firstDayOfWeek = WeekFields.of(Locale.getDefault()).firstDayOfWeek
    var daysOfWeek = DayOfWeek.values()
    // Order `daysOfWeek` array so that firstDayOfWeek is at index 0.
    if (firstDayOfWeek != DayOfWeek.MONDAY) {
        val rhs = daysOfWeek.sliceArray(firstDayOfWeek.ordinal..daysOfWeek.indices.last)
        val lhs = daysOfWeek.sliceArray(0 until firstDayOfWeek.ordinal)
        daysOfWeek = rhs + lhs
    }
    return daysOfWeek
}

fun GradientDrawable.setCornerRadius(
    topLeft: Float = 0F,
    topRight: Float = 0F,
    bottomRight: Float = 0F,
    bottomLeft: Float = 0F
) {
    cornerRadii = arrayOf(
        topLeft, topLeft,
        topRight, topRight,
        bottomRight, bottomRight,
        bottomLeft, bottomLeft
    ).toFloatArray()
}

fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
    hideKeyboard(currentFocus ?: View(this))
}

fun Activity.showAlertMessage(
    title: String? = null,
    message: String
) {
    if (!isFinishing)
        AlertDialog.Builder(this).setMessage(message)
            .setTitle(title)
            .setPositiveButton("OK") { _, _ ->
            }.setCancelable(false)
            .show()
}

fun Activity.showAlertMessageWithAction(
    title: String? = null,
    message: String,
    callBack: () -> Unit
) {
    if (!isFinishing)
        AlertDialog.Builder(this).setMessage(message)
            .setTitle(title)
            .setPositiveButton("OK") { _, _ ->
                callBack()
            }.setCancelable(false)
            .show()
}

fun Activity.showConfirmationAlertMessageWithAction(
    title: String? = null,
    message: String,
    callBack: () -> Unit
) {
    if (!isFinishing)
        AlertDialog.Builder(this).setMessage(message)
            .setTitle(title)
            .setPositiveButton(resources.getString(R.string.yes)) { _, _ ->
                callBack()
            }.setNegativeButton(resources.getString(R.string.no)) { _, _ ->
            }.setCancelable(false)
            .show()
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun String.getHtmlText(): Spanned? {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        // FROM_HTML_MODE_LEGACY is the behaviour that was used for versions below android N
        // we are using this flag to give a consistent behaviour
        Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY);
    } else {
        Html.fromHtml(this);
    }
}

fun TextView.setHTMLText(txt: String?) {
    text = txt?.getHtmlText()
}

fun View.showApiParsingErrorSnack() {
    Snackbar.make(this, R.string.something_went_wrong, Snackbar.LENGTH_SHORT).show()
}

fun View.showApiErrorSnack() {
    Snackbar.make(this, R.string.something_went_wrong, Snackbar.LENGTH_SHORT).show()
}

fun View.showSnackMessage(message: String) {
    Snackbar.make(this, message, Snackbar.LENGTH_SHORT).show()
}

fun Int.dpToPx(displayMetrics: DisplayMetrics): Int = (this * displayMetrics.density).toInt()

fun Int.pxToDp(displayMetrics: DisplayMetrics): Int = (this / displayMetrics.density).toInt()