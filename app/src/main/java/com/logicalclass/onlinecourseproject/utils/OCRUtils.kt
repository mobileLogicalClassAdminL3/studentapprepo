package com.logicalclass.onlinecourseproject.utils

import android.graphics.Bitmap
import android.util.Log
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import java.util.concurrent.Executors

class OCRUtils(private val listener: Listener) {
    private val detector = TextRecognition.getClient(
        TextRecognizerOptions.Builder().setExecutor(Executors.newCachedThreadPool()).build()
    )

    fun detect(image: Bitmap) {
        InputImage.fromBitmap(image, 0).let {
            detector.process(it)
                .addOnSuccessListener { visionText ->
                    // Task completed successfully
                    Log.i("TAG", "Text : " + visionText.text + " , " + visionText.textBlocks.size)
                    listener.onSuccess(visionText.text)
                }
                .addOnFailureListener { exception ->
                    // Task failed with an exception
                    Log.e("TAG", "Text recognition error", exception)
                    listener.onError(exception)
                }
        }
    }

    interface Listener {
        fun onSuccess(text: String)
        fun onError(exception: Exception?)
    }

}