package com.logicalclass.onlinecourseproject.profile.view

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.logicalclass.onlinecourseproject.*
import com.logicalclass.onlinecourseproject.authentication.view.LoginActivity
import com.logicalclass.onlinecourseproject.profile.models.UserProfileResponse
import com.logicalclass.onlinecourseproject.repository.AppService
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.androidThread
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.ioThread
import com.logicalclass.onlinecourseproject.utils.*
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : AppCompatActivity() {

    private var tokenid2: String? = null
    private var disposable: CompositeDisposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        setToolbarData()
        disposable = CompositeDisposable()
        tokenid2 = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.TOKEN_ID_2)
        requestForProfile()
        setLogoutAction()
    }

    private fun setLogoutAction() {
        logoutBtn.setOnClickListener {
            showConfirmationDialog()
        }
    }

    private fun showConfirmationDialog() {
        AlertDialog.Builder(this)
            .setTitle("Confirmation")
            .setMessage("Are you sure you want to Logout?")
            .setPositiveButton(R.string.yes) { _, _ -> navigateToSearchOrganization() }
            .setNegativeButton(R.string.no, null)
            .show()
    }

    private fun navigateToSearchOrganization() {
        SharedPrefsUtils.clearPreferenceData(this)
        val intent = if (BuildConfig.FLAVOR == "logicalclass") {
            Intent(this@ProfileActivity, SearchOrganisationActivity::class.java)
        } else {
            Intent(this@ProfileActivity, LoginActivity::class.java)
        }
        intent.addFlags(
            Intent.FLAG_ACTIVITY_CLEAR_TOP
                    or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    or Intent.FLAG_ACTIVITY_NEW_TASK
        )
        startActivity(intent)
        finish()
    }

    private fun setToolbarData() {
        supportActionBar?.title = getString(R.string.title_profile)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun requestForProfile() {
        disposable?.add(
            AppService.create().getProfile(
                tokenid2
            ).subscribeOn(ioThread())
                .doOnSubscribe {
                    showProgress()
                }.observeOn(androidThread())
                .subscribe({ response: UserProfileResponse ->
                    handleUserProfileResponse(response)
                }, {
                    rootView.showApiParsingErrorSnack()
                })
        )
    }

    private fun handleUserProfileResponse(response: UserProfileResponse) {
        if (response.status == 200) {
            response.userProfile?.let { userProfile ->
                txtName.text = userProfile.name
                txtUniqueCode.text = userProfile.uniqueCode
                txtEmail.text = userProfile.email
                txtClass.text = userProfile.className
                txtContact.text = userProfile.contact
                Glide.with(this)
                    .load(if (userProfile.profileImg != null) "${BuildConfig.HTTP_KEY}${userProfile.profileImg}" else "")
                    .error(R.drawable.ic_empty_profile)
                    .into(imgProfile)
            }
        } else {
            rootView.showApiErrorSnack()
        }
        hideProgress()
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun showProgress() {
        rlProgress.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.clear()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
