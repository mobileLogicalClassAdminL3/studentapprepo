package com.logicalclass.onlinecourseproject.profile.models

import com.google.gson.annotations.SerializedName
import com.logicalclass.onlinecourseproject.repository.Auth

data class UserProfileResponse(
        val authData: Auth,
        @SerializedName("data")
        val userProfile: UserProfile?,
        val msg: String,
        val status: Int,
        val token: String
)

data class UserProfile(
        @SerializedName("class")
        val className: String,
        val contact: String,
        val email: String,
        val name: String,
        val profileImg: String?,
        val uniqueCode: String
)
