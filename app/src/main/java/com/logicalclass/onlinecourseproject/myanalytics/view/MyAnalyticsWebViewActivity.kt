package com.logicalclass.onlinecourseproject.myanalytics.view

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Bundle
import android.view.MenuItem
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.myanalytics.models.MyAnalyticsResponse
import com.logicalclass.onlinecourseproject.repository.AppService
import com.logicalclass.onlinecourseproject.repository.RxUtils
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.utils.show
import com.logicalclass.onlinecourseproject.utils.showApiParsingErrorSnack
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_topic_webview.*
import kotlinx.android.synthetic.main.activity_topic_webview.rlProgress
import kotlinx.android.synthetic.main.activity_topic_webview.rootView
import kotlinx.android.synthetic.main.activity_topics.*


class MyAnalyticsWebViewActivity : AppCompatActivity() {
    private var tokenid2: String? = null
    private var disposable: CompositeDisposable? = null

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_topic_webview)
        setToolbarData()
        disposable = CompositeDisposable()
        tokenid2 = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.TOKEN_ID_2)

        setupWebView()
        requestForMyAnalyticsData()
    }

    private fun setupWebView() {
        webView.settings.javaScriptEnabled = true
        webView.settings.loadWithOverviewMode = true
        webView.settings.useWideViewPort = true
        webView.settings.userAgentString =
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36";
        webView.settings.javaScriptCanOpenWindowsAutomatically = true
        webView.settings.builtInZoomControls = true

        webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                hideProgress()
            }

            override fun onReceivedError(
                view: WebView?,
                request: WebResourceRequest?,
                error: WebResourceError?
            ) {
                super.onReceivedError(view, request, error)
                if (!isFinishing) {
                    hideProgress()
                }
            }
        }
    }

    private fun requestForMyAnalyticsData() {
        showProgress()
        disposable?.add(
            AppService.create().getMyAnalytics(tokenid2).subscribeOn(RxUtils.ioThread())
                .doOnSubscribe {
                    showProgress()
                }.observeOn(RxUtils.androidThread())
                .subscribe({ response ->
                    handleMyAnalyticsResponse(response)
                }, {
                    hideProgress()
                    rootView.showApiParsingErrorSnack()
                    llEmptyData.show()
                })
        )
    }

    private fun handleMyAnalyticsResponse(response: MyAnalyticsResponse?) {
        response?.let {
            webView.loadUrl("https:" + it.data?.url)
        } ?: kotlin.run {
            rootView.showApiParsingErrorSnack()
            hideProgress()
        }
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun showProgress() {
        rlProgress.show()
    }

    private fun setToolbarData() {
        supportActionBar?.title = getString(R.string.title_my_analytics)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        if (this.webView.canGoBack()) {
            this.webView.goBack()
        } else {
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        webView.destroy()
        super.onDestroy()
    }
}
