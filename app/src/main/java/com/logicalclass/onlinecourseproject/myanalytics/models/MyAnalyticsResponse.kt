package com.logicalclass.onlinecourseproject.myanalytics.models

import com.google.gson.annotations.SerializedName
import com.logicalclass.onlinecourseproject.repository.Auth

data class MyAnalyticsResponse(
    val authData: Auth,
    val data: MyAnalyticsData?,
    val msg: String,
    val status: Int,
    val token: String
)

data class MyAnalyticsData(
    @SerializedName("class")
    val className: String = "",
    val subject: String = "",
    val url: String = "",
)