package com.logicalclass.onlinecourseproject.onlinecourses

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.onlinecourses.adapters.OnlineCourseClassesListAdapter
import com.logicalclass.onlinecourseproject.onlinecourses.model.CoursesClassModelResponse
import com.logicalclass.onlinecourseproject.repository.AppService
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.androidThread
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.ioThread
import com.logicalclass.onlinecourseproject.utils.show
import com.logicalclass.onlinecourseproject.utils.showApiParsingErrorSnack
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_online_courses.*

class CourseClassesActivity : AppCompatActivity() {

    private lateinit var adapter: OnlineCourseClassesListAdapter
    private var StudentAuth: String? = null
    private var orgAuth: String? = null
    private var tokenid2: String? = null
    private var disposable: CompositeDisposable? = null
    private var courseId: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_online_courses)
        setToolbarData()
        courseId = intent?.getIntExtra("courseId", 0)
        disposable = CompositeDisposable()
        tokenid2 = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.TOKEN_ID_2)
        StudentAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.STUDENT_AUTH)
        orgAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.ORG_AUTH)
        requestForOnlineCourseClassesList()
    }

    private fun setToolbarData() {
        /*setSupportActionBar(findViewById(R.id.toolbar))*/
        supportActionBar?.title = intent?.getStringExtra("course_name")
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            toolbar?.navigationIcon?.colorFilter = BlendModeColorFilter(android.R.color.white, BlendMode.SRC_ATOP)
        } else {
            toolbar?.navigationIcon?.setColorFilter(getColor(android.R.color.white), PorterDuff.Mode.SRC_ATOP)
        }*/
    }

    private fun requestForOnlineCourseClassesList() {
        disposable?.add(AppService.create().getMyCurseClasses(
                tokenid2,
                StudentAuth,
                orgAuth,
                SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.CLASS_NAME),
                courseId
        ).subscribeOn(ioThread())
                .doOnSubscribe {
                    showProgress()
                }.observeOn(androidThread())
                .subscribe({ response: CoursesClassModelResponse ->
                    handleOnlineCourseClassesResponse(response)
                }, {
                    rootView.showApiParsingErrorSnack()
                    llEmptyData.show()
                })
        )
    }

    private fun handleOnlineCourseClassesResponse(response: CoursesClassModelResponse) {
        if (response.status == 200 && response.data.courseClasses.isNotEmpty()) {
            val list = response.data.courseClasses
            val layoutManager = LinearLayoutManager(this)
            adapter = OnlineCourseClassesListAdapter(this, list)
            rvList.layoutManager = layoutManager
            rvList.adapter = adapter
        } else {
            llEmptyData.show()
        }
        hideProgress()
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun showProgress() {
        rlProgress.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.clear()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
