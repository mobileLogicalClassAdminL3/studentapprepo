package com.logicalclass.onlinecourseproject.onlinecourses

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.webkit.PermissionRequest
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.utils.show
import kotlinx.android.synthetic.main.activity_live_class_webview.*


class LiveCourseWebViewActivity : AppCompatActivity() {
    val MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 101
    public var myRequest: PermissionRequest? = null

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_live_class_webview)
        setToolbarData()
        webView.settings.javaScriptEnabled = true
        webView.settings.loadWithOverviewMode = true
        webView.settings.useWideViewPort = true
        webView.settings.userAgentString =
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36";
        webView.settings.javaScriptCanOpenWindowsAutomatically = true
        webView.settings.mediaPlaybackRequiresUserGesture = false;

        // Enable pinch to zoom without the zoom buttons
        /*webView.settings.builtInZoomControls = true;*/

        webView.webChromeClient = ChromeClient(this)

        webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                showProgress()
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                hideProgress()
            }
        }
        webView.loadUrl(intent.getStringExtra("webview_url") ?: "")
    }

    fun askForPermission(origin: String, permission: String, requestCode: Int) {
        if (ContextCompat.checkSelfPermission(
                applicationContext,
                permission
            ) != PackageManager.PERMISSION_GRANTED
        ) { // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else { // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, arrayOf(permission), requestCode)
            }
        } else {
            myRequest?.grant(myRequest?.resources)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_RECORD_AUDIO -> {
                Log.d("WebView", "PERMISSION FOR AUDIO")
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    myRequest?.grant(myRequest?.resources)
                    webView.loadUrl(intent.getStringExtra("webview_url") ?: "")
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }
        }
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun showProgress() {
        rlProgress.show()
    }

    private fun setToolbarData() {
        /*setSupportActionBar(findViewById(R.id.toolbar))*/
        supportActionBar?.title = intent?.getStringExtra("course_name")
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            toolbar?.navigationIcon?.colorFilter = BlendModeColorFilter(android.R.color.white, BlendMode.SRC_ATOP)
        } else {
            toolbar?.navigationIcon?.setColorFilter(getColor(android.R.color.white), PorterDuff.Mode.SRC_ATOP)
        }*/
    }

    class ChromeClient internal constructor(val context: LiveCourseWebViewActivity) :
        WebChromeClient() {
        private var mCustomView: View? = null
        private var mCustomViewCallback: CustomViewCallback? = null
        protected var mFullscreenContainer: FrameLayout? = null
        private var mOriginalOrientation = 0
        private var mOriginalSystemUiVisibility = 0
        override fun getDefaultVideoPoster(): Bitmap? {
            return if (mCustomView == null) {
                null
            } else BitmapFactory.decodeResource(context.resources, 2130837573)
        }

        override fun onHideCustomView() {
            (context.getWindow().getDecorView() as FrameLayout).removeView(mCustomView)
            mCustomView = null
            context.getWindow().getDecorView().setSystemUiVisibility(mOriginalSystemUiVisibility)
            context.setRequestedOrientation(mOriginalOrientation)
            mCustomViewCallback!!.onCustomViewHidden()
            mCustomViewCallback = null
        }

        override fun onShowCustomView(
            paramView: View?,
            paramCustomViewCallback: CustomViewCallback?
        ) {
            if (mCustomView != null) {
                onHideCustomView()
                return
            }
            mCustomView = paramView
            mOriginalSystemUiVisibility = context.getWindow().getDecorView().getSystemUiVisibility()
            mOriginalOrientation = context.getRequestedOrientation()
            mCustomViewCallback = paramCustomViewCallback
            (context.getWindow().getDecorView() as FrameLayout).addView(
                mCustomView,
                FrameLayout.LayoutParams(-1, -1)
            )
            context.getWindow().getDecorView()
                .setSystemUiVisibility(3846 or View.SYSTEM_UI_FLAG_LAYOUT_STABLE)
        }

        override fun onPermissionRequest(request: PermissionRequest) {
            context.myRequest = request
            for (permission in request.resources) {
                when (permission) {
                    "android.webkit.resource.AUDIO_CAPTURE" -> {
                        context.askForPermission(
                            request.origin.toString(),
                            Manifest.permission.RECORD_AUDIO,
                            context.MY_PERMISSIONS_REQUEST_RECORD_AUDIO
                        )
                    }
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /*override fun onBackPressed() {
        if (this.webView.canGoBack()) {
            this.webView.goBack()
        } else {
            super.onBackPressed()
        }
    }*/

    override fun onPause() {
        webView.onPause()
        webView.pauseTimers()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        webView.resumeTimers()
        webView.onResume()
    }

    override fun onDestroy() {
        webView.destroy()
        super.onDestroy()
    }
}
