package com.logicalclass.onlinecourseproject.onlinecourses.adapters

import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.animation.Animation
import androidx.recyclerview.widget.RecyclerView
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.databinding.ItemOnlineCoursesListBinding
import com.logicalclass.onlinecourseproject.utils.getHtmlText
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.onlinecourses.model.MyCourseModel
import com.logicalclass.onlinecourseproject.utils.show
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils

class OnlineCoursesListAdapter(private val context: Context,
                               private val onlineClassesList: List<MyCourseModel>,
                               private val callBacks: ICallBacks?) :
        RecyclerView.Adapter<OnlineCoursesListAdapter.OnlineCourseViewHolder>() {

    interface ICallBacks {
        fun onEnrollOnlineCourse(position: Int, resultData: MyCourseModel?)
        fun onJoinOnlineCourse(position: Int, resultData: MyCourseModel?)
        fun onViewClassesLink(position: Int, resultData: MyCourseModel?)
    }

    override fun getItemCount(): Int {
        return onlineClassesList.size
    }

    fun getItem(position: Int): MyCourseModel {
        return onlineClassesList[position]
    }

    fun updateEnrollStatus(position: Int) {
        val onlineClassesModel = getItem(position)
        onlineClassesModel.course.student?.add(
                SharedPrefsUtils.getStringPreference(context, SharedPrefsUtils.STUDENT_AUTH) ?: "")
        notifyItemChanged(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OnlineCourseViewHolder {
        return OnlineCourseViewHolder(
                ItemOnlineCoursesListBinding.inflate(
                        LayoutInflater.from(context),
                        parent,
                        false
                ))
    }

    override fun onBindViewHolder(holder: OnlineCourseViewHolder, position: Int) {
        val courseModel = getItem(position)
        val onlineCourse = courseModel.course
        holder.binding.onlineCourse = onlineCourse

        holder.binding.txtViewClasses.text = context.getString(R.string.view_classes_scheduled).getHtmlText()
        holder.binding.txtChapter.text = onlineCourse.getChaptersString()
        courseModel.classToday?.let {
            holder.binding.topicLayout.show()
            holder.binding.txtTopics.text = it.getTopicsString() ?: "---"
        } ?: kotlin.run {
            holder.binding.topicLayout.hide()
        }

        val isStudentEnrolled = onlineCourse.isStudentEnrolled(context)
        val isStudentJoinedAlready = onlineCourse.isStudentJoinedAlready(context)

        if (!isStudentEnrolled && !isStudentJoinedAlready && onlineCourse.canEnroll.equals("NO", true)) {
            holder.binding.txtExpired.visibility = VISIBLE
            holder.binding.btnEnroll.visibility = GONE
            holder.binding.txtBtnJoin.visibility = GONE
        } else if (!isStudentEnrolled && !isStudentJoinedAlready && onlineCourse.canEnroll.equals("YES", true)) {
            holder.binding.btnEnroll.visibility = VISIBLE
            holder.binding.txtBtnJoin.visibility = GONE
            holder.binding.txtExpired.visibility = GONE
        } else {
            holder.binding.txtBtnJoin.visibility = VISIBLE
            holder.binding.btnEnroll.visibility = GONE
            holder.binding.txtExpired.visibility = GONE
        }
    }

    private fun manageBlinkEffect(textView: View) {
        val anim: ObjectAnimator = ObjectAnimator.ofInt(textView, "backgroundColor", Color.WHITE, Color.RED,
                Color.WHITE)
        anim.duration = 1500
        anim.setEvaluator(ArgbEvaluator())
        anim.repeatMode = ValueAnimator.REVERSE
        anim.repeatCount = Animation.INFINITE
        anim.start()
    }

    inner class OnlineCourseViewHolder(var binding: ItemOnlineCoursesListBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.btnEnroll.setOnClickListener { callBacks?.onEnrollOnlineCourse(adapterPosition, getItem(adapterPosition)) }
            binding.txtBtnJoin.setOnClickListener { callBacks?.onJoinOnlineCourse(adapterPosition, getItem(adapterPosition)) }
            binding.txtViewClasses.setOnClickListener { callBacks?.onViewClassesLink(adapterPosition, getItem(adapterPosition)) }
        }
    }
}