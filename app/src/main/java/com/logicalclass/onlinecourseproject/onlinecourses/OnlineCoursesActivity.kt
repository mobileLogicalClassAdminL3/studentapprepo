package com.logicalclass.onlinecourseproject.onlinecourses

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.logicalclass.onlinecourseproject.BuildConfig
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.onlineclasses.model.EnrollResponse
import com.logicalclass.onlinecourseproject.onlinecourses.adapters.OnlineCoursesListAdapter
import com.logicalclass.onlinecourseproject.onlinecourses.model.JoinCourseData
import com.logicalclass.onlinecourseproject.onlinecourses.model.JoinCourseResponse
import com.logicalclass.onlinecourseproject.onlinecourses.model.MyCourseModel
import com.logicalclass.onlinecourseproject.onlinecourses.model.OnlineCoursesModelResponse
import com.logicalclass.onlinecourseproject.repository.AppService
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.androidThread
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.ioThread
import com.logicalclass.onlinecourseproject.utils.*
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_online_courses.*

class OnlineCoursesActivity : AppCompatActivity(), OnlineCoursesListAdapter.ICallBacks {

    private lateinit var onlineCoursesListAdapter: OnlineCoursesListAdapter
    private var StudentAuth: String? = null
    private var orgAuth: String? = null
    private var auth: String? = null
    private var disposable: CompositeDisposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_online_courses)
        setToolbarData()
        disposable = CompositeDisposable()
        auth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.TOKEN_ID_2)
        StudentAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.STUDENT_AUTH)
        orgAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.ORG_AUTH)
        requestForOnlineCoursesList()
    }

    private fun setToolbarData() {
        supportActionBar?.title = getString(R.string.title_online_courses)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun requestForOnlineCoursesList() {
        disposable?.add(AppService.create().getMyCourses(
                auth,
                StudentAuth,
                orgAuth,
                SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.CLASS_NAME)
        ).subscribeOn(ioThread())
                .doOnSubscribe {
                    showProgress()
                }.observeOn(androidThread())
                .subscribe({ response: OnlineCoursesModelResponse ->
                    handleOnlineCoursesResponse(response)
                }, {
                    hideProgress()
                    rootView.showApiParsingErrorSnack()
                    llEmptyData.show()
                })
        )
    }

    private fun handleOnlineCoursesResponse(response: OnlineCoursesModelResponse) {
        if (response.status == 200 && response.data.myCourses.isNotEmpty()) {
            val list = response.data.myCourses
            val layoutManager = LinearLayoutManager(this)
            onlineCoursesListAdapter = OnlineCoursesListAdapter(this, list, this)
            rvList.layoutManager = layoutManager
            rvList.adapter = onlineCoursesListAdapter
        } else {
            llEmptyData.show()
        }
        hideProgress()
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun showProgress() {
        rlProgress.show()
    }

    override fun onEnrollOnlineCourse(position: Int, resultData: MyCourseModel?) {
        requestForEnrollOnlineCourse(position, resultData?.course?.id)
    }

    override fun onJoinOnlineCourse(position: Int, resultData: MyCourseModel?) {
        requestForStartOnlineCourse(resultData)
    }

    override fun onViewClassesLink(position: Int, resultData: MyCourseModel?) {
        val intent = Intent(this, CourseClassesActivity::class.java)
        intent.putExtra("courseId", resultData?.course?.id ?: 0)
        intent.putExtra("course_name", resultData?.course?.courseName)
        startActivity(intent)
    }

    private fun requestForEnrollOnlineCourse(position: Int, courseId: Int?) {
        disposable?.add(AppService.create().enrollOnlineCourse(
                auth,
                StudentAuth,
                orgAuth,
                SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.CLASS_NAME),
                courseId
        ).subscribeOn(ioThread())
                .observeOn(androidThread())
                .doOnSubscribe {
                    showProgress()
                }
                .subscribe({ response: EnrollResponse ->
                    handleEnrollOnlineCourseResponse(position, response)
                }, {
                    hideProgress()
                    rootView.showApiParsingErrorSnack()
                })
        )
    }

    private fun handleEnrollOnlineCourseResponse(position: Int, response: EnrollResponse) {
        if (response.status == 200) {
            onlineCoursesListAdapter.updateEnrollStatus(position)
        } else {
            rootView.showSnackMessage(response.msg)
        }
        hideProgress()
    }

    private fun requestForStartOnlineCourse(courseResponse: MyCourseModel?) {
        disposable?.add(AppService.create().joinCourseClass(
                auth,
                StudentAuth,
                orgAuth,
                SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.CLASS_NAME),
                courseResponse?.course?.id,
                courseResponse?.classToday?.classId
        ).subscribeOn(ioThread())
                .doOnSubscribe {
                    showProgress()
                }
                .observeOn(androidThread())
                .subscribe({ response: JoinCourseResponse ->
                    handleJoinOnlineCourseResponse(response, courseResponse)
                }, {
                    hideProgress()
                    rootView.showApiParsingErrorSnack()
                })
        )
    }

    private fun handleJoinOnlineCourseResponse(response: JoinCourseResponse, courseResponse: MyCourseModel?) {
        if (response.status == 200) {
            val intent = Intent(this, LiveCourseWebViewActivity::class.java)
            intent.putExtra("webview_url", prepareJoinClassURL(response.data, courseResponse))
            intent.putExtra("course_title", courseResponse?.course?.courseName)
            startActivity(intent)
        } else {
            rootView.showSnackMessage(response.msg)
        }
        hideProgress()
    }

    private fun prepareJoinClassURL(joinClassData: JoinCourseData, courseResponse: MyCourseModel?): String {
        return BuildConfig.HTTP_KEY + joinClassData.primaryUrl.plus("?").plus("classStatusUrl=").plus(joinClassData.classStatusUrl)
                .plus("&&").plus("orgAuth=").plus(joinClassData.orgAuth)
                .plus("&&").plus("StudentAuth=").plus(joinClassData.StudentAuth)
                .plus("&&").plus("classId=").plus(joinClassData.classId)
                .plus("&&").plus("class=").plus(joinClassData.className)
                .plus("&&").plus("courseId=").plus(courseResponse?.course?.id)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.clear()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
