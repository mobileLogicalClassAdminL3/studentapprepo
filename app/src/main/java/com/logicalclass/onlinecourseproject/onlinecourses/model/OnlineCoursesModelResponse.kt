package com.logicalclass.onlinecourseproject.onlinecourses.model

import android.content.Context
import com.google.gson.annotations.SerializedName
import com.logicalclass.onlinecourseproject.repository.Auth
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils

data class OnlineCoursesModelResponse(
    val authData: Auth,
    @SerializedName("data")
    val data: CoursesData,
    val msg: String,
    val status: Int,
    val token: String
)

data class CoursesData(
    val myCourses: List<MyCourseModel>
)

data class MyCourseModel(
    val classToday: CourseClassModel?,
    val course: CourseModel
)

data class CourseModel(
    val about: String,
    val accessLevels: List<String>?,
    val attendedBy: List<String>?,
    val chapter: List<String>?,
    @SerializedName("class")
    val className: String,
    val courseName: String,
    val createdBy: String,
    val endDate: String,
    val id: Int,
    val level: String,
    val mappedExam: List<String>?,
    val org: String,
    val price: String,
    val startDate: String,
    val student: ArrayList<String>?,
    val subject: String,
    val canEnroll: String
) {
    fun getChaptersString(): String {
        chapter?.let {
            val chaptersString = StringBuilder()
            for (i in it.indices) {
                if (i != 0) {
                    chaptersString.append(" | ")
                }
                chaptersString.append(it[i])
            }
            return chaptersString.toString()
        }
        return ""
    }

    fun isStudentEnrolled(context: Context): Boolean {
        student?.forEach { i ->
            if (SharedPrefsUtils.getStringPreference(context, SharedPrefsUtils.STUDENT_AUTH) == i)
                return true
        }
        return false
    }

    fun isStudentJoinedAlready(context: Context): Boolean {
        attendedBy?.forEach { i ->
            if (SharedPrefsUtils.getStringPreference(context, SharedPrefsUtils.STUDENT_AUTH) == i)
                return true
        }
        return false
    }
}

data class CoursesClassModelResponse(
    val authData: Auth,
    @SerializedName("data")
    val data: CourseClassData,
    val msg: String,
    val status: Int,
    val token: String
)

data class CourseClassData(
    val courseClasses: List<CourseClassModel>
)

data class CourseClassModel(
    val about: String?,
    val attendedBy: List<Any>,
    val chapter: String,
    val classCount: Int,
    val classId: String,
    val courseId: Int,
    val createdBy: String,
    val endTime: String?,
    val onDate: String,
    val org: String,
    val startTime: String?,
    val status: String,
    val topic: List<String>?
) {
    fun getTopicsString(): String? {
        topic?.let {
            val topicsString = StringBuilder()
            for (i in it.indices) {
                topicsString.append(it[i])
            }
            return topicsString.toString()
        }
        return null
    }
}

data class JoinCourseResponse(
    val authData: Auth,
    @SerializedName("data")
    val data: JoinCourseData,
    val msg: String,
    val status: Int,
    val token: String
)

data class JoinCourseData(
    val StudentAuth: String,
    @SerializedName("class")
    val className: String,
    val classId: String,
    val classStatusUrl: String,
    val orgAuth: String,
    val primaryUrl: String
)
