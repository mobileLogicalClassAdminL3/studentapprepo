package com.logicalclass.onlinecourseproject.onlinecourses.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.logicalclass.onlinecourseproject.databinding.ItemOnlineCourseClassesListBinding
import com.logicalclass.onlinecourseproject.onlinecourses.model.CourseClassModel

class OnlineCourseClassesListAdapter(private val context: Context,
                                     private val courseClassList: List<CourseClassModel>) :
        RecyclerView.Adapter<OnlineCourseClassesListAdapter.CourseClassesViewHolder>() {

    override fun getItemCount(): Int {
        return courseClassList.size
    }

    fun getItem(position: Int): CourseClassModel {
        return courseClassList[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CourseClassesViewHolder {
        return CourseClassesViewHolder(
                ItemOnlineCourseClassesListBinding.inflate(
                        LayoutInflater.from(context),
                        parent,
                        false
                ))
    }

    override fun onBindViewHolder(holder: CourseClassesViewHolder, position: Int) {
        val onlineCourse = getItem(position)
        holder.binding.courseClass = onlineCourse
        holder.binding.txtTopics.text = onlineCourse.getTopicsString() ?: "---"
        holder.binding.txtAbout.text = onlineCourse.about?.trim()
        onlineCourse.startTime?.let {
            holder.binding.txtTime.text = onlineCourse.startTime.plus(" - ").plus(onlineCourse.endTime)
        } ?: kotlin.run {
            holder.binding.txtTime.text = "---"
        }
    }

    inner class CourseClassesViewHolder(var binding: ItemOnlineCourseClassesListBinding) : RecyclerView.ViewHolder(binding.root)
}