package com.logicalclass.onlinecourseproject.repository

import com.logicalclass.onlinecourseproject.announcements.models.AnnouncementsResponse
import com.logicalclass.onlinecourseproject.attendance.model.AttendanceModel
import com.logicalclass.onlinecourseproject.authentication.model.UserDetails
import com.logicalclass.onlinecourseproject.home.TipResponse
import com.logicalclass.onlinecourseproject.mistakesofstudents.model.AppearInAllQuestions
import com.logicalclass.onlinecourseproject.mistakesofstudents.model.SingleQuestionResponse
import com.logicalclass.onlinecourseproject.models.ExamResultResponse
import com.logicalclass.onlinecourseproject.models.SearchOrgResponse
import com.logicalclass.onlinecourseproject.models.StudyMaterialsTopicsResponse
import com.logicalclass.onlinecourseproject.models.TopicsResponse
import com.logicalclass.onlinecourseproject.myanalytics.models.MyAnalyticsResponse
import com.logicalclass.onlinecourseproject.ocr_q.model.ScannedDataResponse
import com.logicalclass.onlinecourseproject.onlineclasses.model.EnrollResponse
import com.logicalclass.onlinecourseproject.onlineclasses.model.JoinClassResponse
import com.logicalclass.onlinecourseproject.onlineclasses.model.OnlineClassesModelResponse
import com.logicalclass.onlinecourseproject.onlinecourses.model.CoursesClassModelResponse
import com.logicalclass.onlinecourseproject.onlinecourses.model.JoinCourseResponse
import com.logicalclass.onlinecourseproject.onlinecourses.model.OnlineCoursesModelResponse
import com.logicalclass.onlinecourseproject.onlinediary.model.DigitalDiaryResponse
import com.logicalclass.onlinecourseproject.onlinetest.model.*
import com.logicalclass.onlinecourseproject.profile.models.UserProfileResponse
import com.logicalclass.onlinecourseproject.studymaterials.models.SubjectWiseChaptersResponse
import com.logicalclass.onlinecourseproject.subjectchapters.SubjectsChaptersResponse
import com.logicalclass.onlinecourseproject.videotutoriales.models.VideoSubjectWiseChaptersResponse
import com.logicalclass.onlinecourseproject.videotutoriales.models.VideoTopicsResponse
import com.logicalclass.onlinecourseproject.viewresult.models.ViewResultResponse
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface RequestInterface {

    @GET("studentProfile")
    fun getProfile(
        @Query("auth") auth: String?
    ): Observable<UserProfileResponse>

    @GET("checkOrg")
    fun getCheckOrganisation(
        @Query("orgName") auth: String?
    ): Observable<SearchOrgResponse>

    @GET("studentLogin")
    fun forgotPassword(
        @Query("uniqueId") email: String
    ): Observable<UserDetails>

    @GET("studentLogin")
    fun login(
        @Query("email") email: String,
        @Query("password") password: String,
        @Query("auth") auth: String?,
        @Query("device_token") device_token: String?
    ): Observable<UserDetails>

    @GET("attendanceDateRangDetails")
    fun attendanceDateRangDetails(
        @Query("auth") auth: String?,
        @Query("StudentAuth") StudentAuth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("startDate") startDate: String?,
        @Query("endDate") endDate: String?
    ): Observable<AttendanceModel>

    @GET("mockTests")
    fun mockTests(
        @Query("auth") auth: String?,
        @Query("StudentAuth") StudentAuth: String?,
        @Query("orgAuth") orgAuth: String?
    ): Observable<MockTestsModelResponse>

    @GET("startMockTest")
    fun startMockTest(
        @Query("auth") auth: String?,
        @Query("StudentAuth") StudentAuth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("examId") questionId: String?
    ): Observable<StartMockTestResponse>

    @POST("endExamination")
    fun endExamination(
        @Query("auth") auth: String?,
        @Query("StudentAuth") StudentAuth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("examination") examination: String?,
        @Query("time") time: Long,
        @Body questionAnswers: List<Question>
    ): Observable<SubmitMockTestResponse>

    @GET("getOrgClasses")
    fun onlineClasses(
        @Query("auth") auth: String?,
        @Query("StudentAuth") StudentAuth: String?,
        @Query("orgAuth") orgAuth: String?
    ): Observable<OnlineClassesModelResponse>

    @GET("enrollClass")
    fun enrollOnlineClass(
        @Query("auth") auth: String?,
        @Query("StudentAuth") StudentAuth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("classId") classId: String?
    ): Observable<EnrollResponse>

    @GET("startClass")
    fun startClass(
        @Query("auth") auth: String?,
        @Query("StudentAuth") StudentAuth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("classId") classId: String?
    ): Observable<JoinClassResponse>

    @GET("digitalDiary")
    fun getDigitalDiary(
        @Query("auth") auth: String?,
        @Query("StudentAuth") StudentAuth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("class") className: String?,
        @Query("diaryDate") diaryDate: String?
    ): Observable<DigitalDiaryResponse>

    @GET("announcements")
    fun getAnnouncements(
        @Query("auth") auth: String?,
        @Query("StudentAuth") StudentAuth: String?,
        @Query("orgAuth") orgAuth: String?
    ): Observable<AnnouncementsResponse>

    @GET("onlineTestResult")
    fun getOnlineTestResult(
        @Query("auth") auth: String?
    ): Observable<ExamResultResponse>

    @GET("studyMaterialSubjectsChapter")
    fun getSubjectWiseChapters(
        @Query("auth") auth: String?,
        @Query("StudentAuth") StudentAuth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("class") className: String?
    ): Observable<SubjectWiseChaptersResponse>

    @GET("studyMaterialTopics")
    fun getStudyMaterialTopics(
        @Query("auth") auth: String?,
        @Query("StudentAuth") StudentAuth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("class") className: String?,
        @Query("subject") subject: String?,
        @Query("chapter") chapter: String?
    ): Observable<StudyMaterialsTopicsResponse>

    @GET("myCourses")
    fun getMyCourses(
        @Query("auth") auth: String?,
        @Query("StudentAuth") StudentAuth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("class") className: String?
    ): Observable<OnlineCoursesModelResponse>

    @GET("courseClasses")
    fun getMyCurseClasses(
        @Query("auth") auth: String?,
        @Query("StudentAuth") StudentAuth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("class") className: String?,
        @Query("courseId") courseId: Int?
    ): Observable<CoursesClassModelResponse>

    @GET("joinCourseClass")
    fun joinCourseClass(
        @Query("auth") auth: String?,
        @Query("StudentAuth") StudentAuth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("class") className: String?,
        @Query("courseId") courseId: Int?,
        @Query("classId") classId: String?
    ): Observable<JoinCourseResponse>

    @GET("enrollInToCourse")
    fun enrollOnlineCourse(
        @Query("auth") auth: String?,
        @Query("StudentAuth") StudentAuth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("class") className: String?,
        @Query("courseId") courseId: Int?
    ): Observable<EnrollResponse>

    @GET("studyMaterialSubjectsChapterNew")
    fun getVideoSubjectWiseChapters(
        @Query("auth") auth: String?,
        @Query("StudentAuth") StudentAuth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("class") className: String?
    ): Observable<VideoSubjectWiseChaptersResponse>

    @GET("studyMaterialTopicsNew")
    fun getVideoStudyMaterialTopics(
        @Query("auth") auth: String?,
        @Query("StudentAuth") StudentAuth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("class") className: String?,
        @Query("subject") subject: String?,
        @Query("chapter") chapter: String?
    ): Observable<VideoTopicsResponse>

    @GET("mockTestAnalyticsDetails")
    fun getMockTestAnalyticsDetails(
        @Query("auth") auth: String?,
        @Query("examId") examId: String?
    ): Observable<ViewResultResponse>

    @GET("digitalLibrarySubjectsChapter")
    fun getDigitalLibrarySubjectsChapters(
        @Query("auth") auth: String?,
        @Query("StudentAuth") StudentAuth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("class") className: String?
    ): Observable<SubjectWiseChaptersResponse>

    @GET("mySubjectsChapterStudyMaterial")
    fun getMySubjectsChapterStudyMaterial(
        @Query("auth") auth: String?,
        @Query("StudentAuth") StudentAuth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("class") className: String?
    ): Observable<SubjectWiseChaptersResponse>

    @GET("scanContent")
    fun getScannedContent(
        @Query("auth") auth: String?,
        @Query("content") content: String?
    ): Observable<ScannedDataResponse>

    /**************************** ERROR ANALYSIS ****************************/
    /*************** Mistake Questions ***************/
    @GET("viewWrongQuestions")
    fun getPractiseOwnMistakesSubjectChaptersList(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?
    ): Observable<SubjectsChaptersResponse>

    @GET("showMyWrongTopicWise")
    fun getPractiseOwnMistakesTopicsList(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("subject") subject: String?,
        @Query("chapter") chapter: String?
    ): Observable<TopicsResponse>

    @GET("appearInMyWrongQuestion")
    fun getPractiseOwnMistakesAllQuestionsIds(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("subject") subject: String?,
        @Query("chapter") chapter: String?,
        @Query("topic") topic: String?
    ): Observable<AppearInAllQuestions>
    /*************** Mistake Questions ***************/
    /*************** Time wasted Questions ***************/
    @GET("viewTimeTakenQuestions")
    fun getViewOwnTimeTakenSubjectChaptersList(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?
    ): Observable<SubjectsChaptersResponse>

    @GET("showTimeTakenQuestionsTopicWise")
    fun getViewOwnTimeTakenTopicsList(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("subject") subject: String?,
        @Query("chapter") chapter: String?
    ): Observable<TopicsResponse>

    @GET("appearInTimeTakenQuestions")
    fun getOwnTimeTakenAllQuestionIds(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("subject") subject: String?,
        @Query("chapter") chapter: String?,
        @Query("topic") topic: String?
    ): Observable<AppearInAllQuestions>
    /*************** Time wasted Questions ***************/
    /**************************** ERROR ANALYSIS ****************************/

    /**************************** MOST STUDENT ERRORS ****************************/
    /*************** Mistake Questions ***************/
    @GET("viewMistakeOfMostStudents")
    fun getMistakesOfMostStudentsSubjectChaptersList(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?
    ): Observable<SubjectsChaptersResponse>

    @GET("showMistakeOfMostStudentTopicWise")
    fun getMistakesOfMostStudentsTopicsList(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("subject") subject: String?,
        @Query("chapter") chapter: String?
    ): Observable<TopicsResponse>

    @GET("appearInMostMistakeStudent")
    fun getMistakesOfMostStudentsAllQuestionIds(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("subject") subject: String?,
        @Query("chapter") chapter: String?,
        @Query("topic") topic: String?
    ): Observable<AppearInAllQuestions>
    /*************** Mistake Questions ***************/
    /*************** Time wasted Questions ***************/
    @GET("viewTimeWastedQuesOfMostStudents")
    fun getPracticeTimeWastedOfMostStudentsSubjectChaptersList(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?
    ): Observable<SubjectsChaptersResponse>

    @GET("showTimeWastedOfMostStudentTopicWise")
    fun getPracticeTimeWastedOfMostStudentsTopicsList(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("subject") subject: String?,
        @Query("chapter") chapter: String?
    ): Observable<TopicsResponse>

    @GET("appearInTimeWastedOfMostStudent")
    fun getPracticeTimeWastedOfMostStudentsAllQuestionIds(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("subject") subject: String?,
        @Query("chapter") chapter: String?,
        @Query("topic") topic: String?
    ): Observable<AppearInAllQuestions>
    /*************** Time wasted Questions ***************/
    /**************************** MOST STUDENT ERRORS ****************************/

    /**************************** TOPPER ERRORS ****************************/
    /*************** Mistake Questions ***************/
    @GET("wrongOfTopper")
    fun getWrongOfTopperSubjectChaptersList(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?
    ): Observable<SubjectsChaptersResponse>

    @GET("showWrongOfTopperTopicWise")
    fun getWrongOfTopperTopicsList(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("subject") subject: String?,
        @Query("chapter") chapter: String?
    ): Observable<TopicsResponse>

    @GET("appearInWrongOfTopper")
    fun getWrongOfTopperAllQuestionIds(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("subject") subject: String?,
        @Query("chapter") chapter: String?,
        @Query("topic") topic: String?
    ): Observable<AppearInAllQuestions>
    /*************** Mistake Questions ***************/
    /*************** Time wasted Questions ***************/
    @GET("timeWastedOfTopper")
    fun getTimeWastedOfTopperSubjectChaptersList(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?
    ): Observable<SubjectsChaptersResponse>

    @GET("showTimeWastedOfTopperTopicWise")
    fun getTimeWastedOfTopperTopicsList(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("subject") subject: String?,
        @Query("chapter") chapter: String?
    ): Observable<TopicsResponse>

    @GET("appearInTimeWastedOfTopper")
    fun getTimeWastedOfTopperAllQuestionIds(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("subject") subject: String?,
        @Query("chapter") chapter: String?,
        @Query("topic") topic: String?
    ): Observable<AppearInAllQuestions>
    /*************** Time wasted Questions ***************/
    /**************************** TOPPER ERRORS ****************************/

    @GET("getOneQuestion")
    fun getOneQuestion(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("questionId") questionId: String?
    ): Observable<SingleQuestionResponse>

    @POST("responseAnswer")
    fun submitQuestion(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Body question: Question
    ): Observable<SubmitQuestionResponse>

    @GET("tipsNotifications")
    fun getTipsNotifications(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?
    ): Observable<TipResponse>

    @GET("discardNotification")
    fun discardTipNotification(
        @Query("auth") auth: String?,
        @Query("orgAuth") orgAuth: String?,
        @Query("notificationId") notificationId: String
    ): Observable<TipResponse>

    @GET("studentDashboard")
    fun getMyAnalytics(
        @Query("auth") auth: String?
    ): Observable<MyAnalyticsResponse>
}