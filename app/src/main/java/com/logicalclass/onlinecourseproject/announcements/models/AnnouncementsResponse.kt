package com.logicalclass.onlinecourseproject.announcements.models

import com.google.gson.annotations.SerializedName
import com.logicalclass.onlinecourseproject.repository.Auth

data class AnnouncementsResponse(
        val authData: Auth,
        @SerializedName("data")
        val data: AnnouncementsData,
        val msg: String,
        val status: Int,
        val token: String
)

data class AnnouncementsData(
        val announcements: List<AnnouncementModel>
)

data class AnnouncementModel(
        val announcement: String,
        val endDate: String,
        val id: Long,
        val startDate: String,
        val title: String
)