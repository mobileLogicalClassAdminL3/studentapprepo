package com.logicalclass.onlinecourseproject.announcements.view

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.announcements.adapters.AnnouncementsListAdapter
import com.logicalclass.onlinecourseproject.announcements.models.AnnouncementsResponse
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.repository.AppService
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.androidThread
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.ioThread
import com.logicalclass.onlinecourseproject.utils.show
import com.logicalclass.onlinecourseproject.utils.showApiParsingErrorSnack
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_announcements.*

class PublicAnnouncementsActivity : AppCompatActivity() {

    private lateinit var announcementsAdapter: AnnouncementsListAdapter
    private var StudentAuth: String? = null
    private var orgAuth: String? = null
    private var tokenid2: String? = null
    private var disposable: CompositeDisposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_announcements)
        setToolbarData()
        disposable = CompositeDisposable()
        tokenid2 = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.TOKEN_ID_2)
        StudentAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.STUDENT_AUTH)
        orgAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.ORG_AUTH)
        requestForAnnouncementsList()
    }

    private fun setToolbarData() {
        supportActionBar?.title = getString(R.string.title_public_announcements)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun requestForAnnouncementsList() {
        disposable?.add(AppService.create().getAnnouncements(
                tokenid2,
                StudentAuth,
                orgAuth
        ).subscribeOn(ioThread())
                .doOnSubscribe {
                    showProgress()
                }.observeOn(androidThread())
                .subscribe({ response: AnnouncementsResponse ->
                    handleOnlineClassesResponse(response)
                }, {
                    rootView.showApiParsingErrorSnack()
                    llEmptyData.show()
                })
        )
    }

    private fun handleOnlineClassesResponse(response: AnnouncementsResponse) {
        if (response.status == 200 && response.data.announcements.isNotEmpty()) {
            val list = response.data.announcements
            val layoutManager = LinearLayoutManager(this)
            announcementsAdapter = AnnouncementsListAdapter(this, list)
            rvList.layoutManager = layoutManager
            rvList.adapter = announcementsAdapter
        } else {
            llEmptyData.show()
        }
        hideProgress()
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun showProgress() {
        rlProgress.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.clear()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
