package com.logicalclass.onlinecourseproject.announcements.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.logicalclass.onlinecourseproject.announcements.models.AnnouncementModel
import com.logicalclass.onlinecourseproject.databinding.ItemAnnouncementsListBinding

class AnnouncementsListAdapter(private val context: Context,
                               private val announcementList: List<AnnouncementModel>) :
        RecyclerView.Adapter<AnnouncementsListAdapter.AnnouncementsViewHolder>() {

    override fun getItemCount(): Int {
        return announcementList.size
    }

    fun getItem(position: Int): AnnouncementModel {
        return announcementList[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnnouncementsViewHolder {
        return AnnouncementsViewHolder(
                ItemAnnouncementsListBinding.inflate(
                        LayoutInflater.from(context),
                        parent,
                        false
                ))
    }

    override fun onBindViewHolder(holder: AnnouncementsViewHolder, position: Int) {
        val announcementModel = getItem(position)
        holder.binding.announcement = announcementModel
    }

    inner class AnnouncementsViewHolder(var binding: ItemAnnouncementsListBinding) : RecyclerView.ViewHolder(binding.root)
}