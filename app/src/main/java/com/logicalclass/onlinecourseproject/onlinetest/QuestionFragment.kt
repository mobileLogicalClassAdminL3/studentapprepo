package com.logicalclass.onlinecourseproject.onlinetest

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.fragment.app.Fragment
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.databinding.FragmentPagerListBinding
import com.logicalclass.onlinecourseproject.onlinetest.model.AttemptType
import com.logicalclass.onlinecourseproject.onlinetest.model.QuestionModel

class QuestionFragment : Fragment() {
    private var position: Int = 0
    private var totalQuestions: Int = 0
    private lateinit var binding: FragmentPagerListBinding
    var questionModel: QuestionModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        position = arguments?.getInt(CURRENT_QUESTION_POS) ?: 0
        totalQuestions = arguments?.getInt(QUESTIONS_COUNT) ?: 0
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentPagerListBinding.inflate(LayoutInflater.from(context), container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setQuestionViewData()
        setOnCheckChangeListeners()
    }

    private fun setOnCheckChangeListeners() {
        binding.opt1Layout.setOnClickListener {
            binding.ckbOpt1.toggle()
            refreshQuestionList(binding.ckbOpt1)
        }
        binding.opt2Layout.setOnClickListener {
            binding.ckbOpt2.toggle()
            refreshQuestionList(binding.ckbOpt2)
        }

        binding.opt3Layout.setOnClickListener {
            binding.ckbOpt3.toggle()
            refreshQuestionList(binding.ckbOpt3)
        }

        binding.opt4Layout.setOnClickListener {
            binding.ckbOpt4.toggle()
            refreshQuestionList(binding.ckbOpt4)
        }

        binding.ckbMarkReview.setOnCheckedChangeListener { view, isChecked ->
            activity?.let {
                it as MockTestPagerActivity
                if (isChecked) {
                    questionModel?.setAttemptType(AttemptType.REVIEW)
                    it.setQuestionModel(position, questionModel)
                } else {
                    questionModel?.setAttemptType(if (isQuestionAnswered()) AttemptType.ANSWERED else AttemptType.NOT_ATTEMPTED)
                    it.setQuestionModel(position, questionModel)
                }
            }
        }

        binding.imgMaximize.setOnClickListener {
            activity?.let {
                it as MockTestPagerActivity
                it.showFullScreenView(questionModel?.getFormattedQuestion())
            }
        }
    }

    private fun isQuestionAnswered(): Boolean {
        return (binding.ckbOpt1.isChecked || binding.ckbOpt2.isChecked ||
                binding.ckbOpt3.isChecked || binding.ckbOpt4.isChecked)
    }

    private fun refreshQuestionList(view: CompoundButton) {
        activity?.let {
            it as MockTestPagerActivity
            if (binding.ckbOpt1.isChecked || binding.ckbOpt2.isChecked ||
                    binding.ckbOpt3.isChecked || binding.ckbOpt4.isChecked) {
                questionModel?.setAttemptType(if (binding.ckbMarkReview.isChecked) AttemptType.REVIEW else AttemptType.ANSWERED)
            } else {
                questionModel?.setAttemptType(AttemptType.NOT_ATTEMPTED)
            }
            when (view) {
                binding.ckbOpt1 -> {
                    questionModel?.setAnswer("opt1")
                }
                binding.ckbOpt2 -> {
                    questionModel?.setAnswer("opt2")
                }
                binding.ckbOpt3 -> {
                    questionModel?.setAnswer("opt3")
                }
                binding.ckbOpt4 -> {
                    questionModel?.setAnswer("opt4")
                }
            }
            it.setQuestionModel(position, questionModel)
        }
    }

    private fun setQuestionViewData() {
        activity?.let {
            it as MockTestPagerActivity
            questionModel = it.getQuestionModel(position)
        }
        questionModel?.let {
            binding.txtQuestionNo.text = getString(R.string.question_no, (position + 1).toString(), totalQuestions.toString())
            binding.txtQuestion.setDisplayText(it.getFormattedQuestion())
            binding.txtOpt1Value.setDisplayText(it.getFormattedOpt1())
            binding.txtOpt2Value.setDisplayText(it.getFormattedOpt2())
            binding.txtOpt3Value.setDisplayText(it.getFormattedOpt3())
            binding.txtOpt4Value.setDisplayText(it.getFormattedOpt4())
            binding.txtSelectOptions.text = getString(R.string.select_your_options, it.getQuestionType())
        }
    }

    override fun onDestroyView() {
        if (view != null) {
            val parentViewGroup = view?.parent as ViewGroup?
            parentViewGroup?.removeAllViews();
        }
        super.onDestroyView()
    }

    companion object {
        private const val CURRENT_QUESTION_POS = "current_question_pos"
        private const val QUESTIONS_COUNT = "questions_count"


        fun newInstance(num: Int, questionsCount: Int): QuestionFragment {
            val f = QuestionFragment()
            val args = Bundle()
            args.putInt(CURRENT_QUESTION_POS, num)
            args.putInt(QUESTIONS_COUNT, questionsCount)
            f.arguments = args
            return f
        }
    }

    private var time: Long = 0
    override fun onResume() {
        time = System.currentTimeMillis()
        super.onResume()
    }

    override fun onPause() {
        val timeSpentInMillis = System.currentTimeMillis() - time
        activity?.let {
            it as MockTestPagerActivity
            it.setTimeSpentOnQuestion(position, timeSpentInMillis)
        }
        super.onPause()
    }
}