package com.logicalclass.onlinecourseproject.onlinetest

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.onlinetest.MockTestsActivity.Companion.ON_ACTIVITY_RESULT_CODE
import com.logicalclass.onlinecourseproject.onlinetest.adapters.MockTestsPagerAdapter
import com.logicalclass.onlinecourseproject.onlinetest.adapters.QuestionsBottomListAdapter
import com.logicalclass.onlinecourseproject.onlinetest.adapters.SectionedGridRecyclerViewAdapter
import com.logicalclass.onlinecourseproject.onlinetest.adapters.SectionedGridRecyclerViewAdapter.Section
import com.logicalclass.onlinecourseproject.onlinetest.model.*
import com.logicalclass.onlinecourseproject.repository.AppService
import com.logicalclass.onlinecourseproject.repository.RxUtils
import com.logicalclass.onlinecourseproject.service.MyFirebaseMessagingService
import com.logicalclass.onlinecourseproject.socrecard.view.ScoreCardActivity
import com.logicalclass.onlinecourseproject.utils.*
import com.logicalclass.onlinecourseproject.viewresult.view.ViewResultsActivity
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.acitivity_mock_test_pager.*
import kotlinx.android.synthetic.main.questions_bottom_sheet.*
import java.util.*
import kotlin.collections.ArrayList


class MockTestPagerActivity : AppCompatActivity(), QuestionsBottomListAdapter.ISelectionCallBack {

    private var startExamData: StartExamData? = null
    private var questionsAdapter: QuestionsBottomListAdapter? = null
    private var mSectionedAdapter: SectionedGridRecyclerViewAdapter? = null
    private lateinit var behavior: BottomSheetBehavior<*>
    private var pagerAdapter: MockTestsPagerAdapter? = null
    private var disposable: CompositeDisposable = CompositeDisposable()

    private var studentAuth: String? = null
    private var orgAuth: String? = null
    private var tokenId2: String? = null

    private var mCountDownTimer: CountDownTimer? = null
    private var mStartTimeInMillis: Long = 3600000
    private var mTimeLeftInMillis: Long = mStartTimeInMillis
    private var mEndTime: Long = 0
    private var mTimerRunning = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.acitivity_mock_test_pager)
        tokenId2 = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.TOKEN_ID_2)
        studentAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.STUDENT_AUTH)
        orgAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.ORG_AUTH)
        setToolbarData()
        setClickListeners()
        requestForStartMockTest(intent.getStringExtra("exam_id"))
    }

    private fun setToolbarData() {
        supportActionBar?.title = intent?.getStringExtra("name_of_exam")
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun onStart() {
        super.onStart()
        startExamData?.let { examData ->
            val prefs = SharedPrefsUtils.getPreference(this)
            prefs?.let {
                mStartTimeInMillis =
                    prefs.getLong("startTimeInMillis", examData.getExamTimeInMillis())
                mTimeLeftInMillis = prefs.getLong("millisLeft", mStartTimeInMillis)
                mTimerRunning = prefs.getBoolean("timerRunning", false)
                updateCountDownText()

                if (mTimerRunning) {
                    mEndTime = prefs.getLong("endTime", 0)
                    mTimeLeftInMillis = mEndTime - System.currentTimeMillis()
                    if (mTimeLeftInMillis < 0) {
                        mTimeLeftInMillis = 0
                        mTimerRunning = false
                        updateCountDownText()
                    } else {
                        startTimer()
                    }
                } else {
                    startTimer()
                }
            }
        }
    }

    private fun makeViewsVisible() {
        txtTime.show()
        pager.show()
        btnPrevious.show()
        btnNext.show()
        bottomSheet.show()
    }

    private fun setClickListeners() {
        btnPrevious.setOnClickListener {
            pager.adapter?.let {
                if ((pager.currentItem - 1) >= 0) {
                    setViewPagerToPosition(pager.currentItem - 1)
                    hideFullScreenView()
                }
            }
        }
        btnNext.setOnClickListener {
            pager.adapter?.let {
                if ((pager.currentItem + 1) < it.itemCount) {
                    setViewPagerToPosition(pager.currentItem + 1)
                    hideFullScreenView()
                }
            }
        }
        imgMinimize.setOnClickListener {
            hideFullScreenView()
        }
    }

    private fun requestForStartMockTest(examId: String?) {
        disposable.add(AppService.create().startMockTest(
            tokenId2,
            studentAuth,
            orgAuth,
            examId
        ).subscribeOn(RxUtils.ioThread())
            .observeOn(RxUtils.androidThread())
            .doOnSubscribe {
                showProgress()
            }
            .subscribe({ response: StartMockTestResponse ->
                handleStartMockTestResponse(response)
            }, {
                hideProgress()
                showExamQuestionFetchingError()
            })
        )
    }

    private fun handleStartMockTestResponse(response: StartMockTestResponse) {
        if (response.status == 200 && response.data.allQuestions.isNotEmpty()) {
            startExamData = response.data
            setMockTestPagerAdapter()
            setBottomListAdapter()
            initTimerValues()
            startTimer()
            makeViewsVisible()
        } else {
            showExamQuestionFetchingError(response.msg)
        }
        hideProgress()
    }

    private fun requestForSubmitMockTest() {
        disposable.add(AppService.create().endExamination(
            tokenId2,
            studentAuth,
            orgAuth,
            startExamData?.examId ?: "",
            mStartTimeInMillis - mTimeLeftInMillis,
            getSubmitRequest()
        ).subscribeOn(RxUtils.ioThread())
            .observeOn(RxUtils.androidThread())
            .doOnSubscribe {
                showProgress(true)
            }
            .subscribe({ response: SubmitMockTestResponse ->
                handleSubmitMockTestResponse(response)
            }, {
                hideProgress()
                rootView.showApiParsingErrorSnack()
            })
        )
    }

    private fun getSubmitRequest(): ArrayList<Question> {
        val questionAnswers = ArrayList<Question>()
        questionsAdapter?.questionsList?.forEach { item ->
            if (item.getAttemptType() == AttemptType.NOT_ATTEMPTED)
                return@forEach
            val question = Question()
            question.id = item.id
            question.opt = item.selectedAnswersList ?: arrayListOf()
            question.timeSpent = item.timeSpentInMillis
            question.chapter = item.chapter
            question.className = item.className
            question.subject = item.subject
            question.topic = item.topic
            question.type = item.getQuestionType()
            questionAnswers.add(question)
        }
        return questionAnswers
    }

    private fun handleSubmitMockTestResponse(response: SubmitMockTestResponse) {
        if (response.status == 200) {
            showExamSubmissionMessageDialog()
        } else {
            rootView.showSnackMessage(response.msg?.msg ?: "")
        }
        hideProgress()
    }

    private fun getQuestionsList(): ArrayList<QuestionModel> {
        startExamData?.let {
            return it.allQuestions
        }
        return arrayListOf()
    }

    private fun initTimerValues() {
        mStartTimeInMillis = startExamData?.getExamTimeInMillis() ?: 3600000
        mTimeLeftInMillis = mStartTimeInMillis
        mTimerRunning = false
    }

    private fun setMockTestPagerAdapter() {
        pagerAdapter = MockTestsPagerAdapter(this, getQuestionsList().size)
        pager.adapter = pagerAdapter
    }

    private fun setBottomListAdapter() {
        rvQuestions.setHasFixedSize(true)
        rvQuestions.layoutManager = GridLayoutManager(this, 5)
        questionsAdapter = QuestionsBottomListAdapter(this, getQuestionsList(), this)
        mSectionedAdapter = SectionedGridRecyclerViewAdapter(
            this,
            R.layout.section,
            R.id.section_text,
            rvQuestions,
            questionsAdapter!!
        )
        mSectionedAdapter?.setSections(getSectionsListFromQuestionsList(getQuestionsList()))
        rvQuestions.adapter = mSectionedAdapter

        behavior = BottomSheetBehavior.from<View>(bottomSheet)
        rlSelectQuestions.setOnClickListener {
            expandBottomSheet()
        }
    }

    private fun getSectionsListFromQuestionsList(questionsList: ArrayList<QuestionModel>): Array<Section> {
        val sections: ArrayList<Section> = ArrayList()
        val groupBy = questionsList.groupBy { it.subject }
        var sectionPos = 0
        for ((subject, list) in groupBy) {
            sections.add(Section(sectionPos, subject ?: ""))
            sectionPos += list.size
        }
        return sections.toTypedArray()
    }

    override fun onQuestionSelected(pos: Int) {
        mSectionedAdapter?.let {
            setViewPagerToPosition(it.sectionedPositionToPosition(pos))
            frameFullView.hide()
        }
    }

    private fun setViewPagerToPosition(position: Int) {
        pager.setCurrentItem(position, false)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_submit, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            R.id.action_submit -> {
                showSubmitExamConfirmationDialog()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun getQuestionModel(pos: Int): QuestionModel? {
        return if (getQuestionsList().size > 0)
            getQuestionsList()[pos]
        else
            null
    }

    fun setQuestionModel(pos: Int, question: QuestionModel?) {
        mSectionedAdapter?.let {
            questionsAdapter?.setQuestionModel(pos, it.positionToSectionedPosition(pos), question)
        }
    }

    fun setTimeSpentOnQuestion(pos: Int, time: Long) {
        val questionModel = getQuestionModel(pos)
        questionModel?.let {
            it.timeSpentInMillis += time
            questionsAdapter?.setQuestionModelWithOutNotify(pos, it)
        }
    }

    private fun expandBottomSheet() {
        if (behavior.state == BottomSheetBehavior.STATE_COLLAPSED) {
            behavior.state = BottomSheetBehavior.STATE_EXPANDED
            imgArrow.setImageResource(R.drawable.ic_arrow_down_white)
        } else {
            behavior.state = BottomSheetBehavior.STATE_COLLAPSED
            imgArrow.setImageResource(R.drawable.ic_arrow_up_white)
        }
    }

    private fun startTimer() {
        mEndTime = System.currentTimeMillis() + mTimeLeftInMillis
        mCountDownTimer = object : CountDownTimer(mTimeLeftInMillis, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                mTimeLeftInMillis = millisUntilFinished
                updateCountDownText()
            }

            override fun onFinish() {
                mTimerRunning = false
                requestForSubmitMockTest()
            }
        }.start()
        mTimerRunning = true
    }

    private fun updateCountDownText() {
        val hours = (mTimeLeftInMillis / 1000).toInt() / 3600
        val minutes = (mTimeLeftInMillis / 1000 % 3600).toInt() / 60
        val seconds = (mTimeLeftInMillis / 1000).toInt() % 60
        val timeLeftFormatted: String
        timeLeftFormatted = String.format(
            Locale.getDefault(),
            "%02d:%02d:%02d", hours, minutes, seconds
        )
        txtTime.text = timeLeftFormatted
    }

    override fun onStop() {
        super.onStop()
        setTimerValues()
        mCountDownTimer?.cancel()
    }

    private fun setTimerValues() {
        val prefs = SharedPrefsUtils.getPreference(this)
        val editor = prefs?.edit()
        editor?.putLong("startTimeInMillis", mStartTimeInMillis)
        editor?.putLong("millisLeft", mTimeLeftInMillis)
        editor?.putBoolean("timerRunning", mTimerRunning);
        editor?.putLong("endTime", mEndTime)
        editor?.apply()
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun showProgress(shouldShowText: Boolean = false) {
        rlProgress.show()
        txtProgressDesc.visibility = if (shouldShowText) View.VISIBLE else View.GONE
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

    override fun onBackPressed() {
        showQuitExamConfirmationDialog()
    }

    private fun showSubmitExamConfirmationDialog() {
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.confirmation))
            .setCancelable(false)
            .setMessage(getString(R.string.are_sure_want_to_submit_exam))
            .setPositiveButton(android.R.string.yes) { _, _ -> requestForSubmitMockTest() }
            .setNegativeButton(android.R.string.no, null)
            .show()
    }

    private fun showQuitExamConfirmationDialog() {
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.confirmation))
            .setMessage(getString(R.string.are_sure_want_to_quit_exam))
            .setPositiveButton(android.R.string.yes) { _, _ -> super.onBackPressed() }
            .setNegativeButton(android.R.string.no, null)
            .show()
    }

    private fun showExamSubmissionMessageDialog() {
        AlertDialog.Builder(this)
            .setCancelable(false)
            .setMessage("Your exam has been submitted successfully. Do you want to check your results of your exam?")
            .setPositiveButton(android.R.string.yes) { _, _ ->
                navigateToViewResultActivity(
                    startExamData?.examId
                )
            }
            .setNegativeButton(android.R.string.no) { _, _ -> finishWithRefresh() }
            .show()
    }

    private fun finishWithRefresh() {
        val returnIntent = Intent().putExtra("isRefresh", true)
        setResult(ON_ACTIVITY_RESULT_CODE, returnIntent)
        finish()
    }

    private fun finishWithFinishPrevious() {
        val returnIntent = Intent().putExtra("isFinishActivity", true)
        setResult(ON_ACTIVITY_RESULT_CODE, returnIntent)
        finish()
    }

    private fun showExamQuestionFetchingError(message: String? = "") {
        val defaultMessage =
            "There is an issue in fetching questions for this exam, please try again."
        AlertDialog.Builder(this)
            .setCancelable(false)
            .setMessage(if (TextUtils.isEmpty(message)) defaultMessage else message)
            .setPositiveButton(android.R.string.ok) { _, _ -> finishWithRefresh() }
            .show()
    }

    private fun navigateToScoreCardActivity(examId: String?) {
        val intent = Intent(this, ScoreCardActivity::class.java)
        intent.putExtra("isFromMockTest", true)
        intent.putExtra(MyFirebaseMessagingService.EXAM_ID, examId)
        startActivity(intent)
        finishWithFinishPrevious()
    }

    private fun navigateToViewResultActivity(examId: String?) {
        val intent = Intent(this, ViewResultsActivity::class.java)
        intent.putExtra("isFromMockTest", true)
        intent.putExtra(MyFirebaseMessagingService.EXAM_ID, examId)
        startActivity(intent)
        finishWithFinishPrevious()
    }

    fun showFullScreenView(data: String?) {
        data?.let {
            frameFullView.show()
            txtFullView.setDisplayText(it)
        }
    }

    fun hideFullScreenView() {
        frameFullView.hide()
        txtFullView.setDisplayText("")
    }
}