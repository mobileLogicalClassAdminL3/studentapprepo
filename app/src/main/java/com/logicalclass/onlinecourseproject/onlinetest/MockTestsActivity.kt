package com.logicalclass.onlinecourseproject.onlinetest

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.onlinetest.adapters.MockTestListAdapter
import com.logicalclass.onlinecourseproject.onlinetest.model.MockTestsModel
import com.logicalclass.onlinecourseproject.onlinetest.model.MockTestsModelResponse
import com.logicalclass.onlinecourseproject.onlinetest.model.Value
import com.logicalclass.onlinecourseproject.repository.AppService
import com.logicalclass.onlinecourseproject.repository.RxUtils
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.utils.show
import com.logicalclass.onlinecourseproject.utils.showApiParsingErrorSnack
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_mock_tests.*

class MockTestsActivity : AppCompatActivity(), MockTestListAdapter.ICallBacks {

    private var StudentAuth: String? = null
    private var orgAuth: String? = null
    private var tokenid2: String? = null
    private var disposable: CompositeDisposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mock_tests)
        setToolbarInfo()
        disposable = CompositeDisposable()
        tokenid2 = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.TOKEN_ID_2)
        StudentAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.STUDENT_AUTH)
        orgAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.ORG_AUTH)
        requestForMockTestsList()
    }

    /*override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        requestForMockTestsList()
    }*/

    private fun requestForMockTestsList() {
        disposable?.add(AppService.create().mockTests(
            tokenid2,
            StudentAuth,
            orgAuth
        ).subscribeOn(RxUtils.ioThread())
            .doOnSubscribe {
                showProgress()
            }
            .observeOn(RxUtils.androidThread())
            .subscribe({ response: MockTestsModelResponse ->
                handleMockTestsResponse(response)
            }, {
                llEmptyData.show()
                rootView.showApiParsingErrorSnack()
                hideProgress()
            })
        )
    }

    private fun handleMockTestsResponse(response: MockTestsModelResponse) {
        if (response.status == 200 && response.data.isNotEmpty()) {
            val list = response.data
            val layoutManager = LinearLayoutManager(this)
            val mockTestListAdapter = MockTestListAdapter(this, list, this)
            rvExamsList.layoutManager = layoutManager
            rvExamsList.adapter = mockTestListAdapter
        } else {
            llEmptyData.show()
        }
        hideProgress()
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun showProgress() {
        rlProgress.show()
    }

    private fun setToolbarInfo() {
        supportActionBar?.title = getString(R.string.title_mock_tests)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun onStartExam(position: Int, resultData: MockTestsModel?) {
        showDisclaimerDialog(resultData?.value)
    }

    private fun showDisclaimerDialog(id: Value?) {
        AlertDialog.Builder(this)
            .setTitle("Disclaimer")
            .setMessage(getString(R.string.disclaimer_text_mock_text))
            .setPositiveButton(android.R.string.yes) { _, _ -> navigateToStartExam(id) }
            .setNegativeButton(android.R.string.no, null)
            .show()
    }

    private fun navigateToStartExam(examValue: Value?) {
        val intent = Intent(this, MockTestPagerActivity::class.java)
        intent.putExtra("exam_id", examValue?.id)
        intent.putExtra("name_of_exam", examValue?.nameOfExam)
        startActivityForResult(intent, ON_ACTIVITY_RESULT_CODE)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val ON_ACTIVITY_RESULT_CODE = 1111
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == ON_ACTIVITY_RESULT_CODE) {
            data?.let {
                if (it.getBooleanExtra("isFinishActivity", false)) {
                    finish()
                } else if (it.getBooleanExtra("isRefresh", true)) {
                    requestForMockTestsList()
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.clear()
    }
}
