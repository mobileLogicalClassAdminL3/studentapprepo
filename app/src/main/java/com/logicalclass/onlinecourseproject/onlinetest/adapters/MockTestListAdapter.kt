package com.logicalclass.onlinecourseproject.onlinetest.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.logicalclass.onlinecourseproject.databinding.ItemMockTestsListBinding
import com.logicalclass.onlinecourseproject.onlinetest.model.MockTestsModel
import com.logicalclass.onlinecourseproject.utils.getHtmlText

class MockTestListAdapter(private val context: Context,
                          private val mockTestsList: List<MockTestsModel>,
                          private val callBacks: ICallBacks?) :
        RecyclerView.Adapter<MockTestListAdapter.MockTestViewHolder>() {

    interface ICallBacks {
        fun onStartExam(position: Int, resultData: MockTestsModel?)
    }

    override fun getItemCount(): Int {
        return mockTestsList.size
    }

    fun getItem(position: Int): MockTestsModel {
        return mockTestsList[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MockTestViewHolder {
        return MockTestViewHolder(
                ItemMockTestsListBinding.inflate(
                        LayoutInflater.from(context),
                        parent,
                        false
                ))
    }

    override fun onBindViewHolder(holder: MockTestViewHolder, position: Int) {
        val mockTestData = getItem(position)
        holder.binding.mockTestData = mockTestData
        holder.binding.txtSubjects.text = mockTestData.getSubjectsString()
        holder.binding.txtAbout.text = mockTestData.value.about.getHtmlText()
    }

    inner class MockTestViewHolder(var binding: ItemMockTestsListBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.btnStartExam.setOnClickListener { callBacks?.onStartExam(adapterPosition, getItem(adapterPosition)) }
        }
    }
}