package com.logicalclass.onlinecourseproject.onlinetest.adapters

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.logicalclass.onlinecourseproject.onlinetest.QuestionFragment

class MockTestsPagerAdapter(activity: AppCompatActivity, private val questionsCount: Int) : FragmentStateAdapter(activity) {

    override fun createFragment(position: Int): Fragment {
        return QuestionFragment.newInstance(position, questionsCount)
    }

    override fun getItemCount(): Int {
        return questionsCount
    }
}