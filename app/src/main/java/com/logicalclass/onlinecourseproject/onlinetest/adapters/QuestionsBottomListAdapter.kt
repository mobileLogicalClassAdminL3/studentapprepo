package com.logicalclass.onlinecourseproject.onlinetest.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.databinding.ItemBottomQuestionsListBinding
import com.logicalclass.onlinecourseproject.onlinetest.model.AttemptType
import com.logicalclass.onlinecourseproject.onlinetest.model.QuestionModel

class QuestionsBottomListAdapter(var context: Context,
                                 var questionsList: ArrayList<QuestionModel>,
                                 var callback: ISelectionCallBack
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface ISelectionCallBack {
        fun onQuestionSelected(pos: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuestionsViewHolder {
        return QuestionsViewHolder(ItemBottomQuestionsListBinding.inflate(
                LayoutInflater.from(context),
                parent,
                false
        ))
    }

    fun getItem(position: Int): QuestionModel? {
        return questionsList[position]
    }

    fun setQuestionModel(pos: Int, sectionPos: Int, question: QuestionModel?) {
        question?.let {
            if (pos >= 0 && pos < questionsList.size) {
                questionsList[pos] = question
                notifyItemChanged(sectionPos)
            }
        }
    }

    fun setQuestionModelWithOutNotify(pos: Int, question: QuestionModel?) {
        question?.let {
            if (pos >= 0 && pos < questionsList.size) {
                questionsList[pos] = question
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val question = getItem(position)
        holder as QuestionsViewHolder
        holder.binding.btnQuestion.text = context.getString(R.string.question_tag, (position + 1).toString())
        when (question?.getAttemptType()) {
            AttemptType.ANSWERED -> {
                holder.binding.btnQuestion.setTextColor(context.resources.getColorStateList(R.color.white, context.theme))
                holder.binding.btnQuestion.backgroundTintList = context.resources.getColorStateList(R.color.answered_color, context.theme)
            }
            AttemptType.REVIEW -> {
                holder.binding.btnQuestion.setTextColor(context.resources.getColorStateList(R.color.white, context.theme))
                holder.binding.btnQuestion.backgroundTintList = context.resources.getColorStateList(R.color.review_color, context.theme)
            }
            else -> {
                holder.binding.btnQuestion.setTextColor(context.resources.getColorStateList(R.color.black, context.theme))
                holder.binding.btnQuestion.backgroundTintList = context.resources.getColorStateList(R.color.not_attempted_color, context.theme)
            }
        }
    }

    override fun getItemCount(): Int {
        return questionsList.size
    }

    inner class QuestionsViewHolder(var binding: ItemBottomQuestionsListBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.btnQuestion.setOnClickListener {
                callback.onQuestionSelected(adapterPosition)
            }
        }
    }
}