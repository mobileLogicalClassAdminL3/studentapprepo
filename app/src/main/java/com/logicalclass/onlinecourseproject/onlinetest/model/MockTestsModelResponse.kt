package com.logicalclass.onlinecourseproject.onlinetest.model

import com.google.gson.annotations.SerializedName
import com.logicalclass.onlinecourseproject.BuildConfig
import com.logicalclass.onlinecourseproject.repository.Auth

data class MockTestsModelResponse(
    val authData: Auth,
    @SerializedName("data")
    val data: List<MockTestsModel>,
    val msg: String,
    val status: Int,
    val token: String
)

data class MockTestsModel(
    @SerializedName("class")
    val className: String,
    val end: String,
    val examination: String,
    val image: String,
    val nameOfExam: String,
    val start: String,
    val subjects: List<String>,
    val value: Value
) {
    fun getSubjectsString(): String? {
        val items = java.lang.StringBuilder()
        for (i in subjects.indices) {
            if (i != 0) {
                items.append(",")
            }
            items.append(subjects.get(i))
        }
        return items.toString()
    }
}

data class SubmitMockTestResponse(
    val authData: Auth,
    val msg: Message?,
    val status: Int,
    val token: String
)

data class SubmitQuestionResponse(
    val authData: Auth,
    val msg: String,
    val status: Int,
    val token: String,
    val data: SubmitQuestionDataModel
)

data class SubmitQuestionDataModel(
    val answer: List<String>?,
    val isCorrect: Boolean,
    val solution: String
)

data class Message(
    val msg: String,
    val status: Int
)

data class Value(
    val about: String,
    val auth: String,
    @SerializedName("class")
    val className: String,
    val creatorType: String,
    val discount: String,
    val display: String,
    val duration: String,
    val end: String,
    val endTime: String,
    val examination: String,
    val from: String,
    val id: String,
    val nameOfExam: String,
    val negative: String,
    val otherClass: List<String>,
    val price: String,
    val start: String,
    val startTime: String,
    val to: String
)

data class StartMockTestResponse(
    val authData: Auth,
    @SerializedName("data")
    val data: StartExamData,
    val msg: String,
    val status: Int,
    val token: String
)

data class StartExamData(
    val examId: String,
    val examTimeMinute: String?,
    val allQuestions: ArrayList<QuestionModel>
) {
    fun getExamTimeInMillis(): Long {
        examTimeMinute?.let {
            return it.toLong() * 60 * 1000
        } ?: kotlin.run {
            return 3600000
        }
    }
}

data class QuestionModel(
    @SerializedName("ANALYTICAL_ABILITIES")
    val analyticalAbilitiesList: List<String>? = arrayListOf(),
    @SerializedName("CLASSSECTION")
    val classSection: ArrayList<String>,
    @SerializedName("DIFFICULTY_LEVELS")
    val difficultyLevels: ArrayList<String>?,
    @SerializedName("EXAMINATIONS")
    val examinations: ArrayList<String>,
    val Source: String,
    @SerializedName("TYPE_OF_QUESTIONS")
    val typeOfQuestionsList: ArrayList<String>?,
    /* @SerializedName("answer")
     val answerList: ArrayList<String>?,*/
    @SerializedName("class")
    val className: String?,
    val chapter: String?,
    val date: String,
    val id: String,
    val opt1: String,
    val opt2: String,
    val opt3: String,
    val opt4: String,
    val question: String,
    val randId: String,
    val solution: String,
    val status: String,
    var subject: String?,
    val submitId: String,
    val time: String,
    val topic: String?,
    val typeOfQuestion: String,
    val user: String,
    private var attemptType: AttemptType? = AttemptType.NOT_ATTEMPTED,
    var selectedAnswersList: ArrayList<String>? = arrayListOf(),
    var timeSpentInMillis: Long = 0
) {
    private fun getSrcImagePathCorrected(text: String): String {
        var replacedText = text
        if (text.contains("src")) {
            replacedText = text.replace(
                "/lc/kcfinder/",
                "${BuildConfig.IMAGE_PATH_URL}lc/kcfinder/"
            )
        }
        return replacedText
    }

    fun getFormattedQuestion(): String {
        return getSrcImagePathCorrected(question)
    }

    fun getFormattedSolution(): String {
        return getSrcImagePathCorrected(solution)
    }

    fun getFormattedOpt1(): String {
        return getSrcImagePathCorrected(opt1)
    }

    fun getFormattedOpt2(): String {
        return getSrcImagePathCorrected(opt2)
    }

    fun getFormattedOpt3(): String {
        return getSrcImagePathCorrected(opt3)
    }

    fun getFormattedOpt4(): String {
        return getSrcImagePathCorrected(opt4)
    }

    fun getAttemptType(): AttemptType {
        return attemptType ?: AttemptType.NOT_ATTEMPTED
    }

    fun setAttemptType(type: AttemptType) {
        attemptType = type
    }

    fun getQuestionType(): String {
        typeOfQuestionsList?.let {
            return if (it.size > 0) it[0] else ""
        }
        return ""
    }

    fun getDifficultyLevel(): String {
        difficultyLevels?.let {
            return if (it.size > 0) it[0] else ""
        }
        return ""
    }

    fun setAnswer(option: String?) {
        option?.let {
            if (selectedAnswersList == null) {
                selectedAnswersList = arrayListOf()
            }
            if (selectedAnswersList?.contains(option) == true) {
                selectedAnswersList?.remove(option)
            } else {
                selectedAnswersList?.add(option)
            }
        }
    }
}

enum class AttemptType {
    REVIEW,
    ANSWERED,
    NOT_ATTEMPTED
}

data class Question(
    var id: String = "",
    var opt: ArrayList<String> = arrayListOf(),
    var timeSpent: Long = 0,
    var chapter: String? = "",
    @SerializedName("class")
    var className: String? = "",
    var subject: String? = "",
    var topic: String? = "",
    var type: String? = ""
) {
    fun setAnswer(option: String?) {
        option?.let {
            if (opt.contains(option)) {
                opt.remove(option)
            } else {
                opt.add(option)
            }
        }
    }
}