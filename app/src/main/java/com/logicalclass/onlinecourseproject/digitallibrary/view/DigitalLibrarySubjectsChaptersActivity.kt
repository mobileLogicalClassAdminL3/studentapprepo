package com.logicalclass.onlinecourseproject.digitallibrary.view

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.ItemAnimator
import com.logicalclass.onlinecourseproject.BuildConfig
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.digitallibrary.view.MainWebViewActivity.Companion.KEY_WEB_VIEW_TITLE
import com.logicalclass.onlinecourseproject.digitallibrary.view.MainWebViewActivity.Companion.KEY_WEB_VIEW_URL
import com.logicalclass.onlinecourseproject.home.AI_LIBRARY
import com.logicalclass.onlinecourseproject.home.HomeSubModuleData
import com.logicalclass.onlinecourseproject.home.MainActivity.Companion.KEY_SUB_MODULE
import com.logicalclass.onlinecourseproject.home.ORGANISATION_LIBRARY
import com.logicalclass.onlinecourseproject.repository.AppService
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.androidThread
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.ioThread
import com.logicalclass.onlinecourseproject.studymaterials.adapters.SubjectChapterAdapter
import com.logicalclass.onlinecourseproject.studymaterials.models.SubjectWiseChaptersResponse
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.utils.show
import com.logicalclass.onlinecourseproject.utils.showApiParsingErrorSnack
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_announcements.*

class DigitalLibrarySubjectsChaptersActivity : AppCompatActivity() {
    private var subModuleData: HomeSubModuleData? = null

    private lateinit var adapter: SubjectChapterAdapter
    private var StudentAuth: String? = null
    private var orgAuth: String? = null
    private var tokenid2: String? = null
    private var disposable: CompositeDisposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subjects_chapters)
        getIntentData()
        setToolbarData()
        disposable = CompositeDisposable()
        tokenid2 = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.TOKEN_ID_2)
        StudentAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.STUDENT_AUTH)
        orgAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.ORG_AUTH)
        requestForStudyMaterials()
    }

    private fun getIntentData() {
        intent?.let {
            subModuleData = intent.getParcelableExtra(KEY_SUB_MODULE)
        }
    }

    private fun setToolbarData() {
        supportActionBar?.title = subModuleData?.title
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun requestForStudyMaterials() {
        getModuleAPIObservable()?.let {
            disposable?.add(
                it.subscribeOn(ioThread())
                    .doOnSubscribe {
                        showProgress()
                    }.observeOn(androidThread())
                    .subscribe({ response: SubjectWiseChaptersResponse ->
                        handleSubjectChaptersResponse(response)
                    }, {
                        rootView.showApiParsingErrorSnack()
                        llEmptyData.show()
                    })
            )
        }
    }

    private fun getModuleAPIObservable(): Observable<SubjectWiseChaptersResponse>? {
        return when (subModuleData?.tag) {
            AI_LIBRARY -> {
                AppService.create().getDigitalLibrarySubjectsChapters(
                    tokenid2,
                    StudentAuth,
                    orgAuth,
                    SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.CLASS_NAME)
                )
            }
            ORGANISATION_LIBRARY -> {
                AppService.create().getMySubjectsChapterStudyMaterial(
                    tokenid2,
                    StudentAuth,
                    orgAuth,
                    SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.CLASS_NAME)
                )
            }
            else -> null
        }
    }

    private fun handleSubjectChaptersResponse(response: SubjectWiseChaptersResponse) {
        if (response.status == 200 && response.data.isNotEmpty()) {
            setListAdapter(response)
        } else {
            llEmptyData.show()
        }
        hideProgress()
    }

    private fun setListAdapter(response: SubjectWiseChaptersResponse) {
        val layoutManager = LinearLayoutManager(this)
        rvList.layoutManager = layoutManager
        rvList.setHasFixedSize(true)
        val animator: ItemAnimator? = rvList.itemAnimator
        if (animator is DefaultItemAnimator) {
            animator.supportsChangeAnimations = false
        }
        adapter = SubjectChapterAdapter(response.getSectionHeadersList())
        adapter.setChildClickListener { groupPos, childPos, _ ->
            response.getSubject(groupPos)?.let {
                val chapter = response.getChapter(groupPos, childPos)
                val finalURL = BuildConfig.HTTP_KEY + it.url + chapter
                val intent = Intent(this, MainWebViewActivity::class.java)
                intent.putExtra(KEY_WEB_VIEW_URL, finalURL)
                intent.putExtra(KEY_WEB_VIEW_TITLE, chapter)
                startActivity(intent)
            }
        }
        rvList.adapter = adapter
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun showProgress() {
        rlProgress.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.clear()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
