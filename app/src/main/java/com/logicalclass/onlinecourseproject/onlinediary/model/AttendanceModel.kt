package com.logicalclass.onlinecourseproject.onlinediary.model

import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName
import com.logicalclass.onlinecourseproject.repository.Auth


data class DigitalDiaryResponse(
        val authData: Auth,
        @SerializedName("data")
        val data: DigitalDiaryModel,
        val msg: String,
        val status: Int,
        val token: String
)

data class DigitalDiaryModel(
        val className: String,
        val classWork: JsonObject,
        val diaryDate: String,
        val homeWork: JsonObject,
        val id: String,
        val org: String
) {
    fun getClassWorkMap(): Map<String, String>? {
        val result: MutableMap<String, String> = HashMap()
        for ((key, value) in classWork.entrySet()) {
            result[key] = value.toString()
        }
        return result
    }

    fun getHomeWorkMap(): Map<String, String>? {
        val result: MutableMap<String, String> = HashMap()
        for ((key, value) in homeWork.entrySet()) {
            result[key] = value.toString()
        }
        return result
    }
}