package com.logicalclass.onlinecourseproject.onlineclasses.adapters

import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.animation.Animation
import androidx.recyclerview.widget.RecyclerView
import com.logicalclass.onlinecourseproject.databinding.ItemOnlineClassesListBinding
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.onlineclasses.model.OnlineClassesModel
import com.logicalclass.onlinecourseproject.utils.show
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils

class OnlineClassesListAdapter(private val context: Context,
                               private val onlineClassesList: List<OnlineClassesModel>,
                               private val callBacks: ICallBacks?) :
        RecyclerView.Adapter<OnlineClassesListAdapter.OnlineClassesViewHolder>() {

    interface ICallBacks {
        fun onEnrollOnlineClass(position: Int, resultData: OnlineClassesModel?)
        fun onJoinOnlineClass(position: Int, resultData: OnlineClassesModel?)
    }

    override fun getItemCount(): Int {
        return onlineClassesList.size
    }

    fun getItem(position: Int): OnlineClassesModel {
        return onlineClassesList[position]
    }

    fun updateEnrollStatus(position: Int) {
        val onlineClassesModel = getItem(position)
        onlineClassesModel.student.add(
                SharedPrefsUtils.getStringPreference(context, SharedPrefsUtils.STUDENT_AUTH) ?: "")
        notifyItemChanged(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OnlineClassesViewHolder {
        return OnlineClassesViewHolder(
                ItemOnlineClassesListBinding.inflate(
                        LayoutInflater.from(context),
                        parent,
                        false
                ))
    }

    override fun onBindViewHolder(holder: OnlineClassesViewHolder, position: Int) {
        val onlineClass = getItem(position)
        holder.binding.onlineClass = onlineClass

        if (onlineClass.isLiveStatusVisible()) {
            holder.binding.classLive.show()
            manageBlinkEffect(holder.binding.classLive)
        } else {
            holder.binding.classLive.hide()
            holder.binding.classLive.clearAnimation()
        }
        val isStudentEnrolled = onlineClass.isStudentEnrolled(context)
        val isStudentJoinedAlready = onlineClass.isStudentJoinedAlready(context)

        if (!isStudentEnrolled && !isStudentJoinedAlready && onlineClass.canEnroll.equals("NO", true)) {
            holder.binding.txtExpired.visibility = VISIBLE
            holder.binding.btnEnroll.visibility = GONE
            holder.binding.txtBtnJoin.visibility = GONE
        } else if (!isStudentEnrolled && !isStudentJoinedAlready && onlineClass.canEnroll.equals("YES", true)) {
            holder.binding.btnEnroll.visibility = VISIBLE
            holder.binding.txtBtnJoin.visibility = GONE
            holder.binding.txtExpired.visibility = GONE
        } else {
            holder.binding.txtBtnJoin.visibility = VISIBLE
            holder.binding.btnEnroll.visibility = GONE
            holder.binding.txtExpired.visibility = GONE
        }
    }

    private fun manageBlinkEffect(textView: View) {
        val anim: ObjectAnimator = ObjectAnimator.ofInt(textView, "backgroundColor", Color.WHITE, Color.RED,
                Color.WHITE)
        anim.duration = 1500
        anim.setEvaluator(ArgbEvaluator())
        anim.repeatMode = ValueAnimator.REVERSE
        anim.repeatCount = Animation.INFINITE
        anim.start()
    }

    inner class OnlineClassesViewHolder(var binding: ItemOnlineClassesListBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.btnEnroll.setOnClickListener { callBacks?.onEnrollOnlineClass(adapterPosition, getItem(adapterPosition)) }
            binding.txtBtnJoin.setOnClickListener { callBacks?.onJoinOnlineClass(adapterPosition, getItem(adapterPosition)) }
        }
    }
}