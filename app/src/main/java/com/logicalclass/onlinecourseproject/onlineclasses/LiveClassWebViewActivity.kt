package com.logicalclass.onlinecourseproject.onlineclasses

import android.Manifest
import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.webkit.*
import android.webkit.PermissionRequest.RESOURCE_AUDIO_CAPTURE
import android.webkit.PermissionRequest.RESOURCE_VIDEO_CAPTURE
import android.widget.FrameLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.studymaterials.view.PdfViewActivity
import com.logicalclass.onlinecourseproject.utils.FileUtils
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.utils.show
import com.logicalclass.onlinecourseproject.utils.showSnackMessage
import kotlinx.android.synthetic.main.activity_live_class_webview.*


class LiveClassWebViewActivity : AppCompatActivity() {
    var myRequest: PermissionRequest? = null
    var isDownloadInProgress = false

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_live_class_webview)
        showProgress()
        setToolbarData()
        webView.settings.javaScriptEnabled = true
        webView.settings.loadWithOverviewMode = true
        webView.settings.useWideViewPort = true
        webView.settings.userAgentString =
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36";
        webView.settings.javaScriptCanOpenWindowsAutomatically = true
        webView.settings.mediaPlaybackRequiresUserGesture = false

        webView.webChromeClient = ChromeClient(this)

        webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
            }

            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                val url = request?.url?.toString()
                if (url?.contains("presFilename") == true) {
                    isDownloadInProgress = true
                    downloadPdfFromInternet(url, intent?.getStringExtra("download_file_name"))
                    return true
                }
                return super.shouldOverrideUrlLoading(view, request)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                if (!isDownloadInProgress)
                    hideProgress()
            }
        }
        askForRequiredPermissions(
            arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.CAMERA
            ),
            PERMISSIONS_MULTIPLE_REQUEST
        )
    }

    /*fun askForPermission(origin: String, permission: String, requestCode: Int) {
        if (ContextCompat.checkSelfPermission(applicationContext, permission) != PackageManager.PERMISSION_GRANTED) { // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else { // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, arrayOf(permission), requestCode)
            }
        } else {
            myRequest?.grant(myRequest?.resources)
        }
    }*/

    private fun askForRequiredPermissions(
        arrayOfPermissions: Array<String>,
        requestCode: Int
    ) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
            + ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            + ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
            != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                ActivityCompat.shouldShowRequestPermissionRationale
                    (this, Manifest.permission.RECORD_AUDIO) ||
                ActivityCompat.shouldShowRequestPermissionRationale
                    (this, Manifest.permission.CAMERA)
            ) {
                showPermissionConfirmationDialog(DialogInterface.OnClickListener { _, _ ->
                    ActivityCompat.requestPermissions(this, arrayOfPermissions, requestCode)
                })
            } else {
                ActivityCompat.requestPermissions(this, arrayOfPermissions, requestCode)
            }
        } else {
            myRequest?.grant(myRequest?.resources)
            webView.loadUrl(intent.getStringExtra("webview_url") ?: "")
        }
    }

    private fun downloadPdfFromInternet(url: String?, fileName: String?) {
        showProgress()
        PRDownloader.download(
            url,
            FileUtils.getOnlineClassesDocumentsPath(this),
            fileName
        ).build()
            .start(object : OnDownloadListener {
                override fun onDownloadComplete() {
                    isDownloadInProgress = false
                    hideProgress()
                    showDocumentPreviewConfirmation(fileName)
                }

                override fun onError(error: Error?) {
                    isDownloadInProgress = false
                    hideProgress()
                    webView.showSnackMessage("Error in downloading file : $error")
                }
            })
    }

    private fun navigateToPFDViewer(fileName: String?) {
        val intent = Intent(this@LiveClassWebViewActivity, PdfViewActivity::class.java)
        intent.putExtra(PdfViewActivity.VIEW_TYPE, PdfViewActivity.TYPE_STORAGE)
        intent.putExtra(
            PdfViewActivity.ACTIVITY_TYPE_ONLINE_CLASS,
            LiveClassWebViewActivity::class.java.simpleName
        )
        intent.putExtra(PdfViewActivity.PDF_FILE_NAME, fileName)
        startActivity(intent)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSIONS_MULTIPLE_REQUEST -> {
                /*Log.d("WebView", "PERMISSION FOR AUDIO")
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    myRequest?.grant(myRequest?.resources)
                    webView.loadUrl(intent.getStringExtra("webview_url"))
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }*/
                if (grantResults.isNotEmpty()) {
                    val readExternalFile = grantResults[0] == PackageManager.PERMISSION_GRANTED
                    val recordAudioPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED
                    val cameraPermission = grantResults[2] == PackageManager.PERMISSION_GRANTED
                    if (recordAudioPermission && readExternalFile && cameraPermission) {
                        myRequest?.grant(myRequest?.resources)
                        webView.loadUrl(intent.getStringExtra("webview_url") ?: "")
                    } else {
                        showPermissionConfirmationDialog(DialogInterface.OnClickListener { _, _ ->
                            requestPermissions(
                                arrayOf(
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.RECORD_AUDIO,
                                    Manifest.permission.CAMERA
                                ),
                                PERMISSIONS_MULTIPLE_REQUEST
                            )
                        })
                    }
                }
            }
        }
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun showProgress() {
        rlProgress.show()
    }

    private fun setToolbarData() {
        supportActionBar?.title = intent.getStringExtra("title")
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    open class ChromeClient internal constructor(val context: LiveClassWebViewActivity) :
        WebChromeClient() {
        private var mCustomView: View? = null
        private var mCustomViewCallback: CustomViewCallback? = null
        protected var mFullscreenContainer: FrameLayout? = null
        private var mOriginalOrientation = 0
        private var mOriginalSystemUiVisibility = 0
        override fun getDefaultVideoPoster(): Bitmap? {
            return if (mCustomView == null) {
                null
            } else BitmapFactory.decodeResource(context.resources, 2130837573)
        }

        override fun onHideCustomView() {
            (context.window.decorView as FrameLayout).removeView(mCustomView)
            mCustomView = null
            context.window.decorView.systemUiVisibility = mOriginalSystemUiVisibility
            context.requestedOrientation = mOriginalOrientation
            mCustomViewCallback?.onCustomViewHidden()
            mCustomViewCallback = null
        }

        override fun onShowCustomView(
            paramView: View?,
            paramCustomViewCallback: CustomViewCallback?
        ) {
            if (mCustomView != null) {
                onHideCustomView()
                return
            }
            mCustomView = paramView
            mOriginalSystemUiVisibility = context.window.decorView.systemUiVisibility
            mOriginalOrientation = context.requestedOrientation
            mCustomViewCallback = paramCustomViewCallback
            (context.window.decorView as FrameLayout).addView(
                mCustomView,
                FrameLayout.LayoutParams(-1, -1)
            )
            context.window.decorView.systemUiVisibility = 3846 or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        }

        override fun onPermissionRequest(request: PermissionRequest) {
            context.myRequest = request
            for (permission in request.resources) {
                when (permission) {
                    RESOURCE_VIDEO_CAPTURE,
                    RESOURCE_AUDIO_CAPTURE -> {
                        /*context.askForRequiredPermissions(
                            arrayOf(
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.RECORD_AUDIO,
                                Manifest.permission.CAMERA
                            ),
                            Companion.PERMISSIONS_MULTIPLE_REQUEST
                        )*/
                        request.grant(arrayOf(RESOURCE_AUDIO_CAPTURE, RESOURCE_VIDEO_CAPTURE))
                    }
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /*override fun onBackPressed() {
        if (this.webView.canGoBack()) {
            this.webView.goBack()
        } else {
            super.onBackPressed()
        }
    }*/

    private fun showPermissionConfirmationDialog(onClickListener: DialogInterface.OnClickListener) {
        if (!isFinishing)
            AlertDialog.Builder(this)
                .setMessage("Please Grant Permissions to enable microphone, camera and download class documents.")
                .setPositiveButton(android.R.string.yes, onClickListener)
                .setNegativeButton(android.R.string.no, null)
                .show()
    }

    private fun showDocumentPreviewConfirmation(fileName: String?) {
        if (!isFinishing)
            AlertDialog.Builder(this)
                .setMessage("Document has been downloaded. Would you like to open it?")
                .setPositiveButton(android.R.string.yes) { _, _ -> navigateToPFDViewer(fileName) }
                .setNegativeButton(android.R.string.no, null)
                .show()
    }

    override fun onPause() {
        webView.onPause()
        webView.pauseTimers()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        webView.resumeTimers()
        webView.onResume()
    }

    override fun onDestroy() {
        webView.destroy()
        super.onDestroy()
    }

    companion object {
        const val PERMISSIONS_MULTIPLE_REQUEST = 101
    }
}
