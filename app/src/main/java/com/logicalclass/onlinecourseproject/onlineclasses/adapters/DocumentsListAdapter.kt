package com.logicalclass.onlinecourseproject.onlineclasses.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.logicalclass.onlinecourseproject.databinding.ItemOnlineClassDocumentsListBinding
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.utils.show
import java.io.File

class DocumentsListAdapter(private val context: Context,
                           private val documentsList: Array<File>,
                           val callBack: ICallBack) :
        RecyclerView.Adapter<DocumentsListAdapter.DocumentViewHolder>() {

    interface ICallBack {
        fun onFileSelected(position: Int, file: File?)
    }

    override fun getItemCount(): Int {
        return documentsList.size
    }

    private fun getItem(position: Int): File {
        return documentsList[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DocumentViewHolder {
        return DocumentViewHolder(
                ItemOnlineClassDocumentsListBinding.inflate(
                        LayoutInflater.from(context),
                        parent,
                        false
                ))
    }

    override fun onBindViewHolder(holder: DocumentViewHolder, position: Int) {
        val documentModel = getItem(position)
        val arrayNames = documentModel.name.split("##")
        if (arrayNames.size == 3) {
            holder.binding.txtSubjectLabel.show()
            holder.binding.txtSubject.show()
            holder.binding.txtDateTimeLabel.show()
            holder.binding.txtDateTime.show()
            holder.binding.txtOnlineClass.text = arrayNames[0]
            holder.binding.txtSubject.text = arrayNames[1]
            holder.binding.txtDateTime.text = arrayNames[2]
        } else {
            holder.binding.txtOnlineClass.text = documentModel.name
            holder.binding.txtSubjectLabel.hide()
            holder.binding.txtSubject.hide()
            holder.binding.txtDateTimeLabel.hide()
            holder.binding.txtDateTime.hide()
        }
    }

    inner class DocumentViewHolder(var binding: ItemOnlineClassDocumentsListBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener {
                callBack.onFileSelected(adapterPosition, getItem(adapterPosition))
            }
        }
    }
}