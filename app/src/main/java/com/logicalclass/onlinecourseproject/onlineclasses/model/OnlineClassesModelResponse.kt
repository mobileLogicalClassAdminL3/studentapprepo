package com.logicalclass.onlinecourseproject.onlineclasses.model

import android.content.Context
import com.google.gson.annotations.SerializedName
import com.logicalclass.onlinecourseproject.repository.Auth
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils

data class OnlineClassesModelResponse(
        val authData: Auth,
        @SerializedName("data")
        val data: Data,
        val msg: String,
        val status: Int,
        val token: String
)

data class Data(
        @SerializedName("myOrgClasses")
        val onlineClasses: List<OnlineClassesModel>
)

data class OnlineClassesModel(
        val about: String,
        val accessLevels: List<String>,
        val attendedBy: List<String>,
        val auth: String,
        val batch: String,
        val chapter: String,
        @SerializedName("class")
        val className: String,
        val dateTime: String,
        val forExamination: List<String>,
        val id: String,
        val isActive: String,
        val org: String,
        val price: String,
        val status: String,
        val student: ArrayList<String>,
        val subject: String,
        val time: Any,
        val topic: List<String>,
        val canEnroll: String
) {
    fun getTopicsString(): String? {
        val topicsString = StringBuilder()
        for (i in topic.indices) {
            topicsString.append(topic.get(i))
        }
        return topicsString.toString()
    }

    fun isStudentEnrolled(context: Context): Boolean {
        student.forEach { i ->
            if (SharedPrefsUtils.getStringPreference(context, SharedPrefsUtils.STUDENT_AUTH) == i)
                return true
        }
        return false
    }

    fun isStudentJoinedAlready(context: Context): Boolean {
        attendedBy.forEach { i ->
            if (SharedPrefsUtils.getStringPreference(context, SharedPrefsUtils.STUDENT_AUTH) == i)
                return true
        }
        return false
    }

    fun isLiveStatusVisible(): Boolean {
        return status.equals("live", true)
    }
}

data class EnrollResponse(
        val authData: Auth,
        val msg: String,
        val status: Int,
        val token: String
)

data class JoinClassResponse(
        val authData: Auth,
        val msg: String,
        @SerializedName("data")
        val data: JoinClassModel,
        val status: Int,
        val token: String
)

data class JoinClassModel(
        val StudentAuth: String,
        @SerializedName("class")
        val className: String,
        val classId: String,
        val classStatusUrl: String,
        val orgAuth: String,
        val primaryUrl: String
)

data class DocumentModel(
        val fileName: String
)

