package com.logicalclass.onlinecourseproject.onlineclasses

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.onlineclasses.adapters.DocumentsListAdapter
import com.logicalclass.onlinecourseproject.utils.show
import com.logicalclass.onlinecourseproject.studymaterials.view.PdfViewActivity
import com.logicalclass.onlinecourseproject.utils.FileUtils
import kotlinx.android.synthetic.main.activity_online_class_documents.*
import java.io.File

class OnlineClassDocumentsListActivity : AppCompatActivity(), DocumentsListAdapter.ICallBack {
    private lateinit var documentsListAdapter: DocumentsListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_online_class_documents)
        setToolbarData()
        checkForRequiredPermissions()
    }

    private fun checkForRequiredPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showPermissionConfirmationDialog(DialogInterface.OnClickListener { _, _ ->
                    requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                            LiveClassWebViewActivity.PERMISSIONS_MULTIPLE_REQUEST)
                })
            } else {
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        LiveClassWebViewActivity.PERMISSIONS_MULTIPLE_REQUEST)
            }
        } else {
            setFilesListAdapter()
        }
    }

    private fun showPermissionConfirmationDialog(onClickListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this)
                .setMessage("Please Grant Permissions to access storage.")
                .setPositiveButton(android.R.string.yes, onClickListener)
                .setNegativeButton(android.R.string.no, null)
                .show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LiveClassWebViewActivity.PERMISSIONS_MULTIPLE_REQUEST -> {
                if (grantResults.isNotEmpty()) {
                    val readExternalFile = grantResults[0] == PackageManager.PERMISSION_GRANTED
                    if (readExternalFile) {
                        setFilesListAdapter()
                    } else {
                        showPermissionConfirmationDialog(DialogInterface.OnClickListener { _, _ ->
                            requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                                    LiveClassWebViewActivity.PERMISSIONS_MULTIPLE_REQUEST)
                        })
                    }
                }
            }
        }
    }

    private fun setFilesListAdapter() {
        val filesList = getFilesList()
        if (filesList != null && filesList.isNotEmpty()) {
            val layoutManager = LinearLayoutManager(this)
            rvList.layoutManager = layoutManager
            documentsListAdapter = DocumentsListAdapter(this, filesList, this)
            rvList.adapter = documentsListAdapter
        } else {
            llEmptyData.show()
        }
    }

    private fun getFilesList(): Array<File>? {
        val list = File(FileUtils.getOnlineClassesDocumentsPath(this))
        return list.listFiles()
    }

    private fun setToolbarData() {
        supportActionBar?.title = "Documents"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun showProgress() {
        rlProgress.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onFileSelected(position: Int, file: File?) {
        val intent = Intent(this, PdfViewActivity::class.java)
        intent.putExtra(PdfViewActivity.VIEW_TYPE, PdfViewActivity.TYPE_STORAGE)
        intent.putExtra(PdfViewActivity.PDF_FILE_NAME, file?.name)
        intent.putExtra(PdfViewActivity.ACTIVITY_TYPE_ONLINE_CLASS, OnlineClassDocumentsListActivity::class.java.simpleName)
        startActivity(intent)
    }
}
