package com.logicalclass.onlinecourseproject.onlineclasses

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.logicalclass.onlinecourseproject.BuildConfig
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.onlineclasses.adapters.OnlineClassesListAdapter
import com.logicalclass.onlinecourseproject.onlineclasses.model.*
import com.logicalclass.onlinecourseproject.repository.AppService
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.androidThread
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.ioThread
import com.logicalclass.onlinecourseproject.utils.*
import com.logicalclass.onlinecourseproject.utils.FileUtils.getOnlineClassDownloadFileName
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_online_classes.*

class OnlineClassesActivity : AppCompatActivity(), OnlineClassesListAdapter.ICallBacks {

    private lateinit var onlineClassListAdapter: OnlineClassesListAdapter
    private var StudentAuth: String? = null
    private var orgAuth: String? = null
    private var tokenid2: String? = null
    private var disposable: CompositeDisposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_online_classes)
        setToolbarData()
        disposable = CompositeDisposable()
        tokenid2 = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.TOKEN_ID_2)
        StudentAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.STUDENT_AUTH)
        orgAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.ORG_AUTH)
        requestForOnlineClassesList()

        txtViewHistory.text = getString(R.string.link_to_saved_class_documents).getHtmlText()
        txtViewHistory.setOnClickListener {
            startActivity(Intent(this, OnlineClassDocumentsListActivity::class.java))
        }
    }

    private fun setToolbarData() {
        supportActionBar?.title = getString(R.string.title_online_classes)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun requestForOnlineClassesList() {
        disposable?.add(AppService.create().onlineClasses(
                tokenid2,
                StudentAuth,
                orgAuth
        ).subscribeOn(ioThread())
                .doOnSubscribe {
                    showProgress()
                }.observeOn(androidThread())
                .subscribe({ response: OnlineClassesModelResponse ->
                    handleOnlineClassesResponse(response)
                }, {
                    rootView.showApiParsingErrorSnack()
                    llEmptyData.show()
                })
        )
    }

    private fun handleOnlineClassesResponse(response: OnlineClassesModelResponse) {
        if (response.status == 200 && response.data.onlineClasses.isNotEmpty()) {
            val list = response.data.onlineClasses
            val layoutManager = LinearLayoutManager(this)
            onlineClassListAdapter = OnlineClassesListAdapter(this, list, this)
            rvList.layoutManager = layoutManager
            rvList.adapter = onlineClassListAdapter
        } else {
            llEmptyData.show()
        }
        hideProgress()
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun showProgress() {
        rlProgress.show()
    }

    override fun onEnrollOnlineClass(position: Int, resultData: OnlineClassesModel?) {
        requestForEnrollOnlineClass(position, resultData?.id)
    }

    override fun onJoinOnlineClass(position: Int, resultData: OnlineClassesModel?) {
        requestForStartOnlineClass(resultData)
    }

    private fun requestForEnrollOnlineClass(position: Int, classId: String?) {
        disposable?.add(AppService.create().enrollOnlineClass(
                tokenid2,
                StudentAuth,
                orgAuth,
                classId
        ).subscribeOn(ioThread())
                .observeOn(androidThread())
                .doOnSubscribe {
                    showProgress()
                }.doFinally {
                    hideProgress()
                }
                .subscribe({ response: EnrollResponse ->
                    handleEnrollOnlineClassResponse(position, response)
                }, {
                    rootView.showApiParsingErrorSnack()
                })
        )
    }

    private fun handleEnrollOnlineClassResponse(position: Int, response: EnrollResponse) {
        if (response.status == 200) {
            onlineClassListAdapter.updateEnrollStatus(position)
        } else {
            rootView.showSnackMessage(response.msg)
        }
    }

    private fun requestForStartOnlineClass(resultData: OnlineClassesModel?) {
        disposable?.add(AppService.create().startClass(
                tokenid2,
                StudentAuth,
                orgAuth,
                resultData?.id
        ).subscribeOn(ioThread())
                .doOnSubscribe {
                    showProgress()
                }
                .observeOn(androidThread())
                .subscribe({ response: JoinClassResponse ->
                    handleStartOnlineClassResponse(response, resultData)
                }, {
                    rootView.showApiParsingErrorSnack()
                })
        )
    }

    private fun handleStartOnlineClassResponse(response: JoinClassResponse, resultData: OnlineClassesModel?) {
        hideProgress()
        if (response.status == 200) {
            val intent = Intent(this, LiveClassWebViewActivity::class.java)
            intent.putExtra("webview_url", prepareJoinClassURL(response.data))
            resultData?.let {
                intent.putExtra("download_file_name", getOnlineClassDownloadFileName(it.batch, it.subject, it.dateTime))
            } ?: run {
                intent.putExtra("download_file_name", "OnlineClass")
            }
            intent.putExtra("title", resultData?.batch)
            startActivity(intent)
        } else {
            rootView.showSnackMessage(response.msg)
        }
    }

    private fun prepareJoinClassURL(joinClassData: JoinClassModel): String {
        return BuildConfig.HTTP_KEY.plus(joinClassData.primaryUrl).plus("?").plus("classStatusUrl=").plus(joinClassData.classStatusUrl)
                .plus("&&").plus("orgAuth=").plus(joinClassData.orgAuth)
                .plus("&&").plus("StudentAuth=").plus(joinClassData.StudentAuth)
                .plus("&&").plus("classId=").plus(joinClassData.classId)
                .plus("&&").plus("class=").plus(joinClassData.className)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.clear()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
