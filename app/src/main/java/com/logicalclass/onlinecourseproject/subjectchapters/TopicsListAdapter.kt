package com.logicalclass.onlinecourseproject.subjectchapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.logicalclass.onlinecourseproject.databinding.ItemStudyMaterialTopicListBinding
import com.logicalclass.onlinecourseproject.models.TopicModel
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.utils.show

class TopicsListAdapter(
    private val context: Context,
    private val studyMaterialsList: List<TopicModel>,
    val callBack: ICallBack
) :
    RecyclerView.Adapter<TopicsListAdapter.TopicViewHolder>() {

    interface ICallBack {
        fun onTopicSelected(position: Int, resultData: TopicModel?)
    }

    override fun getItemCount(): Int {
        return studyMaterialsList.size
    }

    private fun getItem(position: Int): TopicModel {
        return studyMaterialsList[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopicViewHolder {
        return TopicViewHolder(
            ItemStudyMaterialTopicListBinding.inflate(
                LayoutInflater.from(context),
                parent,
                false
            )
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: TopicViewHolder, position: Int) {
        val topicModel = getItem(position)
        holder.binding.topic = topicModel
        holder.binding.txtTopic.text = topicModel.getNameOfTheTopic()

        topicModel.basic?.let {
            holder.binding.txtBasic.text = "Basic: $it"
            holder.binding.txtBasic.show()
        } ?: kotlin.run {
            holder.binding.txtBasic.hide()
        }

        topicModel.medium?.let {
            holder.binding.txtMedium.text = "Medium: $it"
            holder.binding.txtMedium.show()
        } ?: kotlin.run {
            holder.binding.txtMedium.hide()
        }

        topicModel.tough?.let {
            holder.binding.txtTough.text = "Tough: $it"
            holder.binding.txtTough.show()
        } ?: kotlin.run {
            holder.binding.txtTough.hide()
        }
    }

    inner class TopicViewHolder(var binding: ItemStudyMaterialTopicListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener {
                callBack.onTopicSelected(adapterPosition, getItem(adapterPosition))
            }
        }
    }
}