package com.logicalclass.onlinecourseproject.subjectchapters

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.ItemAnimator
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.home.*
import com.logicalclass.onlinecourseproject.home.SubModuleActivity.Companion.KEY_MAIN_MODULE_NAME
import com.logicalclass.onlinecourseproject.home.SubModuleActivity.Companion.KEY_SUB_MODULE_NAME
import com.logicalclass.onlinecourseproject.home.SubModuleActivity.Companion.KEY_SUB_MODULE_TYPE
import com.logicalclass.onlinecourseproject.repository.AppService
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.androidThread
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.ioThread
import com.logicalclass.onlinecourseproject.studymaterials.adapters.SubjectChapterAdapter
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.utils.show
import com.logicalclass.onlinecourseproject.utils.showApiParsingErrorSnack
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_announcements.*

class SubjectsChaptersActivity : AppCompatActivity() {
    private var mainModuleName: String? = null
    private var subModuleType: String? = null
    private var subModuleName: String? = null

    private lateinit var adapter: SubjectChapterAdapter
    private var orgAuth: String? = null
    private var tokenid2: String? = null
    private var disposable: CompositeDisposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subjects_chapters)
        getIntentData()
        setToolbarData()

        disposable = CompositeDisposable()
        tokenid2 = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.TOKEN_ID_2)
        orgAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.ORG_AUTH)
        requestForSubjectChaptersList()
    }

    private fun getIntentData() {
        intent?.let {
            mainModuleName = intent.getStringExtra(KEY_MAIN_MODULE_NAME)
            subModuleType = intent.getStringExtra(KEY_SUB_MODULE_TYPE)
            subModuleName = intent.getStringExtra(KEY_SUB_MODULE_NAME)
        }
    }

    private fun setToolbarData() {
        supportActionBar?.title = mainModuleName
        supportActionBar?.subtitle = subModuleName
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun requestForSubjectChaptersList() {
        getModuleAPIObservable()?.let {
            disposable?.add(
                it.subscribeOn(ioThread())
                    .doOnSubscribe {
                        showProgress()
                    }.observeOn(androidThread())
                    .subscribe({ response: SubjectsChaptersResponse ->
                        handleSubjectChaptersResponse(response)
                    }, {
                        rootView.showApiParsingErrorSnack()
                        llEmptyData.show()
                    })
            )
        }
    }

    private fun getModuleAPIObservable(): Observable<SubjectsChaptersResponse>? {
        return when (subModuleType) {
            TIME_TAKEN_QUESTIONS -> {
                AppService.create().getViewOwnTimeTakenSubjectChaptersList(
                    tokenid2,
                    orgAuth
                )
            }
            MISTAKE_OF_MOST_STUDENTS -> {
                AppService.create().getMistakesOfMostStudentsSubjectChaptersList(
                    tokenid2,
                    orgAuth
                )
            }
            PRACTICE_TIME_WASTED_QUESTIONS_OF_MOST_STUDENTS -> {
                AppService.create().getPracticeTimeWastedOfMostStudentsSubjectChaptersList(
                    tokenid2,
                    orgAuth
                )
            }
            WRONG_OF_TOPPER -> {
                AppService.create().getWrongOfTopperSubjectChaptersList(
                    tokenid2,
                    orgAuth
                )
            }
            TIME_WASTED_QUESTION_OF_TOPPER -> {
                AppService.create().getTimeWastedOfTopperSubjectChaptersList(
                    tokenid2,
                    orgAuth
                )
            }
            PRACTICE_OWN_MISTAKE_QUESTIONS -> {
                AppService.create().getPractiseOwnMistakesSubjectChaptersList(
                    tokenid2,
                    orgAuth
                )
            }
            else -> null
        }
    }

    private fun handleSubjectChaptersResponse(response: SubjectsChaptersResponse) {
        if (response.status == 200 && response.data.isNotEmpty()) {
            setListAdapter(response)
        } else {
            llEmptyData.show()
        }
        hideProgress()
    }

    private fun setListAdapter(response: SubjectsChaptersResponse) {
        val layoutManager = LinearLayoutManager(this)
        rvList.layoutManager = layoutManager
        rvList.setHasFixedSize(true)
        val animator: ItemAnimator? = rvList.itemAnimator
        if (animator is DefaultItemAnimator) {
            animator.supportsChangeAnimations = false
        }
        adapter = SubjectChapterAdapter(response.getSectionHeadersList())
        adapter.setChildClickListener { groupPos, childPos, _ ->
            moveToNextScreen(response, groupPos, childPos)
        }
        rvList.adapter = adapter
    }

    private fun moveToNextScreen(
        response: SubjectsChaptersResponse,
        groupPos: Int,
        childPos: Int
    ) {
        val subject = response.getSubject(groupPos)?.subject
        val chapter = response.getChapter(groupPos, childPos)?.name
        val intent = Intent(this, TopicsActivity::class.java)
        intent.putExtra("subject", subject)
        intent.putExtra("chapter", chapter)
        intent.putExtra("MODULE_TYPE", subModuleType)
        startActivity(intent)
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun showProgress() {
        rlProgress.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.clear()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
