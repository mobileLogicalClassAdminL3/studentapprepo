package com.logicalclass.onlinecourseproject.subjectchapters

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.logicalclass.onlinecourseproject.BuildConfig
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.home.*
import com.logicalclass.onlinecourseproject.mistakesofstudents.PracticeQuestionsActivity
import com.logicalclass.onlinecourseproject.models.StudyMaterialsTopicsResponse
import com.logicalclass.onlinecourseproject.models.TopicModel
import com.logicalclass.onlinecourseproject.models.TopicsResponse
import com.logicalclass.onlinecourseproject.repository.AppService
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.androidThread
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.ioThread
import com.logicalclass.onlinecourseproject.studymaterials.view.PdfViewActivity
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.utils.show
import com.logicalclass.onlinecourseproject.utils.showApiParsingErrorSnack
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_topics.*

class TopicsActivity : AppCompatActivity(), TopicsListAdapter.ICallBack {
    private var subject: String? = null
    private var chapter: String? = null
    private lateinit var topicsListAdapter: TopicsListAdapter
    private var studentAuth: String? = null
    private var orgAuth: String? = null
    private var tokenid2: String? = null
    private var disposable: CompositeDisposable? = null
    private var baseURLForFile: String? = null
    private var moduleType: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_topics)
        getIntentData()
        setToolbarData()
        disposable = CompositeDisposable()
        tokenid2 = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.TOKEN_ID_2)
        studentAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.STUDENT_AUTH)
        orgAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.ORG_AUTH)
        requestForTopicsList()
    }

    private fun getIntentData() {
        subject = intent?.getStringExtra("subject")
        chapter = intent?.getStringExtra("chapter")
        moduleType = intent.getStringExtra("MODULE_TYPE")

        /*txtSubjectName.text = subject
        txtChapterName.text = chapter*/
    }

    private fun setToolbarData() {
        supportActionBar?.title = subject
        supportActionBar?.subtitle = chapter
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun requestForTopicsList() {
        getModuleAPIObservable()?.let {
            disposable?.add(
                it.subscribeOn(ioThread())
                    .doOnSubscribe {
                        showProgress()
                    }.observeOn(androidThread())
                    .subscribe({ response ->
                        handleTopicsListResponse(response)
                    }, {
                        rootView.showApiParsingErrorSnack()
                        llEmptyData.show()
                    })
            )
        }
    }

    private fun getModuleAPIObservable(): Observable<*>? {
        return when (moduleType) {
            STUDY_MATERIALS -> {
                AppService.create().getStudyMaterialTopics(
                    tokenid2,
                    studentAuth,
                    orgAuth,
                    SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.CLASS_NAME),
                    subject,
                    chapter
                )
            }
            TIME_TAKEN_QUESTIONS -> {
                AppService.create().getViewOwnTimeTakenTopicsList(
                    tokenid2,
                    orgAuth,
                    subject,
                    chapter
                )
            }
            MISTAKE_OF_MOST_STUDENTS -> {
                AppService.create().getMistakesOfMostStudentsTopicsList(
                    tokenid2,
                    orgAuth,
                    subject,
                    chapter
                )
            }
            PRACTICE_TIME_WASTED_QUESTIONS_OF_MOST_STUDENTS -> {
                AppService.create().getPracticeTimeWastedOfMostStudentsTopicsList(
                    tokenid2,
                    orgAuth,
                    subject,
                    chapter
                )
            }
            WRONG_OF_TOPPER -> {
                AppService.create().getWrongOfTopperTopicsList(
                    tokenid2,
                    orgAuth,
                    subject,
                    chapter
                )
            }
            TIME_WASTED_QUESTION_OF_TOPPER -> {
                AppService.create().getTimeWastedOfTopperTopicsList(
                    tokenid2,
                    orgAuth,
                    subject,
                    chapter
                )
            }
            PRACTICE_OWN_MISTAKE_QUESTIONS -> {
                AppService.create().getPractiseOwnMistakesTopicsList(
                    tokenid2,
                    orgAuth,
                    subject,
                    chapter
                )
            }
            else -> null
        }
    }

    private fun handleTopicsListResponse(response: Any) {
        when (response) {
            is StudyMaterialsTopicsResponse -> {
                handleStudyMaterialsTopicsResponse(response)
            }
            is TopicsResponse -> {
                handleMainTopicsResponse(response)
            }
        }
        hideProgress()
    }

    private fun handleStudyMaterialsTopicsResponse(response: StudyMaterialsTopicsResponse) {
        if (response.status == 200 && response.data.studyMaterials.isNotEmpty()) {
            val list = response.data.studyMaterials
            baseURLForFile = response.data.basicAssetsUrl
            setTopicsListAdapter(list)
        } else {
            llEmptyData.show()
        }
    }

    private fun handleMainTopicsResponse(response: TopicsResponse) {
        if (response.status == 200 && response.data.isNotEmpty()) {
            val list = response.data
            setTopicsListAdapter(list)
        } else {
            llEmptyData.show()
        }
    }

    private fun setTopicsListAdapter(list: List<TopicModel>) {
        val layoutManager = LinearLayoutManager(this)
        topicsListAdapter = TopicsListAdapter(this, list, this)
        rvList.layoutManager = layoutManager
        rvList.adapter = topicsListAdapter
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun showProgress() {
        rlProgress.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.clear()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onTopicSelected(position: Int, resultData: TopicModel?) {
        if (moduleType == STUDY_MATERIALS) {
            val finalURL = BuildConfig.HTTP_KEY + baseURLForFile + "/" + resultData?.fileName
            val intent = Intent(this, PdfViewActivity::class.java)
            intent.putExtra(PdfViewActivity.VIEW_TYPE, PdfViewActivity.TYPE_INTERNET)
            intent.putExtra(PdfViewActivity.PDF_URL, finalURL)
            intent.putExtra(PdfViewActivity.TOPIC_NAME, resultData?.getNameOfTheTopic())
            intent.putExtra(PdfViewActivity.PDF_FILE_NAME, resultData?.fileName)
            startActivity(intent)
        } else {
            val intent = Intent(this, PracticeQuestionsActivity::class.java)
            intent.putExtra("subject", subject)
            intent.putExtra("chapter", chapter)
            intent.putExtra("topic", resultData?.topic)
            intent.putExtra("MODULE_TYPE", moduleType)
            startActivity(intent)
        }
    }
}
