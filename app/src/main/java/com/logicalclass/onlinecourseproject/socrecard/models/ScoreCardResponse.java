package com.logicalclass.onlinecourseproject.socrecard.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.logicalclass.onlinecourseproject.models.AuthData;

public class ScoreCardResponse {
    @SerializedName("status")
    @Expose
    public Integer status;
    @SerializedName("authData")
    @Expose
    public AuthData authData;
    @SerializedName("data")
    @Expose
    public ScoreCardData data;
    @SerializedName("msg")
    @Expose
    public String msg;
    @SerializedName("token")
    @Expose
    public String token;
}