package com.logicalclass.onlinecourseproject.socrecard.view

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.TabLayoutOnPageChangeListener
import com.google.gson.Gson
import com.google.gson.JsonIOException
import com.logicalclass.onlinecourseproject.BuildConfig
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.adapters.ScoreCardPagerAdapter
import com.logicalclass.onlinecourseproject.service.MyFirebaseMessagingService.Companion.EXAM_ID
import com.logicalclass.onlinecourseproject.socrecard.models.ScoreCardResponse
import com.logicalclass.onlinecourseproject.utils.*
import com.logicalclass.onlinecourseproject.viewresult.view.ExamResultsActivity
import kotlinx.android.synthetic.main.activity_score_card.*

class ScoreCardActivity : AppCompatActivity(), TabLayout.OnTabSelectedListener {
    var scoreCardResponse: ScoreCardResponse? = null
    var url = "${BuildConfig.BASE_URL}mockTestChart?"
    private var examId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_score_card)
        examId = intent?.extras?.getString(EXAM_ID)
        setToolbarData()
        requestForExamsList()
    }

    private fun setToolbarData() {
        supportActionBar?.title = getString(R.string.title_score_card)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun setViewPagerAdapter() {
        tabLayout.setupWithViewPager(pager)
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL
        val adapter = ScoreCardPagerAdapter(supportFragmentManager, examId)
        pager.adapter = adapter
        pager.addOnPageChangeListener(TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(this)
    }

    private fun requestForExamsList() {
        showProgress()
        val stringRequest = StringRequest(
                Request.Method.GET,
                url
                        + "examId="
                        + examId
                        + "&"
                        + "auth="
                        + SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.TOKEN_ID_2),
                Response.Listener { response ->
                    try {
                        val gson = Gson()
                        scoreCardResponse = gson.fromJson(response, ScoreCardResponse::class.java)
                        if (scoreCardResponse?.status == 200) {
                            setViewPagerAdapter()
                        } else {
                            rootView.showApiErrorSnack()
                        }
                        hideProgress()
                    } catch (e: JsonIOException) {
                        hideProgress()
                        e.printStackTrace()
                        rootView.showSnackMessage("ScoreCardActivity Error$e")
                    }
                },
                Response.ErrorListener { error ->
                    hideProgress()
                    rootView.showSnackMessage("ScoreCardActivity Error$error")
                    error.printStackTrace()
                })
        val requestQueue = Volley.newRequestQueue(this)
        requestQueue.add(stringRequest)
    }

    override fun onTabSelected(tab: TabLayout.Tab?) {
        pager.currentItem = tab?.position ?: 0
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun showProgress() {
        rlProgress.show()
    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {}
    override fun onTabReselected(tab: TabLayout.Tab?) {}

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        if (intent?.getBooleanExtra("isFromMockTest", false) == true) {
            val examResultsIntent = Intent(this, ExamResultsActivity::class.java)
            startActivity(examResultsIntent)
        }
        super.onBackPressed()
    }
}