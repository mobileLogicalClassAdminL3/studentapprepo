package com.logicalclass.onlinecourseproject.socrecard.view

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.databinding.FragmentScoreCardDetailsBinding
import com.logicalclass.onlinecourseproject.databinding.ItemSubjectDetailsBinding
import com.logicalclass.onlinecourseproject.socrecard.models.ScoreCardResponse
import lecho.lib.hellocharts.model.PieChartData
import lecho.lib.hellocharts.model.SliceValue
import java.util.*

class FragmentScoreCardDetails : Fragment() {
    private var scoreCardResponse: ScoreCardResponse? = null
    private var position = 0
    private val pieData: MutableList<SliceValue> = ArrayList()
    lateinit var binding: FragmentScoreCardDetailsBinding
    private val rnd = Random()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.fragment_score_card_details, container, false)
        initComponents()
        setDetails()
        return binding.root
    }

    private fun initComponents() {
        scoreCardResponse = (activity as ScoreCardActivity?)?.scoreCardResponse
        if (arguments != null) {
            position = arguments?.getInt("position") ?: 0
        }
    }

    private fun setDetails() {
        when (position) {
            0 -> {
                binding.txtFirstColumn.text = "Subject"
                binding.txtSecondColumn.text = "Score (for 100)"
                setDynamicData(scoreCardResponse?.data?.getSubWiseScore())
            }
            1 -> {
                binding.txtFirstColumn.text = "Skill"
                binding.txtSecondColumn.text = "%"
                setDynamicData(scoreCardResponse?.data?.getAnalytics())
            }
            2 -> {
                binding.txtFirstColumn.text = "Subject"
                binding.txtSecondColumn.text = "Time"
                setDynamicData(scoreCardResponse?.data?.getSubTime())
            }
            3 -> {
                binding.txtFirstColumn.text = "Subject"
                binding.txtSecondColumn.text = "%"
                setDynamicData(scoreCardResponse?.data?.getSubWiseScore())
            }
            else -> {
            }
        }
    }

    private fun getTotalPercentValue(map: Map<String, String>): Float {
        var value = 0f
        for ((_, value1) in map) {
            try {
                value += value1.toFloat()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return value
    }

    private fun setDynamicData(map: Map<String, String>?) {
        pieData.clear()
        map?.let {
            for ((key, value) in it) {
                val itemBinding = ItemSubjectDetailsBinding.inflate(LayoutInflater.from(activity), null, false)
                itemBinding.txtName.text = key
                itemBinding.txtValue.text = if (position == 2) getMinutesSecondsDisplayValue(value.toInt()) else value
                binding.rootLayout.addView(itemBinding.root)
                pieData.add(SliceValue(value.toFloat(), randomColor).setLabel(key))
            }
            val pieChartData = PieChartData(pieData)
            pieChartData.setHasLabels(true).valueLabelTextSize = 15
            binding.chart.pieChartData = pieChartData
        }
    }

    private val randomColor: Int
        get() = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))

    private fun getMinutesSecondsDisplayValue(totalSecs: Int): String {
        val minutes = totalSecs % 3600 / 60
        val seconds = totalSecs % 60
        return String.format(Locale.getDefault(), "%2d mins %2d sec", minutes, seconds)
    }
}