package com.logicalclass.onlinecourseproject.socrecard.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AnalyticsDetails {
    @SerializedName("Application Ability")
    @Expose
    public List<String> applicationAbility = null;
    @SerializedName("Conceptual Understanding")
    @Expose
    public List<String> conceptualUnderstanding = null;
    @SerializedName("Problem Solving Skills")
    @Expose
    public List<String> problemSolvingSkills = null;
    @SerializedName("Higher Order Thinking Skills (HOTS)")
    @Expose
    public List<String> higherOrderThinkingSkillsHOTS = null;
}