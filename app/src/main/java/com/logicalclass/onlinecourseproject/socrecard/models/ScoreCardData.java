package com.logicalclass.onlinecourseproject.socrecard.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.logicalclass.onlinecourseproject.models.ExamResultDetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ScoreCardData {
    @SerializedName("student")
    @Expose
    public String student;
    @SerializedName("examId")
    @Expose
    public String examId;
    @SerializedName("details")
    @Expose
    public Details details;
    @SerializedName("examDetails")
    @Expose
    public List<ExamResultDetails> examDetails = null;
    @SerializedName("perQuestionMarkTime")
    @Expose
    public List<PerQuestionMarkTime> perQuestionMarkTime = null;
    @SerializedName("orgName")
    @Expose
    public String orgName;
    @SerializedName("orgLogo")
    @Expose
    public String orgLogo;
    @SerializedName("studentName")
    @Expose
    public String studentName;
    @SerializedName("studentClass")
    @Expose
    public String studentClass;
    @SerializedName("analytics")
    @Expose
    public JsonElement analytics = null;
    @SerializedName("overAll")
    @Expose
    public JsonElement overAll = null;
    @SerializedName("subTime")
    @Expose
    public JsonElement subTime = null;
    @SerializedName("subWiseScore")
    @Expose
    public JsonElement subWiseScore = null;

    public Map<String, String> getASHashMapData(JsonElement jsonElement) {
        HashMap<String, String> mapData = new HashMap<>();
        JsonArray jsonArray = jsonElement.getAsJsonArray();
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject obj = jsonArray.get(i).getAsJsonObject();
            Set<Map.Entry<String, JsonElement>> entries = obj.entrySet();//will return members of your object
            for (Map.Entry<String, JsonElement> entry : entries) {
                mapData.put(entry.getKey(), entry.getValue().getAsString());
            }
        }
        return mapData;
    }

    public Map<String, String> getAnalytics() {
        return getASHashMapData(analytics);
    }

    public void setAnalytics(JsonElement analytics) {
        this.analytics = analytics;
    }

    public Map<String, String> getOverAll() {
        return getASHashMapData(overAll);
    }

    public void setOverAll(JsonElement overAll) {
        this.overAll = overAll;
    }

    public Map<String, String> getSubTime() {
        return getASHashMapData(subTime);
    }

    public void setSubTime(JsonElement subTime) {
        this.subTime = subTime;
    }

    public Map<String, String> getSubWiseScore() {
        return getASHashMapData(subWiseScore);
    }

    public void setSubWiseScore(JsonElement subWiseScore) {
        this.subWiseScore = subWiseScore;
    }
}

class OverAll {
    @SerializedName("Maths")
    @Expose
    public Integer maths;
    @SerializedName("Physics")
    @Expose
    public Integer physics;
    @SerializedName("Chemistry")
    @Expose
    public Integer chemistry;
    @SerializedName("Biology")
    @Expose
    public Integer biology;
}

class PerQuestionMarkTime {
    @SerializedName("examination")
    @Expose
    public String examination;
    @SerializedName("time")
    @Expose
    public String time;
    @SerializedName("mark")
    @Expose
    public String mark;
}

class SubTime {
    @SerializedName("Maths")
    @Expose
    public Integer maths;
    @SerializedName("Physics")
    @Expose
    public Integer physics;
    @SerializedName("Chemistry")
    @Expose
    public Integer chemistry;
    @SerializedName("Biology")
    @Expose
    public Integer biology;
}

class SubWiseScore {
    @SerializedName("Maths")
    @Expose
    public Double maths;
    @SerializedName("Physics")
    @Expose
    public Integer physics;
    @SerializedName("Chemistry")
    @Expose
    public Integer chemistry;
    @SerializedName("Biology")
    @Expose
    public Double biology;
}

class Analytic {
    @SerializedName("Application Ability")
    @Expose
    public Integer applicationAbility;
    @SerializedName("Conceptual Understanding")
    @Expose
    public Integer conceptualUnderstanding;
    @SerializedName("Problem Solving Skills")
    @Expose
    public Integer problemSolvingSkills;
    @SerializedName("Higher Order Thinking Skills (HOTS)")
    @Expose
    public Integer higherOrderThinkingSkillsHOTS;
}