package com.logicalclass.onlinecourseproject.socrecard.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details {
    @SerializedName("subjectWiseAnalytics")
    @Expose
    public SubjectWiseAnalytics subjectWiseAnalytics;
    @SerializedName("analyticsDetails")
    @Expose
    public AnalyticsDetails analyticsDetails;
}