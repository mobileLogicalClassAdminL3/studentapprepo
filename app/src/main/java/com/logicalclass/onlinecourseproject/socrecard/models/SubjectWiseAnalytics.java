package com.logicalclass.onlinecourseproject.socrecard.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubjectWiseAnalytics {
    @SerializedName("Maths")
    @Expose
    public Maths maths;
    @SerializedName("Physics")
    @Expose
    public Physics physics;
    @SerializedName("Chemistry")
    @Expose
    public Chemistry chemistry;
    @SerializedName("Biology")
    @Expose
    public Biology biology;
}

class Biology {
    @SerializedName("subject")
    @Expose
    public String subject;
    @SerializedName("topic")
    @Expose
    public Object topic;
    @SerializedName("chapter")
    @Expose
    public String chapter;
    @SerializedName("right")
    @Expose
    public Integer right;
    @SerializedName("wrong")
    @Expose
    public Integer wrong;
    @SerializedName("notAttempted")
    @Expose
    public Integer notAttempted;
    @SerializedName("timeTaken")
    @Expose
    public Integer timeTaken;
}

class Chemistry {
    @SerializedName("subject")
    @Expose
    public String subject;
    @SerializedName("topic")
    @Expose
    public Object topic;
    @SerializedName("chapter")
    @Expose
    public String chapter;
    @SerializedName("right")
    @Expose
    public Integer right;
    @SerializedName("wrong")
    @Expose
    public Integer wrong;
    @SerializedName("notAttempted")
    @Expose
    public Integer notAttempted;
    @SerializedName("timeTaken")
    @Expose
    public Integer timeTaken;
}

class Maths {
    @SerializedName("subject")
    @Expose
    public String subject;
    @SerializedName("topic")
    @Expose
    public Object topic;
    @SerializedName("chapter")
    @Expose
    public String chapter;
    @SerializedName("right")
    @Expose
    public Integer right;
    @SerializedName("wrong")
    @Expose
    public Integer wrong;
    @SerializedName("notAttempted")
    @Expose
    public Integer notAttempted;
    @SerializedName("timeTaken")
    @Expose
    public Integer timeTaken;
}

class Physics {
    @SerializedName("subject")
    @Expose
    public String subject;
    @SerializedName("topic")
    @Expose
    public Object topic;
    @SerializedName("chapter")
    @Expose
    public String chapter;
    @SerializedName("right")
    @Expose
    public Integer right;
    @SerializedName("wrong")
    @Expose
    public Integer wrong;
    @SerializedName("notAttempted")
    @Expose
    public Integer notAttempted;
    @SerializedName("timeTaken")
    @Expose
    public Integer timeTaken;
}