package com.logicalclass.onlinecourseproject.socrecard.view

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.logicalclass.onlinecourseproject.BuildConfig
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.databinding.FragmentQuestionAnalyticsWebviewBinding
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.utils.show

class FragmentQuestionAnalytics : Fragment() {
    lateinit var binding: FragmentQuestionAnalyticsWebviewBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(activity),
            R.layout.fragment_question_analytics_webview,
            container,
            false
        )
        showWebViewContent()
        return binding.root
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun showWebViewContent() {
        binding.webView.settings.javaScriptEnabled = true
        binding.webView.settings.loadWithOverviewMode = true
        binding.webView.settings.useWideViewPort = true
        binding.webView.settings.userAgentString =
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36";
        binding.webView.settings.javaScriptCanOpenWindowsAutomatically = true
        binding.webView.settings.builtInZoomControls = false
        binding.webView.settings.mediaPlaybackRequiresUserGesture = false

        activity?.let {
            binding.webView.webChromeClient = ChromeClient(it)
        }

        binding.webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                hideProgress()
            }

            override fun onReceivedError(
                view: WebView?,
                request: WebResourceRequest?,
                error: WebResourceError?
            ) {
                super.onReceivedError(view, request, error)
                if (isAdded)
                    hideProgress()
            }
        }

        if (isAdded) {
            var studentAuth: String? = ""
            var orgAuth: String? = ""
            activity?.applicationContext?.let {
                studentAuth =
                    SharedPrefsUtils.getStringPreference(it, SharedPrefsUtils.STUDENT_AUTH)
                orgAuth = SharedPrefsUtils.getStringPreference(it, SharedPrefsUtils.ORG_AUTH)
            }
            val examId = arguments?.getString("examId")
            binding.webView.loadUrl("${BuildConfig.DOMAIN_URL}ManageExternalUrl/questionAnalytics?orgAuth=${orgAuth}&examId=${examId}&studentAuth=${studentAuth}")
            showProgress()
        }
    }

    private fun hideProgress() {
        if (isAdded)
            binding.rlProgress.hide()
    }

    private fun showProgress() {
        if (isAdded)
            binding.rlProgress.show()
    }

    open class ChromeClient internal constructor(val context: FragmentActivity) :
        WebChromeClient() {
        private var mCustomView: View? = null
        private var mCustomViewCallback: CustomViewCallback? = null
        protected var mFullscreenContainer: FrameLayout? = null
        private var mOriginalOrientation = 0
        private var mOriginalSystemUiVisibility = 0
        override fun getDefaultVideoPoster(): Bitmap? {
            return if (mCustomView == null) {
                null
            } else BitmapFactory.decodeResource(context.resources, 2130837573)
        }

        override fun onHideCustomView() {
            (context.window.decorView as FrameLayout).removeView(mCustomView)
            mCustomView = null
            context.window.decorView.systemUiVisibility = mOriginalSystemUiVisibility
            context.requestedOrientation = mOriginalOrientation
            mCustomViewCallback?.onCustomViewHidden()
            mCustomViewCallback = null
        }

        override fun onShowCustomView(
            paramView: View?,
            paramCustomViewCallback: CustomViewCallback?
        ) {
            if (mCustomView != null) {
                onHideCustomView()
                return
            }
            mCustomView = paramView
            mOriginalSystemUiVisibility = context.window.decorView.systemUiVisibility
            mOriginalOrientation = context.requestedOrientation
            mCustomViewCallback = paramCustomViewCallback
            (context.window.decorView as FrameLayout).addView(
                mCustomView,
                FrameLayout.LayoutParams(-1, -1)
            )
            context.window.decorView.systemUiVisibility = 3846 or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        }
    }
}