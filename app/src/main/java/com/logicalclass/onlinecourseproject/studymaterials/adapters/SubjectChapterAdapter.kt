package com.logicalclass.onlinecourseproject.studymaterials.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.models.SubjectSection
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.utils.show
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup

class SubjectChapterAdapter(groups: List<SubjectSection?>?) :
    ExpandableRecyclerViewAdapter<SubjectViewHolder?, ChapterViewHolder?>(groups) {

    override fun onCreateGroupViewHolder(parent: ViewGroup, viewType: Int): SubjectViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_subject_section, parent, false)
        return SubjectViewHolder(view)
    }

    override fun onCreateChildViewHolder(parent: ViewGroup, viewType: Int): ChapterViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_chapter_layout, parent, false)
        return ChapterViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindChildViewHolder(
        holder: ChapterViewHolder?, flatPosition: Int, group:
        ExpandableGroup<*>?, childIndex: Int
    ) {
        val chapter = (group as SubjectSection).items[childIndex]

        holder?.setChapterName(chapter?.name ?: "")
        chapter?.basic?.let {
            holder?.txtBasic?.text = "Basic: $it"
            holder?.txtBasic?.show()
        } ?: kotlin.run {
            holder?.txtBasic?.hide()
        }

        chapter?.medium?.let {
            holder?.txtMedium?.text = "Medium: $it"
            holder?.txtMedium?.show()
        } ?: kotlin.run {
            holder?.txtMedium?.hide()
        }

        chapter?.tough?.let {
            holder?.txtTough?.text = "Tough: $it"
            holder?.txtTough?.show()
        } ?: kotlin.run {
            holder?.txtTough?.hide()
        }
    }

    override fun onBindGroupViewHolder(
        holder: SubjectViewHolder?, flatPosition: Int,
        group: ExpandableGroup<*>?
    ) {
        holder?.setGenreTitle(group!!)
    }
}