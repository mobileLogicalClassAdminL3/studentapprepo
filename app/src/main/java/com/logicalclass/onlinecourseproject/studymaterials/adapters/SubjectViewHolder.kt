package com.logicalclass.onlinecourseproject.studymaterials.adapters

import android.view.View
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.models.SubjectSection
import com.logicalclass.onlinecourseproject.utils.dpToPx
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder

class SubjectViewHolder(itemView: View) : GroupViewHolder(itemView) {
    private val subjectName = itemView.findViewById<TextView>(R.id.sectionHeader)
    private val imgArrow = itemView.findViewById<ImageView>(R.id.imgArrow)
    private val rlRootView = itemView.findViewById<View>(R.id.rlRootView)
    private val card = itemView.findViewById<CardView>(R.id.card)
    fun setGenreTitle(genre: ExpandableGroup<*>) {
        if (genre is SubjectSection) {
            subjectName.text = genre.getTitle()
        }
    }

    override fun expand() {
        animateExpand()
        rlRootView.background = ContextCompat.getDrawable(
            subjectName.context,
            R.drawable.subject_card_top_rounded_gradient
        )
        /*card.radius = dpToPx(0, card.context).toFloat()*/
    }

    override fun collapse() {
        animateCollapse()
        rlRootView.background = ContextCompat.getDrawable(
            subjectName.context,
            R.drawable.subject_card_gradient
        )
        /*card.radius = dpToPx(12, card.context).toFloat()*/
    }

    private fun animateExpand() {
        val rotate = RotateAnimation(
            360f,
            180f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 300
        rotate.fillAfter = true
        imgArrow.animation = rotate
    }

    private fun animateCollapse() {
        val rotate = RotateAnimation(
            180f,
            360f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 300
        rotate.fillAfter = true
        imgArrow.animation = rotate
    }
}