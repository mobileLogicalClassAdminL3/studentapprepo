package com.logicalclass.onlinecourseproject.studymaterials.view

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.onlineclasses.LiveClassWebViewActivity
import com.logicalclass.onlinecourseproject.onlineclasses.OnlineClassDocumentsListActivity
import com.logicalclass.onlinecourseproject.utils.showSnackMessage
import com.logicalclass.onlinecourseproject.utils.FileUtils
import kotlinx.android.synthetic.main.activity_pdf_view.*
import java.io.File

class PdfViewActivity : AppCompatActivity() {

    companion object {
        private const val PDF_SELECTION_CODE = 99

        const val VIEW_TYPE = "view_type"
        const val PDF_URL = "pdf_url"
        const val TOPIC_NAME = "topic_name"
        const val PDF_FILE_NAME = "pdf_file_name"
        const val TYPE_INTERNET = "internet"
        const val TYPE_ASSETS = "assets"
        const val TYPE_STORAGE = "storage"

        const val ACTIVITY_TYPE_ONLINE_CLASS = "ONLINE_CLASS"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pdf_view)
        setToolbarData()
        PRDownloader.initialize(applicationContext)
        checkPdfAction(intent)
    }

    private fun setToolbarData() {
        supportActionBar?.title = intent.getStringExtra(TOPIC_NAME) ?: ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun showPdfFromAssets(pdfName: String) {
        pdfView.fromAsset(pdfName)
                .password(null)
                .defaultPage(0)
                .onPageError { page, _ ->
                    pdfView.showSnackMessage("Error in downloading file : $page")
                }
                .load()
    }

    private fun selectPdfFromStorage() {
        pdfView.showSnackMessage("selectPDF")
        val browseStorage = Intent(Intent.ACTION_GET_CONTENT)
        browseStorage.type = "application/pdf"
        browseStorage.addCategory(Intent.CATEGORY_OPENABLE)
        startActivityForResult(
                Intent.createChooser(browseStorage, "Select PDF"), PDF_SELECTION_CODE
        )
    }

    private fun showPdfFromUri(uri: Uri?) {
        pdfView.fromUri(uri)
                .defaultPage(0)
                .spacing(10)
                .load()
    }

    private fun showPdfFromFile(file: File) {
        pdfView.fromFile(file)
                .password(null)
                .defaultPage(0)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .enableDoubletap(true)
                .onPageError { page, _ ->
                    pdfView.showSnackMessage("Error at page: $page")
                }
                .load()
    }

    private fun downloadPdfFromInternet(url: String?, dirPath: String, fileName: String) {
        PRDownloader.download(
                url,
                dirPath,
                fileName
        ).build()
                .start(object : OnDownloadListener {
                    override fun onDownloadComplete() {
                        val downloadedFile = File(dirPath, fileName)
                        progressBar.visibility = View.GONE
                        showPdfFromFile(downloadedFile)
                    }

                    override fun onError(error: Error?) {
                        pdfView.showSnackMessage("Error in downloading file : $error")
                    }
                })
    }

    private fun checkPdfAction(intent: Intent?) {
        intent?.let {
            when (intent.getStringExtra(VIEW_TYPE)) {
                TYPE_ASSETS -> {
                    showPdfFromAssets(FileUtils.getPdfNameFromAssets())
                }
                TYPE_STORAGE -> {
                    if (intent.getStringExtra(ACTIVITY_TYPE_ONLINE_CLASS) == LiveClassWebViewActivity::class.simpleName ||
                            intent.getStringExtra(ACTIVITY_TYPE_ONLINE_CLASS) == OnlineClassDocumentsListActivity::class.simpleName) {
                        val path = FileUtils.getOnlineClassesDocumentsPath(this)
                        val fileName = it.getStringExtra(PDF_FILE_NAME) ?: ""
                        showPdfFromFile(File(path, fileName))
                    } else {
                        selectPdfFromStorage()
                    }
                }
                TYPE_INTERNET -> {
                    progressBar.visibility = View.VISIBLE
                    val fileName = intent.getStringExtra(PDF_FILE_NAME) ?: "my_pdf_file.pdf"
                    downloadPdfFromInternet(
                            intent.getStringExtra(PDF_URL),
                            FileUtils.getRootDirPath(this),
                            fileName
                    )
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PDF_SELECTION_CODE && resultCode == Activity.RESULT_OK && data != null) {
            val selectedPdfFromStorage = data.data
            showPdfFromUri(selectedPdfFromStorage)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
