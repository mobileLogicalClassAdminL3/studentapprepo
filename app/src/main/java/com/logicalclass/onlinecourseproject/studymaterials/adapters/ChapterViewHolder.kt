package com.logicalclass.onlinecourseproject.studymaterials.adapters

import android.view.View
import android.widget.TextView
import com.logicalclass.onlinecourseproject.R
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder

class ChapterViewHolder(itemView: View) : ChildViewHolder(itemView) {
    private val childTextView = itemView.findViewById<TextView>(R.id.txtChapterName)
    val txtBasic = itemView.findViewById<TextView>(R.id.txtBasic)
    val txtMedium = itemView.findViewById<TextView>(R.id.txtMedium)
    val txtTough = itemView.findViewById<TextView>(R.id.txtTough)

    fun setChapterName(name: String?) {
        childTextView.text = name
    }
}