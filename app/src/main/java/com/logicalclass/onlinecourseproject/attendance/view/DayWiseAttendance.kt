package com.logicalclass.onlinecourseproject.attendance.view

import android.annotation.SuppressLint
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.children
import com.kizitonwose.calendarview.model.CalendarDay
import com.kizitonwose.calendarview.model.DayOwner
import com.kizitonwose.calendarview.ui.DayBinder
import com.kizitonwose.calendarview.ui.ViewContainer
import com.kizitonwose.calendarview.utils.yearMonth
import com.logicalclass.onlinecourseproject.*
import com.logicalclass.onlinecourseproject.attendance.model.AttendanceModel
import com.logicalclass.onlinecourseproject.repository.AppService.Factory.create
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.androidThread
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.ioThread
import com.logicalclass.onlinecourseproject.utils.*
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_attendance.*
import kotlinx.android.synthetic.main.calendar_day_view.view.*
import org.threeten.bp.LocalDate
import org.threeten.bp.YearMonth
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.TextStyle
import java.util.*


class DayWiseAttendance : AppCompatActivity() {
    var auth: String? = null
    var StudentAuth: String? = null
    var orgAuth: String? = null
    var tokenid2: String? = null
    var savedInstanceState: Bundle? = null
    private var disposable: CompositeDisposable? = null

    private var holidayDates = mutableSetOf<LocalDate>()
    private var presentDates = mutableSetOf<LocalDate>()
    private var absentDates = mutableSetOf<LocalDate>()
    private val today = LocalDate.now()
    private val monthTitleFormatter = DateTimeFormatter.ofPattern("MMMM")
    private var dateSelectionFor: String? = null
    private var startDateSelected: String? = null
    private var endDateSelected: String? = null
    private var startDateCalendar: Calendar = Calendar.getInstance()
    private lateinit var startYearMonth: YearMonth
    private lateinit var endYearMonth: YearMonth

    private var attendanceModel: AttendanceModel? = null

    companion object {
        const val START_DATE: String = "StartDate"
        const val END_DATE: String = "EndDate"
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_attendance)
        setToolbarData()
        setClickListeners()
        disposable = CompositeDisposable()
        tokenid2 = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.TOKEN_ID_2)
        StudentAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.STUDENT_AUTH)
        orgAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.ORG_AUTH)
        initStartEndYearMonthData()
    }

    private fun setToolbarData() {
        supportActionBar?.title = getString(R.string.attendance)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showProgress() {
        rlProgress.show()
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun initStartEndYearMonthData() {
        startYearMonth = YearMonth.of(YearMonth.now().year, YearMonth.now().month)
        endYearMonth = YearMonth.of(YearMonth.now().year - 1, YearMonth.now().month)
        startDateSelected =
            "${today.dayOfMonth}/${startYearMonth.monthValue}/${startYearMonth.year}"
        endDateSelected = "1/${endYearMonth.monthValue}/${endYearMonth.year}"
        getDetailedAttendanceData(
            getServerFormatDate(endDateSelected),
            getServerFormatDate(startDateSelected)
        )
    }

    @SuppressLint("SetTextI18n")
    private fun setClickListeners() {
        btnStartDate.setOnClickListener {
            dateSelectionFor = START_DATE
            val datePickerFragment = DatePickerFragment()
            datePickerFragment.setMaxDate(Calendar.getInstance().timeInMillis)
            datePickerFragment.show(supportFragmentManager, getString(R.string.datepicker))
        }

        btnEndDate.setOnClickListener {
            dateSelectionFor = END_DATE
            val datePickerFragment = DatePickerFragment()
            val minCal = Calendar.getInstance()
            minCal.set(Calendar.YEAR, startDateCalendar.get(Calendar.YEAR) - 1)
            minCal.set(Calendar.MONTH, startDateCalendar.get(Calendar.MONTH))
            minCal.set(Calendar.DAY_OF_MONTH, startDateCalendar.get(Calendar.DAY_OF_MONTH))
            datePickerFragment.setMinDate(minCal.timeInMillis)
            datePickerFragment.setMaxDate(startDateCalendar.timeInMillis)
            datePickerFragment.show(supportFragmentManager, getString(R.string.datepicker))
        }

        btnGetAttendance.setOnClickListener {
            getDetailedAttendanceData(
                getServerFormatDate(endDateSelected),
                getServerFormatDate(startDateSelected)
            )
        }

        btnDetailedView.setOnClickListener {
            if (groupDetailedView.visibility == View.VISIBLE) {
                btnDetailedView.setText(R.string.detailed_view)
                groupDetailedView.hide()
                initStartEndYearMonthData()
            } else {
                btnDetailedView.setText(R.string.hide_detailed_view)
                btnStartDate.text = getString(R.string.start_date)
                btnEndDate.text = getString(R.string.end_date)
                btnEndDate.isEnabled = false
                btnEndDate.alpha = 0.4f
                btnGetAttendance.isEnabled = false
                btnGetAttendance.alpha = 0.4f
                groupDetailedView.show()
            }
        }

        calendarView.dayBinder = object : DayBinder<DayViewContainer> {
            override fun create(view: View) = DayViewContainer(view)
            override fun bind(container: DayViewContainer, day: CalendarDay) {
                container.day = day
                val textView = container.textView
                textView.text = day.date.dayOfMonth.toString()
                if (day.owner == DayOwner.THIS_MONTH) {
                    when {
                        presentDates.contains(day.date) -> {
                            textView.setBackgroundResource(R.drawable.calendar_present_day_bg)
                        }

                        absentDates.contains(day.date) -> {
                            textView.setBackgroundResource(R.drawable.calendar_absent_day_bg)
                        }

                        holidayDates.contains(day.date) -> {
                            textView.setBackgroundResource(R.drawable.calendar_holiday_day_bg)
                        }
                        else -> {
                            textView.setTextColorRes(R.color.black)
                            textView.background = null
                        }
                    }
                    if (today == day.date) {
                        val gd: GradientDrawable? = (textView.background
                            ?: GradientDrawable()) as GradientDrawable
                        gd?.setStroke(5, getColor(R.color.black))
                        gd?.shape = GradientDrawable.OVAL
                        textView.background = gd
                    } else {
                        val gd: GradientDrawable? = (textView.background
                            ?: GradientDrawable()) as GradientDrawable
                        gd?.setStroke(0, getColor(android.R.color.transparent))
                        textView.background = gd
                    }
                } else {
                    textView.setTextColorRes(R.color.black)
                    textView.background = null
                }
            }
        }

        calendarView.monthScrollListener = { it ->
            if (calendarView.maxRowCount == 6) {
                exOneYearText.text = it.yearMonth.year.toString()
                exOneMonthText.text = monthTitleFormatter.format(it.yearMonth)
            } else {
                // In week mode, we show the header a bit differently.
                // We show indices with dates from different months since
                // dates overflow and cells in one index can belong to different
                // months/years.
                val firstDate = it.weekDays.first().first().date
                val lastDate = it.weekDays.last().last().date
                if (firstDate.yearMonth == lastDate.yearMonth) {
                    exOneYearText.text = firstDate.yearMonth.year.toString()
                    exOneMonthText.text = monthTitleFormatter.format(firstDate)
                } else {
                    exOneMonthText.text =
                        "${monthTitleFormatter.format(firstDate)} - ${
                            monthTitleFormatter.format(
                                lastDate
                            )
                        }"
                    if (firstDate.year == lastDate.year) {
                        exOneYearText.text = firstDate.yearMonth.year.toString()
                    } else {
                        exOneYearText.text =
                            "${firstDate.yearMonth.year} - ${lastDate.yearMonth.year}"
                    }
                }
            }
            setAttendanceInfoDate(it.yearMonth)
        }
    }

    fun processDatePickerResult(year: Int, month: Int, day: Int) {
        val monthSelected = month + 1
        dateSelectionFor?.let {
            if (it == START_DATE) {
                startDateSelected = "$day/$monthSelected/$year"
                setStartDateCalendar(year, month, day)
                startYearMonth = YearMonth.of(year, monthSelected)
                btnStartDate.text = startDateSelected
                btnEndDate.text = null
                btnEndDate.isEnabled = true
                btnEndDate.alpha = 1f
            } else {
                endDateSelected = "$day/$monthSelected/$year"
                endYearMonth = YearMonth.of(year, monthSelected)
                btnEndDate.text = endDateSelected
                btnGetAttendance.isEnabled = true
                btnGetAttendance.alpha = 1f
            }
        }
        dateSelectionFor = null
    }

    /*
    * input date format dd/mm/yyyy
    * output date format yyyy/mm/dd
    * */
    private fun getServerFormatDate(date: String?): String {
        date?.let {
            val parts = it.split("/")
            return "${parts[2]}-${parts[1]}-${parts[0]}"
        }
        return "";
    }

    private fun setStartDateCalendar(year: Int, monthSelected: Int, day: Int) {
        startDateCalendar.set(Calendar.YEAR, year)
        startDateCalendar.set(Calendar.MONTH, monthSelected)
        startDateCalendar.set(Calendar.DAY_OF_MONTH, day)
        startDateCalendar.set(Calendar.HOUR_OF_DAY, 0)
        startDateCalendar.set(Calendar.MINUTE, 0)
        startDateCalendar.set(Calendar.SECOND, 0)
    }

    private fun setUpCalendarView(startYearMonth: YearMonth, endYearMonth: YearMonth) {
        val daysOfWeek = daysOfWeekFromLocale()
        legendLayout.children.forEachIndexed { index, view ->
            (view as TextView).apply {
                text = daysOfWeek[index].getDisplayName(TextStyle.SHORT, Locale.ENGLISH)
                    .toUpperCase(Locale.ENGLISH)
                setTextColorRes(R.color.black)
            }
        }
        calendarView.setup(
            startYearMonth,
            endYearMonth,
            daysOfWeek.first()
        )
        calendarView.scrollToMonth(
            if (groupDetailedView.visibility == View.VISIBLE)
                startYearMonth
            else YearMonth.now()
        )
    }

    private fun setAttendanceInfoDate(yearMonth: YearMonth = YearMonth.now()) {
        if (attendanceInfo.visibility == View.GONE) {
            attendanceInfo.show()
        }
        if (groupDetailedView.visibility == View.VISIBLE) {
            attendanceModel?.let {
                txtPresentDays.text = it.data.presentDatesList.size.toString()
                txtAbsentDays.text = it.data.absentDatesList.size.toString()
                txtHolidays.text = it.data.holidayDatesList.size.toString()
                txtWorkingDays.text = attendanceModel?.data?.allWorkingDays.toString()
                txtPercentage.text = "${getPercentageValue()}%"
            }
        } else {
            attendanceModel?.let { attendanceModel ->
                var workingDays = 0
                var presentDays = 0
                if (attendanceModel.data.presentDaysMap.containsKey(yearMonth)) {
                    presentDays = attendanceModel.data.presentDaysMap.getValue(yearMonth)
                    txtPresentDays.text = presentDays.toString()
                    workingDays += presentDays
                } else {
                    txtPresentDays.text = "0"
                }
                if (attendanceModel.data.absentDaysMap.containsKey(yearMonth)) {
                    val absentDays = attendanceModel.data.absentDaysMap.getValue(yearMonth)
                    txtAbsentDays.text = absentDays.toString()
                    workingDays += absentDays
                } else {
                    txtAbsentDays.text = "0"
                }
                if (attendanceModel.data.holidaysMap.containsKey(yearMonth)) {
                    txtHolidays.text =
                        attendanceModel.data.holidaysMap.getValue(yearMonth).toString()
                } else {
                    txtHolidays.text = "0"
                }
                txtWorkingDays.text = workingDays.toString()
                txtPercentage.text = ((presentDays / workingDays) * 100).toString() + "%"
            }
        }
    }

    private fun getPercentageValue(): Int {
        attendanceModel?.let {
            return (((presentDates.size +
                    holidayDates.size) - holidayDates.size) / it.data.allWorkingDays) * 100
        }
        return 0
    }

    private fun getDetailedAttendanceData(startDate: String?, endDate: String?) {
        disposable?.add(create().attendanceDateRangDetails(
            tokenid2,
            StudentAuth,
            orgAuth,
            startDate,
            endDate
        ).subscribeOn(ioThread())
            .doOnSubscribe {
                showProgress()
            }
            .observeOn(androidThread())
            .subscribe({ response: AttendanceModel ->
                handleAttendanceResponse(response)
            }, {
                hideProgress()
                rootView.showApiParsingErrorSnack()
            })
        )
    }

    private fun handleAttendanceResponse(attendanceModel: AttendanceModel) {
        this.attendanceModel = attendanceModel
        if (attendanceModel.status == 200) {
            attendanceModel.data.initData()
            presentDates = attendanceModel.data.presentDatesList
            holidayDates = attendanceModel.data.holidayDatesList
            absentDates = attendanceModel.data.absentDatesList
            setUpCalendarView(endYearMonth, startYearMonth)
            btnDetailedView.show()
        } else {
            rootView.showApiErrorSnack()
        }
        hideProgress()
    }

    class DayViewContainer(view: View) : ViewContainer(view) {
        lateinit var day: CalendarDay
        val textView = view.exOneDayText

        init {
            view.setOnClickListener {
                if (day.owner == DayOwner.THIS_MONTH) {
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.clear()
    }
}