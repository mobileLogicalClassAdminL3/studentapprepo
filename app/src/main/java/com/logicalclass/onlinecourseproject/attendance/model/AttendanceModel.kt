package com.logicalclass.onlinecourseproject.attendance.model

import android.util.Log
import com.google.gson.annotations.SerializedName
import com.logicalclass.onlinecourseproject.repository.Auth
import org.threeten.bp.LocalDate
import org.threeten.bp.YearMonth

data class AttendanceModel(
        val authData: Auth,
        @SerializedName("data")
        val data: Data,
        val msg: String,
        val status: Int,
        val token: String
)

data class Data(
        val absentDates: List<String>,
        val absentDays: Int,
        val allWorkingDays: Int,
        val holidaysDates: List<String>,
        val holidaysDays: Int,
        val presentDates: List<String>,
        val presentDays: Int,
        var presentDaysMap: HashMap<YearMonth, Int>,
        var absentDaysMap: HashMap<YearMonth, Int>,
        var holidaysMap: HashMap<YearMonth, Int>,
        var presentDatesList: MutableSet<LocalDate> = mutableSetOf(),
        var holidayDatesList: MutableSet<LocalDate> = mutableSetOf(),
        var absentDatesList: MutableSet<LocalDate> = mutableSetOf()
) {
    private fun initPresentDatesList() {
        try {
            presentDaysMap = HashMap()
            presentDatesList = mutableSetOf()
            presentDates.forEach {
                val count: Int
                val parts = it.split("-")
                val yearMonth = YearMonth.of(Integer.parseInt(parts[0]),
                        Integer.parseInt(parts[1]))
                if (presentDaysMap.containsKey(yearMonth)) {
                    presentDaysMap[yearMonth] = presentDaysMap[yearMonth] as Int + 1
                } else {
                    count = 1
                    presentDaysMap[yearMonth] = count
                }
                presentDatesList.add(LocalDate.of(Integer.parseInt(parts[0]),
                        Integer.parseInt(parts[1]),
                        Integer.parseInt(parts[2])))
            }
        } catch (e: Exception) {
            Log.e("Error", "getPresentDates error")
        }
    }

    private fun initHolidaysDatesList() {
        try {
            holidaysMap = HashMap()
            holidayDatesList = mutableSetOf()
            holidaysDates.forEach {
                val count: Int
                val parts = it.split("-")
                val yearMonth = YearMonth.of(Integer.parseInt(parts[0]),
                        Integer.parseInt(parts[1]))
                if (holidaysMap.containsKey(yearMonth)) {
                    holidaysMap[yearMonth] = holidaysMap[yearMonth] as Int + 1
                } else {
                    count = 1
                    holidaysMap[yearMonth] = count
                }
                holidayDatesList.add(LocalDate.of(Integer.parseInt(parts[0]),
                        Integer.parseInt(parts[1]),
                        Integer.parseInt(parts[2])))
            }
        } catch (e: Exception) {
            Log.e("Error", "getPresentDates error")
        }
    }

    private fun initAbsentDatesList() {
        try {
            absentDaysMap = HashMap()
            absentDatesList = mutableSetOf()
            absentDates.forEach {
                val count: Int
                val parts = it.split("-")
                val yearMonth = YearMonth.of(Integer.parseInt(parts[0]),
                        Integer.parseInt(parts[1]))
                if (absentDaysMap.containsKey(yearMonth)) {
                    absentDaysMap[yearMonth] = absentDaysMap[yearMonth] as Int + 1
                } else {
                    count = 1
                    absentDaysMap[yearMonth] = count
                }
                absentDatesList.add(LocalDate.of(Integer.parseInt(parts[0]),
                        Integer.parseInt(parts[1]),
                        Integer.parseInt(parts[2])))
            }
        } catch (e: Exception) {
            Log.e("Error", "getPresentDates error")
        }
    }

    fun initData() {
        initPresentDatesList()
        initAbsentDatesList()
        initHolidaysDatesList()
    }
}
