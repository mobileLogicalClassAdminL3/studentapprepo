package com.logicalclass.onlinecourseproject.home

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.databinding.ActivitySubModuleBinding
import com.logicalclass.onlinecourseproject.home.MainActivity.Companion.KEY_SUB_MODULE
import com.logicalclass.onlinecourseproject.home.adapters.SubModuleListAdapter
import com.logicalclass.onlinecourseproject.utils.showSnackMessage

class SubModuleActivity : AppCompatActivity() {
    private var subModuleData: HomeSubModuleData? = null

    private lateinit var binding: ActivitySubModuleBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sub_module)
        getIntentData()
        setToolbarData()
        setListAdapter()
    }

    private fun getIntentData() {
        intent?.let {
            subModuleData = intent.getParcelableExtra(KEY_SUB_MODULE)
        }
    }

    private fun setListAdapter() {
        binding.rvList.layoutManager = GridLayoutManager(this, 2)
        binding.rvList.adapter = SubModuleListAdapter(
            this,
            subModuleData?.subModuleList ?: arrayListOf(),
            ::onModuleClick
        )
    }

    private fun setToolbarData() {
        supportActionBar?.title = subModuleData?.title
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun onModuleClick(moduleData: HomeSubModuleData) {
        try {
            val intent = Intent(this, Class.forName(moduleData.destinationPath))
            intent.putExtra(KEY_MAIN_MODULE_TYPE, subModuleData?.tag)
            intent.putExtra(KEY_MAIN_MODULE_NAME, subModuleData?.title)
            intent.putExtra(KEY_SUB_MODULE_TYPE, moduleData.tag)
            intent.putExtra(KEY_SUB_MODULE_NAME, moduleData.title)
            startActivity(intent)
        } catch (ignored: ClassNotFoundException) {
            binding.root.showSnackMessage(getString(R.string.something_went_wrong))
        }
    }

    companion object {
        const val KEY_MAIN_MODULE_TYPE = "KEY_MAIN_MODULE_TYPE"
        const val KEY_MAIN_MODULE_NAME = "KEY_MAIN_MODULE_NAME"
        const val KEY_SUB_MODULE_TYPE = "KEY_SUB_MODULE_TYPE"
        const val KEY_SUB_MODULE_NAME = "KEY_SUB_MODULE_NAME"
    }
}