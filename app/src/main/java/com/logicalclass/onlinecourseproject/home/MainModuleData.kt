package com.logicalclass.onlinecourseproject.home

import android.content.Context
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.logicalclass.onlinecourseproject.repository.Auth
import kotlinx.android.parcel.Parcelize
import java.io.IOException

/*Main Modules*/
const val PROFILE = "PROFILE"
const val ATTENDANCE = "ATTENDANCE"
const val EXAM_RESULTS = "EXAM_RESULTS"
const val Q_SCAN = "Q_SCAN"
const val AI_LIBRARY = "AI_LIBRARY"
const val ORGANISATION_LIBRARY = "ORGANISATION_LIBRARY"
const val ONLINE_CLASSES = "ONLINE_CLASSES"
const val DIGITAL_DIARY = "DIGITAL_DIARY"
const val PUBLIC_ANNOUNCEMENTS = "PUBLIC_ANNOUNCEMENTS"
const val STUDY_MATERIALS = "STUDY_MATERIALS"
const val ONLINE_COURSES = "ONLINE_COURSES"
const val MOCK_TEST = "MOCK_TEST"
const val VIDEO_TUTORIALS = "VIDEO_TUTORIALS"
const val ERROR_ANALYSIS = "ERROR_ANALYSIS"
const val MOST_STUDENT_ERRORS = "MOST_STUDENT_ERRORS"
const val TOPPER_ERRORS = "TOPPER_ERRORS"

/*Sub Modules*/
const val TIME_TAKEN_QUESTIONS = "TIME_TAKEN_QUESTIONS"
const val PRACTICE_OWN_MISTAKE_QUESTIONS = "PRACTICE_OWN_MISTAKE_QUESTIONS"
const val MISTAKE_OF_MOST_STUDENTS = "MISTAKE_OF_MOST_STUDENTS"
const val PRACTICE_TIME_WASTED_QUESTIONS_OF_MOST_STUDENTS =
    "PRACTICE_TIME_WASTED_QUESTIONS_OF_MOST_STUDENTS"
const val WRONG_OF_TOPPER = "WRONG_OF_TOPPER"
const val TIME_WASTED_QUESTION_OF_TOPPER = "TIME_WASTED_QUESTION_OF_TOPPER"

data class MainModuleData(
    val moduleName: String = "",
    val moduleIcon: Int = 0,
    val message: String = "",
    val tag: String = ""
)

data class HomeModuleData(
    val moduleName: String = "",
    val tag: String = "",
    val subModuleList: ArrayList<HomeSubModuleData> = ArrayList()
)

@Parcelize
data class HomeSubModuleData(
    val title: String = "",
    val iconName: String = "",
    val tag: String = "",
    val destinationPath: String = "",
    val subModuleList: ArrayList<HomeSubModuleData> = ArrayList()
) : Parcelable

data class TipResponse(
    val authData: Auth,
    val data: ArrayList<TipData> = arrayListOf(),
    val msg: String,
    val status: Int,
    val token: String
)

data class TipData(
    @SerializedName("class")
    val className: String = "",
    val subject: String = "",
    val chapter: String = "",
    val topic: String = "",
    val title: String = "",
    val tip: String = "",
    val id: String = "",
    val dateTime: String = "",
    val user: String = "",
    val url: String = "",
    val notificationId: String = ""
)

fun getJsonDataFromAsset(context: Context, fileName: String): String? {
    val jsonString: String
    try {
        jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
    } catch (ioException: IOException) {
        ioException.printStackTrace()
        return null
    }
    return jsonString
}