package com.logicalclass.onlinecourseproject.home

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.animation.Animation
import android.view.animation.AnimationUtils.loadAnimation
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager2.widget.MarginPageTransformer
import com.google.android.material.snackbar.Snackbar
import com.logicalclass.onlinecourseproject.BuildConfig
import com.logicalclass.onlinecourseproject.LogicalClassApp
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.databinding.ActivityMainBinding
import com.logicalclass.onlinecourseproject.digitallibrary.view.MainWebViewActivity
import com.logicalclass.onlinecourseproject.digitallibrary.view.MainWebViewActivity.Companion.KEY_WEB_VIEW_TITLE
import com.logicalclass.onlinecourseproject.digitallibrary.view.MainWebViewActivity.Companion.KEY_WEB_VIEW_URL
import com.logicalclass.onlinecourseproject.home.adapters.HomeMainModuleAdapter
import com.logicalclass.onlinecourseproject.home.adapters.TipsPagerAdapter
import com.logicalclass.onlinecourseproject.myanalytics.view.MyAnalyticsWebViewActivity
import com.logicalclass.onlinecourseproject.profile.view.ProfileActivity
import com.logicalclass.onlinecourseproject.repository.RxUtils
import com.logicalclass.onlinecourseproject.utils.*
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils.REFRESH_TIPS
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils.STUDENT_NAME
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), ForceUpdateChecker.OnUpdateNeededListener {

    companion object {
        const val KEY_SUB_MODULE = "KEY_SUB_MODULE"
    }

    private var animBlink: Animation? = null
    private val viewModel by lazy {
        getViewModel(this, HomeViewModel(application as LogicalClassApp))
    }
    private lateinit var binding: ActivityMainBinding
    private val disposable = CompositeDisposable()

    private lateinit var requestPermissionLauncher: ActivityResultLauncher<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        setToolbarData()
        setViewModelObservers()
        animBlink = loadAnimation(applicationContext, R.anim.blink)
        binding.fabMyAnalytics.setOnClickListener {
            val intent = Intent(this, MyAnalyticsWebViewActivity::class.java)
            startActivity(intent)
        }

        if (intent?.getStringExtra("coming_from") == "SplashScreen") checkPlayStoreUpdate()

        if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.POST_NOTIFICATIONS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            checkNotificationPermission()
        }
    }

    private fun checkNotificationPermission() {
        requestPermissionLauncher =
            registerForActivityResult(ActivityResultContracts.RequestPermission()) {
                if (!it) {
                    Log.e("MainActivity", "User not accepted the permission!")
                }
            }

        when {
            shouldShowRequestPermissionRationale(Manifest.permission.POST_NOTIFICATIONS) -> {
                Snackbar.make(
                    binding.root,
                    "The user denied the notifications. Please goto settings and allow the notifications permission.",
                    Snackbar.LENGTH_LONG
                ).setAction("Settings") {
                    val intent = Intent(
                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:$packageName")
                    )
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                }.show()
            }

            else -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
                }
            }
        }
    }

    private fun setToolbarData() {
        supportActionBar?.title =
            SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.ORG_NAME)
    }

    private fun setViewModelObservers() {/*viewModel.showProgress.observe(this, { show ->
            binding.rlProgress.showOrHide(show)
        })*/

        viewModel.fetchHomeModuleData().observe(this) { data ->
            when (data) {
                is HomeUiModel.HomeModuleSuccess -> {
                    viewModel.mainModuleList = data.moduleList

                    binding.rvMainModule.apply {
                        layoutManager = LinearLayoutManager(context)
                        adapter =
                            HomeMainModuleAdapter(viewModel.mainModuleList, ::itemClickCallback)
                    }
                }

                else -> Unit
            }
        }

        if (SharedPrefsUtils.getStringPreference(this, STUDENT_NAME).isNullOrEmpty()) {
            viewModel.fetchProfileInfo().observe(this) { data ->
                data?.let {
                    when (it) {
                        is ProfileUiModel.ProfileSuccess -> {
                            SharedPrefsUtils.setStringPreference(
                                this@MainActivity, STUDENT_NAME, it.response.userProfile?.name
                            )
                            fetchTipsData()
                        }

                        else -> Unit
                    }
                }
            }
        } else {
            fetchTipsData()
        }
    }

    private fun fetchTipsData() {
        viewModel.fetchTipsNotifications().observe(this) { data ->
            when (data) {
                is TipsUiModel.TipsSuccess -> {
                    viewModel.tipsList = data.response.data
                    if (viewModel.tipsList.isNullOrEmpty()) {
                        binding.tipsViewPager.hide()
                    } else {
                        setTipsPagerAdapter()
                        setAutoScrollObserver()
                    }
                    SharedPrefsUtils.setBooleanPreference(this, REFRESH_TIPS, false)
                }

                else -> {
                    binding.tipsViewPager.hide()
                }
            }
        }
    }

    private fun setTipsPagerAdapter() {
        binding.tipsViewPager.apply {
            clipToPadding = false   // allow full width shown with padding
            clipChildren = false    // allow left/right item is not clipped
            offscreenPageLimit = 2  // make sure left/right item is rendered

            // increase this offset to show more of left/right
            val offsetPx = 50.dpToPx(resources.displayMetrics)
            setPadding(offsetPx, 0, offsetPx, 0)

            // increase this offset to increase distance between 2 items
            val pageMarginPx = 20.dpToPx(resources.displayMetrics)
            val marginTransformer = MarginPageTransformer(pageMarginPx)
            setPageTransformer(marginTransformer)

            adapter = TipsPagerAdapter(
                SharedPrefsUtils.getStringPreference(
                    this@MainActivity, STUDENT_NAME
                ), viewModel.tipsList, ::itemTipClicked, ::onTipDismissClicked
            )
            show()
        }
    }

    private fun itemTipClicked(tipData: TipData) {
        val finalURL = BuildConfig.HTTP_KEY + tipData.url
        val intent = Intent(this, MainWebViewActivity::class.java)
        intent.putExtra(KEY_WEB_VIEW_URL, finalURL)
        intent.putExtra(KEY_WEB_VIEW_TITLE, tipData.title)
        startActivity(intent)
    }

    private fun onTipDismissClicked(position: Int, tipData: TipData) {
        showConfirmationAlertMessageWithAction(message = getString(R.string.are_sure_want_to_delete_this_tip)) {
            requestForDeleteTip(position, tipData)
        }
    }

    private fun requestForDeleteTip(position: Int, tipData: TipData) {
        viewModel.discardTipNotification(tipData.notificationId).observe(this) { data ->
            when (data) {
                is DiscardTipUiModel.DiscardTipSuccess -> {
                    binding.tipsViewPager.adapter?.apply {
                        (this as? TipsPagerAdapter)?.removeTip(position)
                        if (itemCount == 0) {
                            binding.tipsViewPager.hide()
                        }
                        binding.root.showSnackMessage("Tip successfully deleted.")
                    }
                }

                else -> {
                    binding.root.showSnackMessage(getString(R.string.something_went_wrong))
                }
            }
        }
    }

    private fun itemClickCallback(homeSubModuleData: HomeSubModuleData) {
        try {
            val intent = Intent(this, Class.forName(homeSubModuleData.destinationPath))
            intent.putExtra(KEY_SUB_MODULE, homeSubModuleData)
            startActivity(intent)
        } catch (ignored: ClassNotFoundException) {
            binding.root.showSnackMessage(getString(R.string.something_went_wrong))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.right_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean { // Handle item selection
        if (item.itemId == R.id.action_profile) {
            navigateToProfileScreen()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun navigateToProfileScreen() {
        val intent = Intent(this, ProfileActivity::class.java)
        startActivity(intent)
    }

    private fun checkPlayStoreUpdate() {
        if (!BuildConfig.DEBUG) {
            ForceUpdateChecker.with(this).onUpdateNeeded(this).check()
        }
    }

    override fun onUpdateNeeded(updateUrl: String?, mandatoryUpdate: Boolean?) {
        val builder = AlertDialog.Builder(this)
        if (mandatoryUpdate == true) {
            builder.setCancelable(false)
        } else {
            builder.setCancelable(true)
            builder.setNegativeButton("No, Thanks") { dialog, _ ->
                dialog.dismiss()
            }
        }
        builder.setTitle("New version available")
        builder.setPositiveButton("Update") { dialog, _ ->
            dialog.dismiss()
            redirectStore(updateUrl)
        }
        builder.setMessage(getString(R.string.update_available_description_dialog)).show()
    }

    private fun redirectStore(updateUrl: String?) {
        val intent = Intent(Intent.ACTION_VIEW).setData(Uri.parse(updateUrl))
        startActivity(intent)
    }

    private fun setAutoScrollObserver() {
        if (viewModel.tipsList.size == 1) return
        disposable.clear()
        disposable.add(Observable.interval(3000, 3000, TimeUnit.MILLISECONDS)
            .subscribeOn(RxUtils.ioThread()).observeOn(RxUtils.androidThread()).subscribe {
                binding.tipsViewPager.adapter?.let {
                    if (binding.tipsViewPager.currentItem == it.itemCount - 1) {
                        binding.tipsViewPager.currentItem = 0
                    } else {
                        binding.tipsViewPager.currentItem += 1
                    }
                }
            })
    }

    private fun setMyAnalyticsBlinkAnimation() {
        disposable.add(Observable.interval(0, 5, TimeUnit.SECONDS).subscribeOn(RxUtils.ioThread())
            .observeOn(RxUtils.androidThread()).subscribe {
                animBlink?.let {
                    binding.fabMyAnalytics.startAnimation(animBlink)
                }
            })
    }

    override fun onResume() {
        super.onResume()
        if (SharedPrefsUtils.getBooleanPreference(this, REFRESH_TIPS, false)) {
            fetchTipsData()
        } else if (!viewModel.tipsList.isNullOrEmpty()) {
            setAutoScrollObserver()
        }
        setMyAnalyticsBlinkAnimation()
    }

    override fun onStop() {
        disposable.clear()
        super.onStop()
    }
}