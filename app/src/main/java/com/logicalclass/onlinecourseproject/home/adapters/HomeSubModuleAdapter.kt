package com.logicalclass.onlinecourseproject.home.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.logicalclass.onlinecourseproject.databinding.ItemHomeSubModuleBinding
import com.logicalclass.onlinecourseproject.home.HomeModuleData
import com.logicalclass.onlinecourseproject.home.HomeSubModuleData
import com.logicalclass.onlinecourseproject.utils.CommonUtils.getDrawableFromName

class HomeSubModuleAdapter(
    private val homeModuleData: HomeModuleData,
    private val moduleList: List<HomeSubModuleData>,
    private val onItemClick: (HomeSubModuleData) -> Unit,
) : RecyclerView.Adapter<HomeSubModuleAdapter.HomeSubModuleViewHolder>() {

    override fun getItemCount(): Int {
        return moduleList.size
    }

    private fun getItem(position: Int): HomeSubModuleData {
        return moduleList[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeSubModuleViewHolder {
        return HomeSubModuleViewHolder(
            ItemHomeSubModuleBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: HomeSubModuleViewHolder, position: Int) {
        val subModuleData = getItem(position)
        holder.binding.moduleData = subModuleData

        getDrawableFromName(
            holder.binding.ivSubHeading.context,
            subModuleData.iconName
        )?.let {
            holder.binding.ivSubHeading.setImageDrawable(it)
        }
    }

    inner class HomeSubModuleViewHolder(var binding: ItemHomeSubModuleBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            binding.subModuleRoot.setOnClickListener {
                onItemClick(getItem(absoluteAdapterPosition))
            }
        }
    }
}