package com.logicalclass.onlinecourseproject.home;

import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.logicalclass.onlinecourseproject.LogicalClassApp
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.core.BaseViewModel
import com.logicalclass.onlinecourseproject.profile.models.UserProfileResponse
import com.logicalclass.onlinecourseproject.repository.AppService
import com.logicalclass.onlinecourseproject.repository.RxUtils
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils

sealed class HomeUiModel {
    data class HomeModuleError(val error: String?) : HomeUiModel()
    data class HomeModuleSuccess(val moduleList: ArrayList<HomeModuleData>) : HomeUiModel()
}

sealed class TipsUiModel {
    data class TipsError(val error: String?) : TipsUiModel()
    data class TipsSuccess(val response: TipResponse) : TipsUiModel()
}

sealed class DiscardTipUiModel {
    data class DiscardTipError(val error: String?) : DiscardTipUiModel()
    data class DiscardTipSuccess(val response: TipResponse) : DiscardTipUiModel()
}

sealed class ProfileUiModel {
    data class ProfileError(val error: String?) : ProfileUiModel()
    data class ProfileSuccess(val response: UserProfileResponse) : ProfileUiModel()
}

class HomeViewModel(
    private val application: LogicalClassApp,
) : BaseViewModel(application) {

    var mainModuleList = arrayListOf<HomeModuleData>()
    var tipsList = arrayListOf<TipData>()
    val auth = SharedPrefsUtils.getStringPreference(application, SharedPrefsUtils.TOKEN_ID_2)
    val orgAuth = SharedPrefsUtils.getStringPreference(application, SharedPrefsUtils.ORG_AUTH)

    fun fetchHomeModuleData(): MutableLiveData<HomeUiModel> {
        val moduleListLiveData = MutableLiveData<HomeUiModel>()
        val accountResponseString = getJsonDataFromAsset(application, "main_module_data.json")
        moduleListLiveData.value = HomeUiModel.HomeModuleSuccess(
            Gson().fromJson(
                accountResponseString,
                object : TypeToken<List<HomeModuleData>?>() {}.type
            )
        )
        return moduleListLiveData
    }

    fun fetchTipsNotifications(): MutableLiveData<TipsUiModel> {
        val tipsLiveData = MutableLiveData<TipsUiModel>()
        disposable.add(
            AppService.create().getTipsNotifications(auth, orgAuth)
                .subscribeOn(RxUtils.ioThread())
                .doOnSubscribe {
                    showProgress.value = true
                }.observeOn(RxUtils.androidThread())
                .subscribe({ response ->
                    showProgress.value = false
                    tipsLiveData.value = TipsUiModel.TipsSuccess(response)
                }, {
                    showProgress.value = false
                    tipsLiveData.value =
                        TipsUiModel.TipsError(getString(R.string.something_went_wrong))
                })
        )
        return tipsLiveData
    }

    fun discardTipNotification(notificationId: String): MutableLiveData<DiscardTipUiModel> {
        val discardSuccessLiveData = MutableLiveData<DiscardTipUiModel>()
        disposable.add(
            AppService.create().discardTipNotification(auth, orgAuth, notificationId)
                .subscribeOn(RxUtils.ioThread())
                .doOnSubscribe {
                    showProgress.value = true
                }.observeOn(RxUtils.androidThread())
                .subscribe({ response ->
                    showProgress.value = false
                    discardSuccessLiveData.value = DiscardTipUiModel.DiscardTipSuccess(response)
                }, {
                    showProgress.value = false
                    discardSuccessLiveData.value =
                        DiscardTipUiModel.DiscardTipError(getString(R.string.something_went_wrong))
                })
        )
        return discardSuccessLiveData
    }

    fun fetchProfileInfo(): MutableLiveData<ProfileUiModel> {
        val profileLiveData = MutableLiveData<ProfileUiModel>()
        disposable.add(
            AppService.create().getProfile(
                auth
            ).subscribeOn(RxUtils.ioThread())
                .doOnSubscribe {
                    showProgress.value = true
                }.observeOn(RxUtils.androidThread())
                .subscribe({ response: UserProfileResponse ->
                    profileLiveData.value = ProfileUiModel.ProfileSuccess(response)
                    showProgress.value = false
                }, {
                    showProgress.value = false
                    profileLiveData.value =
                        ProfileUiModel.ProfileError(getString(R.string.something_went_wrong))
                })
        )
        return profileLiveData
    }
}