package com.logicalclass.onlinecourseproject.home.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.logicalclass.onlinecourseproject.databinding.ItemHomeMainModuleBinding
import com.logicalclass.onlinecourseproject.home.HomeModuleData
import com.logicalclass.onlinecourseproject.home.HomeSubModuleData

class HomeMainModuleAdapter(
    private val mainModuleList: List<HomeModuleData>,
    private val onItemClick: (HomeSubModuleData) -> Unit
) : RecyclerView.Adapter<HomeMainModuleAdapter.MainModuleViewHolder>() {

    override fun getItemCount(): Int {
        return mainModuleList.size
    }

    private fun getItem(position: Int): HomeModuleData {
        return mainModuleList[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainModuleViewHolder {
        return MainModuleViewHolder(
            ItemHomeMainModuleBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MainModuleViewHolder, position: Int) {
        val homeModuleData = getItem(position)
        holder.binding.mainModule = homeModuleData
        holder.binding.rvSubModule.apply {
            layoutManager = GridLayoutManager(holder.binding.txtHeading.context, 4)
            adapter =
                HomeSubModuleAdapter(homeModuleData, homeModuleData.subModuleList, onItemClick)
        }
    }

    inner class MainModuleViewHolder(var binding: ItemHomeMainModuleBinding) :
        RecyclerView.ViewHolder(binding.root)
}