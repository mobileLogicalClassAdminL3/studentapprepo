package com.logicalclass.onlinecourseproject.home.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.logicalclass.onlinecourseproject.databinding.ItemTipsLayoutBinding
import com.logicalclass.onlinecourseproject.home.TipData
import com.logicalclass.onlinecourseproject.utils.setHTMLText

class TipsPagerAdapter(
    private val studentName: String?,
    private val tipsList: ArrayList<TipData>,
    private val onItemClick: (TipData) -> Unit,
    private val onTipDismissClicked: (Int, TipData) -> Unit
) : RecyclerView.Adapter<TipsPagerAdapter.TipsPagerHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TipsPagerHolder {
        return TipsPagerHolder(
            ItemTipsLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    fun getItem(pos: Int): TipData {
        return tipsList[pos]
    }

    override fun onBindViewHolder(holder: TipsPagerHolder, position: Int) {
        val tipData = getItem(position)
        val tipContent =
            "Dear <b>$studentName</b>, you might be facing difficulty in the <b>${tipData.topic}</b> while solving questions, Please go through the following Tips."
        holder.binding.tvTip.setHTMLText(tipContent)
    }

    override fun getItemCount(): Int {
        return tipsList.size
    }

    fun removeTip(position: Int) {
        tipsList.removeAt(position)
        notifyItemRemoved(position)
    }

    inner class TipsPagerHolder(val binding: ItemTipsLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            binding.cardTipRoot.setOnClickListener {
                onItemClick(getItem(absoluteAdapterPosition))
            }

            binding.ivMarkAsRead.setOnClickListener {
                onTipDismissClicked(absoluteAdapterPosition, getItem(absoluteAdapterPosition))
            }
        }
    }
}