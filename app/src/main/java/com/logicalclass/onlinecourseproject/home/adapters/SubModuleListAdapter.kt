package com.logicalclass.onlinecourseproject.home.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.logicalclass.onlinecourseproject.databinding.ItemModuleViewBinding
import com.logicalclass.onlinecourseproject.home.HomeSubModuleData
import com.logicalclass.onlinecourseproject.utils.CommonUtils.getDrawableFromName

class SubModuleListAdapter(
    val context: Context,
    private val moduleList: List<HomeSubModuleData>,
    private val onItemClick: (HomeSubModuleData) -> Unit,
) :
    RecyclerView.Adapter<SubModuleListAdapter.SubModuleViewHolder>() {

    override fun getItemCount(): Int {
        return moduleList.size
    }

    private fun getItem(position: Int): HomeSubModuleData {
        return moduleList[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubModuleViewHolder {
        return SubModuleViewHolder(
            ItemModuleViewBinding.inflate(
                LayoutInflater.from(context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: SubModuleViewHolder, position: Int) {
        val moduleData = getItem(position)
        holder.binding.moduleData = moduleData
        getDrawableFromName(
            holder.binding.imgLogo.context,
            moduleData.iconName
        )?.let {
            holder.binding.imgLogo.setImageDrawable(it)
        }
    }

    inner class SubModuleViewHolder(var binding: ItemModuleViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            binding.cardModule.setOnClickListener {
                onItemClick(getItem(absoluteAdapterPosition))
            }
        }
    }
}