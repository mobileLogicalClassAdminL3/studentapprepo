package com.logicalclass.onlinecourseproject

import android.app.Application
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.jakewharton.threetenabp.AndroidThreeTen

class LogicalClassApp : Application() {

    override fun onCreate() {
        super.onCreate()
        // Initialize ThreeTenABP library
        AndroidThreeTen.init(this)
        initFirebaseRemoteConfig()
    }

    private fun initFirebaseRemoteConfig() {
        val mFirebaseRemoteConfig: FirebaseRemoteConfig? = FirebaseRemoteConfig.getInstance()
        val configSettings = FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(100)
                .build()
        mFirebaseRemoteConfig?.setConfigSettingsAsync(configSettings)
        mFirebaseRemoteConfig?.setDefaultsAsync(R.xml.remote_config_defaults)
        mFirebaseRemoteConfig?.fetchAndActivate()?.addOnCompleteListener {
            if (it.isSuccessful) {
                val result = it.result
            } else {

            }
        }
    }
}