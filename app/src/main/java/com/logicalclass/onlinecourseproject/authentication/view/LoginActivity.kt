package com.logicalclass.onlinecourseproject.authentication.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.InputType
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.logicalclass.onlinecourseproject.BuildConfig
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.authentication.model.UserDetails
import com.logicalclass.onlinecourseproject.home.MainActivity
import com.logicalclass.onlinecourseproject.models.SearchOrgResponse
import com.logicalclass.onlinecourseproject.repository.AppService.Factory.create
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.androidThread
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.ioThread
import com.logicalclass.onlinecourseproject.utils.*
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.login_layout.*

class LoginActivity : AppCompatActivity(), ForceUpdateChecker.OnUpdateNeededListener {
    var tokenid: String? = ""
    private var disposable: CompositeDisposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_layout)
        disposable = CompositeDisposable()
        setClickListeners()
        if (BuildConfig.FLAVOR != "logicalclass") {
            requestForSearchOrganization(BuildConfig.ORG_KEY)
        } else {
            val searchOrgResponse =
                intent.extras?.getParcelable<SearchOrgResponse>("organisation_obj")
            setOrganizationDetails(searchOrgResponse)
        }

        if (BuildConfig.DEBUG) {
            edtUserName.setText("20TG1035345")
            edtPassword.setText("20TG1035345")
        }
    }

    private fun setClickListeners() {
        login_btn.setOnClickListener {
            hideKeyboard()
            doLoginNew()
        }
        txtForgotPassword.setOnClickListener { showForgotPasswordDialog() }
    }

    private fun doLoginNew() {
        disposable?.add(create().login(
            edtUserName?.text.toString().trim { it <= ' ' },
            edtPassword?.text.toString().trim { it <= ' ' },
            tokenid ?: "",
            SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.DEVICE_TOKEN)
        )
            .subscribeOn(ioThread())
            .doOnSubscribe {
                showProgress()
            }
            .observeOn(androidThread())
            .subscribe({ userDetails: UserDetails ->
                handleLoginResponse(userDetails)
            }, {
                hideProgress()
                rootView.showApiParsingErrorSnack()
            })
        )
    }

    private fun hideProgress() {
        rlProgress.hide()
    }

    private fun showProgress() {
        rlProgress.show()
    }

    private fun handleLoginResponse(userDetails: UserDetails) {
        if (userDetails.status == 200) {
            SharedPrefsUtils.setStringPreference(
                this, SharedPrefsUtils.STUDENT_ID,
                edtUserName?.text.toString().trim()
            )
            SharedPrefsUtils.setBooleanPreference(
                this@LoginActivity,
                SharedPrefsUtils.REMEMBER_ME,
                ckbRememberMe.isChecked
            )
            SharedPrefsUtils.setStringPreference(
                this, SharedPrefsUtils.TOKEN_ID_2,
                userDetails.token
            )
            SharedPrefsUtils.setStringPreference(
                this, SharedPrefsUtils.STUDENT_AUTH,
                userDetails.auth.StudentAuth
            )
            SharedPrefsUtils.setStringPreference(
                this, SharedPrefsUtils.ORG_AUTH,
                userDetails.auth.orgAuth
            )
            SharedPrefsUtils.setStringPreference(
                this, SharedPrefsUtils.CLASS_NAME,
                userDetails.auth.className
            )
            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TOP
                        or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        or Intent.FLAG_ACTIVITY_NEW_TASK
            )
            startActivity(intent)
            finish()
        } else {
            rootView.showSnackMessage(
                userDetails.errorMessage
                    ?: getString(R.string.something_went_wrong)
            )
        }
        hideProgress()
    }

    private fun showForgotPasswordDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Forgot Password")
        val input = EditText(this)
        input.inputType = InputType.TYPE_CLASS_TEXT
        val lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
        )
        lp.setMargins(20, 20, 20, 20)
        input.layoutParams = lp
        val linearLayout = LinearLayout(this)
        linearLayout.addView(input)
        builder.setView(linearLayout)
        builder.setPositiveButton(
            getString(android.R.string.ok)
        ) { _, _ -> requestForForgotPassword(input.text.toString()) }
        builder.setNegativeButton(
            android.R.string.no
        ) { dialog, _ -> dialog.cancel() }
        builder.show()
    }

    private fun requestForForgotPassword(uniqueId: String) {
        disposable?.add(create().forgotPassword(uniqueId)
            .subscribeOn(ioThread())
            .doOnSubscribe {
                showProgress()
            }
            .observeOn(androidThread())
            .subscribe({ userDetails: UserDetails ->
                handleForgotPasswordResponse(userDetails)
            }, {
                hideProgress()
                rootView.showApiParsingErrorSnack()
            })
        )
    }

    private fun handleForgotPasswordResponse(userDetails: UserDetails) {
        if (userDetails.status == 200) {
            rootView.showSnackMessage(
                userDetails.errorMessage
                    ?: getString(R.string.something_went_wrong)
            )
        } else {
            rootView.showSnackMessage(
                userDetails.errorMessage
                    ?: getString(R.string.something_went_wrong)
            )
        }
        hideProgress()
    }

    private fun requestForSearchOrganization(orgName: String) {
        disposable?.clear()
        disposable?.add(
            create().getCheckOrganisation(orgName)
                .subscribeOn(ioThread())
                .doOnSubscribe {
                    showProgress()
                }.observeOn(androidThread())
                .subscribe({ response: SearchOrgResponse ->
                    handleSearchOrganizationResponse(response)
                }, {
                    hideProgress()
                    showRetryDialog(getString(R.string.something_went_wrong))
                })
        )
    }

    private fun handleSearchOrganizationResponse(response: SearchOrgResponse) {
        if (response.status == 200) {
            SharedPrefsUtils.setStringPreference(this, SharedPrefsUtils.ORG_NAME, response.orgName)
            setOrganizationDetails(response)
            checkPlayStoreUpdate()
        } else {
            showRetryDialog(response.message ?: getString(R.string.something_went_wrong))
        }
        hideProgress()
    }

    private fun showRetryDialog(errorMessage: String) {
        AlertDialog.Builder(this)
            .setTitle("Error")
            .setMessage(errorMessage)
            .setPositiveButton(R.string.retry) { _, _ -> requestForSearchOrganization(BuildConfig.ORG_KEY) }
            .show()
    }

    private fun setOrganizationDetails(response: SearchOrgResponse?) {
        response?.let {
            tokenid = response.token
            val imgURL = BuildConfig.HTTP_KEY + response.img
            Glide.with(this).load(imgURL)
                .into(imgOrgLogo)
            tvOrgName.text = getString(R.string.welcome_to_org, response.orgName)
            tvOrgName.show()
        }
    }

    private fun checkPlayStoreUpdate() {
        if (!BuildConfig.DEBUG)
            ForceUpdateChecker.with(this).onUpdateNeeded(this).check()
    }

    override fun onUpdateNeeded(updateUrl: String?, mandatoryUpdate: Boolean?) {
        val builder = AlertDialog.Builder(this)
        if (mandatoryUpdate == true) {
            builder.setCancelable(false)
        } else {
            builder.setCancelable(true)
            builder.setNegativeButton("No, Thanks") { dialog, _ ->
                dialog.dismiss()
            }
        }
        builder.setTitle("New version available")
        builder.setPositiveButton("Update") { dialog, _ ->
            dialog.dismiss()
            redirectStore(updateUrl)
        }
        builder.setMessage(getString(R.string.update_available_description_dialog))
            .show()
    }

    private fun redirectStore(updateUrl: String?) {
        val intent = Intent(Intent.ACTION_VIEW).setData(Uri.parse(updateUrl))
        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.clear()
    }
}