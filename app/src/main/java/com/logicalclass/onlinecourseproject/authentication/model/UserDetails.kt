package com.logicalclass.onlinecourseproject.authentication.model

import com.google.gson.annotations.SerializedName
import com.logicalclass.onlinecourseproject.repository.Auth

data class UserDetails(
        val auth: Auth,
        val device_token: String,
        val message: String?,
        @SerializedName("msg")
        val errorMessage: String?,
        val status: Int,
        val token: String
)