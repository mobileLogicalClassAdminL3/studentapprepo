package com.logicalclass.onlinecourseproject.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import kotlinx.android.parcel.Parcelize

data class SubjectChapterModel<T>(
    val chapters: ArrayList<T>,
    val subject: String?,
    val url: String?,
    val className: String?
)

fun getChaptersList(chaptersStringList: ArrayList<*>): List<Chapter> {
    val chaptersList = arrayListOf<Chapter>()
    chaptersStringList.forEach {
        when (it) {
            is String -> {
                chaptersList.add(Chapter(it))
            }
            is Chapter -> {
                chaptersList.add(it)
            }
        }
    }
    return chaptersList
}

class SubjectSection(subject: String?, chapters: List<Chapter>?) :
    ExpandableGroup<Chapter?>(subject, chapters) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        return other is SubjectChapterModel<*>
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }
}

@Parcelize
open class Chapter(
    var name: String?,
    @SerializedName("Medium")
    var medium: String? = null,
    @SerializedName("Basic")
    var basic: String? = null,
    @SerializedName("Tough")
    var tough: String? = null
) : Parcelable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Chapter) return false
        return if (name != null) name == other.name else other.name == null
    }

    override fun hashCode(): Int {
        var result = name?.hashCode() ?: 0
        result = 31 * result + (medium?.hashCode() ?: 0)
        result = 31 * result + (basic?.hashCode() ?: 0)
        result = 31 * result + (tough?.hashCode() ?: 0)
        return result
    }
}