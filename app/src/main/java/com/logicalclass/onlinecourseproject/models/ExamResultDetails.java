package com.logicalclass.onlinecourseproject.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ExamResultDetails {
    @SerializedName("nameOfExam")
    @Expose
    public String nameOfExam;
    @SerializedName("class")
    @Expose
    public String _class;
    @SerializedName("examination")
    @Expose
    public String examination;
    @SerializedName("start")
    @Expose
    public String start;
    @SerializedName("end")
    @Expose
    public String end;
    @SerializedName("startTime")
    @Expose
    public String startTime;
    @SerializedName("endTime")
    @Expose
    public String endTime;
    @SerializedName("duration")
    @Expose
    public String duration;
    @SerializedName("negative")
    @Expose
    public String negative;
    @SerializedName("to")
    @Expose
    public String to;
    @SerializedName("from")
    @Expose
    public String from;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("discount")
    @Expose
    public String discount;
    @SerializedName("otherClass")
    @Expose
    public List<String> otherClass = null;
    @SerializedName("about")
    @Expose
    public String about;
    @SerializedName("display")
    @Expose
    public String display;
    @SerializedName("creatorType")
    @Expose
    public String creatorType;
    @SerializedName("auth")
    @Expose
    public String auth;
    @SerializedName("id")
    @Expose
    public String id;
}