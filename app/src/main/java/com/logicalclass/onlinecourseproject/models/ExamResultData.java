package com.logicalclass.onlinecourseproject.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ExamResultData {
    @SerializedName("value")
    @Expose
    public ExamResultDetails examResultDetails;
    @SerializedName("class")
    @Expose
    public String _class;
    @SerializedName("nameOfExam")
    @Expose
    public String nameOfExam;
    @SerializedName("examination")
    @Expose
    public String examination;
    @SerializedName("start")
    @Expose
    public String start;
    @SerializedName("end")
    @Expose
    public String end;
    @SerializedName("subjects")
    @Expose
    public List<String> subjectsList = new ArrayList<>();
    @SerializedName("image")
    @Expose
    public String image;

    public String getSubjectsString() {
        StringBuilder subjects = new StringBuilder();
        for (int i = 0; i < subjectsList.size(); i++) {
            if (i != 0) {
                subjects.append(",");
            }
            subjects.append(subjectsList.get(i));
        }
        return subjects.toString();
    }
}