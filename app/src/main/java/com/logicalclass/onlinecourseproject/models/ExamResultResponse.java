package com.logicalclass.onlinecourseproject.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ExamResultResponse {
    @SerializedName("status")
    @Expose
    public Integer status;
    @SerializedName("authData")
    @Expose
    public AuthData authData;
    @SerializedName("data")
    @Expose
    public List<ExamResultData> examResultsList = new ArrayList<>();
    @SerializedName("msg")
    @Expose
    public String msg;
    @SerializedName("token")
    @Expose
    public String token;
}