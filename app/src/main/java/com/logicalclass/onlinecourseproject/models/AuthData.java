package com.logicalclass.onlinecourseproject.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AuthData {
    @SerializedName("StudentAuth")
    @Expose
    public String studentAuth;
    @SerializedName("class")
    @Expose
    public String _class;
    @SerializedName("orgAuth")
    @Expose
    public String orgAuth;
}