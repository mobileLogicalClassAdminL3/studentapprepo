package com.logicalclass.onlinecourseproject.models

import com.google.gson.annotations.SerializedName
import com.logicalclass.onlinecourseproject.repository.Auth
import com.logicalclass.onlinecourseproject.repository.Id

data class StudyMaterialsTopicsResponse(
    val authData: Auth,
    @SerializedName("data")
    val data: TopicsData,
    val msg: String,
    val status: Int,
    val token: String
)

data class TopicsData(
    val basicAssetsUrl: String,
    val chapterName: String,
    val className: String,
    val studyMaterials: List<TopicModel>,
    val subjectName: String
)

data class TopicsResponse(
    val authData: Auth,
    val data: List<TopicModel>,
    val msg: String,
    val status: Int,
    val token: String
)

data class TopicModel(
    val _id: Id,
    val chapterName: String,
    val className: String,
    val date: String,
    val downloads: Int,
    val fileName: String,
    val org: String,
    val scope: String,
    val subjectName: String,
    /* don't use this variable, use getNameOfTheTopic() instead*/
    val topicName: String?,
    val pdfFile: String,
    val topic: String?,
    val videoFile: String,
    @SerializedName("Medium")
    var medium: String? = null,
    @SerializedName("Basic")
    var basic: String? = null,
    @SerializedName("Tough")
    var tough: String? = null
) {
    fun getNameOfTheTopic(): String {
        return topicName ?: (topic ?: "")
    }
}
