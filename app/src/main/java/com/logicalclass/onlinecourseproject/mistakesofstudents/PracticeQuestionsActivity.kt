package com.logicalclass.onlinecourseproject.mistakesofstudents

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.MenuItem
import android.widget.ScrollView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.databinding.ActivityPracticeQuestionsBinding
import com.logicalclass.onlinecourseproject.databinding.DialogSubmitQuestionResponseBinding
import com.logicalclass.onlinecourseproject.home.*
import com.logicalclass.onlinecourseproject.mistakesofstudents.model.AppearInAllQuestions
import com.logicalclass.onlinecourseproject.mistakesofstudents.model.SingleQuestionResponse
import com.logicalclass.onlinecourseproject.onlinetest.model.Question
import com.logicalclass.onlinecourseproject.onlinetest.model.QuestionModel
import com.logicalclass.onlinecourseproject.onlinetest.model.SubmitQuestionResponse
import com.logicalclass.onlinecourseproject.repository.AppService
import com.logicalclass.onlinecourseproject.repository.RxUtils
import com.logicalclass.onlinecourseproject.utils.*
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_practice_questions.*

class PracticeQuestionsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPracticeQuestionsBinding
    private var moduleType: String? = null
    private var subject: String? = null
    private var chapter: String? = null
    private var topic: String? = null

    private var orgAuth: String? = null
    private var auth: String? = null
    private var disposable: CompositeDisposable? = null

    private var questionModel: QuestionModel? = null
    private var requestQuestionBody: Question = Question()

    private var allQuestionsList: ArrayList<String> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_practice_questions)
        getIntentData()
        setToolbarData()
        disposable = CompositeDisposable()
        orgAuth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.ORG_AUTH)
        auth = SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.TOKEN_ID_2)

        setOnCheckChangeListeners()
        requestForAllQuestionsOfTopic()
    }

    private fun setToolbarData() {
        supportActionBar?.title = topic
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun getIntentData() {
        subject = intent?.getStringExtra("subject")
        chapter = intent?.getStringExtra("chapter")
        topic = intent?.getStringExtra("topic")
        moduleType = intent.getStringExtra("MODULE_TYPE")
    }

    private fun requestForAllQuestionsOfTopic() {
        getModuleAPIObservable()?.let {
            disposable?.add(
                it.subscribeOn(RxUtils.ioThread())
                    .doOnSubscribe {
                        showProgress()
                    }.observeOn(RxUtils.androidThread())
                    .subscribe({ response ->
                        handleAllQuestionsResponse(response)
                    }, {
                        showAlertMessageWithAction(
                            message = getString(R.string.something_went_wrong),
                            callBack = ::onBackPressed
                        )
                        hideProgress()
                    })
            )
        }
    }

    private fun handleAllQuestionsResponse(response: AppearInAllQuestions) {
        if (response.status == 200 && !response.data.questionIds.isNullOrEmpty()) {
            allQuestionsList = response.data.questionIds
            moveToNextQuestion()
        } else {
            showAlertMessageWithAction(
                message = getString(R.string.something_went_wrong),
                callBack = ::onBackPressed
            )
            hideProgress()
        }
    }

    private fun moveToNextQuestion() {
        // Reset current question info. Ready for next question
        requestQuestionBody = Question()
        questionModel = null
        binding.groupSolutionViews.hide()
        binding.ckbOpt1.isChecked = false
        binding.ckbOpt2.isChecked = false
        binding.ckbOpt3.isChecked = false
        binding.ckbOpt4.isChecked = false

        if (allQuestionsList.isNotEmpty())
            requestForGetSingleQuestion(allQuestionsList[0])
        else
            onBackPressed()
    }

    private fun requestForGetSingleQuestion(questionId: String) {
        disposable?.add(
            AppService.create().getOneQuestion(
                auth,
                orgAuth,
                questionId
            ).subscribeOn(RxUtils.ioThread())
                .doOnSubscribe {
                    showProgress()
                }.observeOn(RxUtils.androidThread())
                .subscribe({ response ->
                    handleSingleQuestionResponse(response)
                }, {
                    showAlertMessageWithAction(
                        message = getString(R.string.something_went_wrong),
                        callBack = ::onBackPressed
                    )
                    hideProgress()
                })
        )
    }

    private fun handleSingleQuestionResponse(response: SingleQuestionResponse) {
        if (response.status == 200) {
            allQuestionsList.removeAt(0)
            setQuestionViewData(response.data.question)
            Handler(Looper.getMainLooper()).postDelayed({
                hideProgress()
            }, 1500)
        } else {
            showAlertMessageWithAction(
                message = getString(R.string.something_went_wrong),
                callBack = ::onBackPressed
            )
            hideProgress()
        }
    }

    private fun requestForSubmitQuestion(question: Question) {
        disposable?.add(
            AppService.create().submitQuestion(
                auth,
                orgAuth,
                question
            ).subscribeOn(RxUtils.ioThread())
                .doOnSubscribe {
                    showProgress()
                }.observeOn(RxUtils.androidThread())
                .subscribe({ response ->
                    handleSubmitQuestion(response)
                }, {
                    enableDisableOptionSelection(true)
                    showAlertMessage(message = getString(R.string.something_went_wrong))
                    hideProgress()
                })
        )
    }

    private fun handleSubmitQuestion(response: SubmitQuestionResponse) {
        if (response.status == 200) {
            /*if (response.data.isCorrect)
                showAlertMessageWithAction(
                    message = getSubmitResponseMessage(true),
                    callBack = ::moveToNextQuestion
                )
            else
                showAlertMessageWithAction(
                    message = getSubmitResponseMessage(false),
                    callBack = ::showSolutionView
                )*/
            showSubmitQuestionResponseDialog(response.data.isCorrect)
        } else {
            enableDisableOptionSelection(true)
            showAlertMessage(message = getString(R.string.something_went_wrong))
        }
        hideProgress()
    }

    private fun getSubmitResponseMessage(isCorrect: Boolean): String {
        return if (isCorrect) {
            if (allQuestionsList.isEmpty())
                getString(R.string.you_have_chosen_correct_answer_ok_to_exit)
            else
                getString(R.string.you_have_chosen_correct_answer_ok_to_next_question)
        } else {
            getString(R.string.you_have_chosen_wrong_answer_ok_to_view_solution)
        }
    }

    private fun showSolutionView() {
        binding.scrollView.post { binding.scrollView.fullScroll(ScrollView.FOCUS_DOWN) }
        binding.groupSolutionViews.show()
        binding.btnNextQuestion.text =
            getString(if (allQuestionsList.isEmpty()) R.string.exit else R.string.next_question)
        binding.txtSolution.setDisplayText(questionModel?.getFormattedSolution())
    }

    private fun getModuleAPIObservable(): Observable<AppearInAllQuestions>? {
        return when (moduleType) {
            TIME_TAKEN_QUESTIONS -> {
                AppService.create().getOwnTimeTakenAllQuestionIds(
                    auth,
                    orgAuth,
                    subject,
                    chapter,
                    topic
                )
            }
            MISTAKE_OF_MOST_STUDENTS -> {
                AppService.create().getMistakesOfMostStudentsAllQuestionIds(
                    auth,
                    orgAuth,
                    subject,
                    chapter,
                    topic
                )
            }
            PRACTICE_TIME_WASTED_QUESTIONS_OF_MOST_STUDENTS -> {
                AppService.create().getPracticeTimeWastedOfMostStudentsAllQuestionIds(
                    auth,
                    orgAuth,
                    subject,
                    chapter,
                    topic
                )
            }
            WRONG_OF_TOPPER -> {
                AppService.create().getWrongOfTopperAllQuestionIds(
                    auth,
                    orgAuth,
                    subject,
                    chapter,
                    topic
                )
            }
            TIME_WASTED_QUESTION_OF_TOPPER -> {
                AppService.create().getTimeWastedOfTopperAllQuestionIds(
                    auth,
                    orgAuth,
                    subject,
                    chapter,
                    topic
                )
            }
            PRACTICE_OWN_MISTAKE_QUESTIONS -> {
                AppService.create().getPractiseOwnMistakesAllQuestionsIds(
                    auth,
                    orgAuth,
                    subject,
                    chapter,
                    topic
                )
            }
            else -> null
        }
    }

    private fun setOnCheckChangeListeners() {
        binding.ckbOpt1.setOnCheckedChangeListener { _, isChecked ->
            if (requestQuestionBody.opt.isEmpty() && !isChecked)
                return@setOnCheckedChangeListener
            requestQuestionBody.setAnswer("opt1")
            binding.btnSubmitQuestion.isEnabled = requestQuestionBody.opt.isNotEmpty()
        }
        binding.ckbOpt2.setOnCheckedChangeListener { _, isChecked ->
            if (requestQuestionBody.opt.isEmpty() && !isChecked)
                return@setOnCheckedChangeListener
            requestQuestionBody.setAnswer("opt2")
            binding.btnSubmitQuestion.isEnabled = requestQuestionBody.opt.isNotEmpty()
        }

        binding.ckbOpt3.setOnCheckedChangeListener { _, isChecked ->
            if (requestQuestionBody.opt.isEmpty() && !isChecked)
                return@setOnCheckedChangeListener
            requestQuestionBody.setAnswer("opt3")
            binding.btnSubmitQuestion.isEnabled = requestQuestionBody.opt.isNotEmpty()
        }

        binding.ckbOpt4.setOnCheckedChangeListener { _, isChecked ->
            if (requestQuestionBody.opt.isEmpty() && !isChecked)
                return@setOnCheckedChangeListener
            requestQuestionBody.setAnswer("opt4")
            binding.btnSubmitQuestion.isEnabled = requestQuestionBody.opt.isNotEmpty()
        }

        binding.btnSubmitQuestion.setOnClickListener {
            enableDisableOptionSelection(false)
            requestForSubmitQuestion(requestQuestionBody)
        }

        binding.btnNextQuestion.setOnClickListener {
            moveToNextQuestion()
        }

        binding.imgMaximizeQuestion.setOnClickListener {
            binding.frameFullView.show()
            binding.txtFullView.setDisplayText(questionModel?.getFormattedQuestion())
        }

        binding.imgMaximizeSolution.setOnClickListener {
            binding.frameFullView.show()
            binding.txtFullView.setDisplayText(questionModel?.getFormattedSolution())
        }

        binding.imgMinimize.setOnClickListener {
            binding.frameFullView.hide()
            binding.txtFullView.setDisplayText("")
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setQuestionViewData(questionModel: QuestionModel) {
        if (!binding.rootView.isVisible) binding.rootView.show()
        this.questionModel = questionModel
        requestQuestionBody.subject = subject
        requestQuestionBody.chapter = chapter
        requestQuestionBody.topic = topic
        requestQuestionBody.className = questionModel.className
        requestQuestionBody.id = questionModel.id

        binding.txtQuestion.setDisplayText(questionModel.getFormattedQuestion())
        binding.txtOpt1Value.setDisplayText(questionModel.getFormattedOpt1())
        binding.txtOpt2Value.setDisplayText(questionModel.getFormattedOpt2())
        binding.txtOpt3Value.setDisplayText(questionModel.getFormattedOpt3())
        binding.txtOpt4Value.setDisplayText(questionModel.getFormattedOpt4())
        binding.txtSelectOptions.text =
            getString(R.string.select_your_options, questionModel.getQuestionType())
        binding.txtDifficultyLevel.text = questionModel.getDifficultyLevel() + " Question"
        binding.txtDifficultyLevel.setBackgroundColor(
            getDifficultyColorValue(questionModel.getDifficultyLevel())
        )
        enableDisableOptionSelection(true)
        binding.btnSubmitQuestion.isEnabled = false
    }

    private fun getDifficultyColorValue(difficultyLevel: String): Int {
        return when {
            difficultyLevel.equals("Basic", true) -> {
                resources.getColor(R.color.difficulty_basic_color, null)
            }
            difficultyLevel.equals("Medium", true) -> {
                resources.getColor(R.color.difficulty_medium_color, null)
            }
            difficultyLevel.equals("Tough", true) -> {
                resources.getColor(R.color.difficulty_tough_color, null)
            }
            else -> {
                resources.getColor(R.color.white, null)
            }
        }
    }

    private fun enableDisableOptionSelection(isEnable: Boolean) {
        binding.ckbOpt1.isEnabled = isEnable
        binding.ckbOpt2.isEnabled = isEnable
        binding.ckbOpt3.isEnabled = isEnable
        binding.ckbOpt4.isEnabled = isEnable
        binding.btnSubmitQuestion.isEnabled = isEnable
    }

    private fun showSubmitQuestionResponseDialog(isCorrect: Boolean) {
        val dialogBinding = DataBindingUtil.inflate<DialogSubmitQuestionResponseBinding>(
            LayoutInflater.from(this),
            R.layout.dialog_submit_question_response,
            null,
            false
        )
        dialogBinding.imAnimationView
            .setAnimation(if (isCorrect) R.raw.correct_answer_anim else R.raw.wrong_anwser_anim)
        dialogBinding.txtMessage.text = getSubmitResponseMessage(isCorrect)
        dialogBinding.btnNextQuestion.text =
            getString(
                if (isCorrect)
                    if (allQuestionsList.isNotEmpty()) R.string.next_question else R.string.exit
                else
                    R.string.view_solution
            )

        val dialog = AlertDialog.Builder(this)
            .setView(dialogBinding.root)
            .setCancelable(false)
            .show()

        dialogBinding.btnNextQuestion.setOnClickListener {
            dialog.dismiss()
            if (isCorrect) moveToNextQuestion() else showSolutionView()
        }
    }

    private fun hideProgress() {
        binding.rlProgress.hide()
    }

    private fun showProgress() {
        binding.rlProgress.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.clear()
    }
}