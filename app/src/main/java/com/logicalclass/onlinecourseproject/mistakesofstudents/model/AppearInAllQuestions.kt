package com.logicalclass.onlinecourseproject.mistakesofstudents.model

import com.logicalclass.onlinecourseproject.onlinetest.model.QuestionModel
import com.logicalclass.onlinecourseproject.repository.Auth

data class AppearInAllQuestions(
    val authData: Auth,
    val data: QuestionIdsData,
    val msg: String,
    val status: Int,
    val token: String
)

data class QuestionIdsData(
    val questionIds: ArrayList<String>
)

data class SingleQuestionResponse(
    val authData: Auth,
    val data: SingleQuestionData,
    val msg: String,
    val status: Int,
    val token: String
)

data class SingleQuestionData(
    val question: QuestionModel
)

